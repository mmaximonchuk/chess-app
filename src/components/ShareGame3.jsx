import React from 'react';
import { Link } from 'react-router-dom';
// import { useSelector } from 'react-redux';
import styled from 'styled-components';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Stepper from './molecules/Stepper';
import HeaderControls from './organism/HeaderControls';

import imgArrowBack from '../assets/images/svg/balck-arrow-back.svg';
import imgShare from '../assets/images/game/VectorShare.svg';
import iFb from '../assets/images/shareTo/fb.svg';
import iWhatsApp from '../assets/images/shareTo/whats-app.svg';
import iViber from '../assets/images/shareTo/viber.svg';
import iMailRu from '../assets/images/shareTo/mail-ru.svg';
import iMessanger from '../assets/images/shareTo/messanger.svg';
import iTelegram from '../assets/images/shareTo/telega.svg';
import iMail from '../assets/images/shareTo/mail.svg';
import iTwitter from '../assets/images/shareTo/twitter.svg';
import iOk from '../assets/images/shareTo/ok.svg';
import iVk from '../assets/images/shareTo/vk.svg';
import iT from '../assets/images/shareTo/t.svg';
import iIn from '../assets/images/shareTo/in.svg';
import iHatena from '../assets/images/shareTo/hatena.svg';
import iPinterest from '../assets/images/shareTo/pinterest.svg';
import iNote from '../assets/images/shareTo/note.svg';
import iWorkPlace from '../assets/images/shareTo/w-place.svg';
import iI from '../assets/images/shareTo/i.svg';
import iArrow from '../assets/images/shareTo/arrow.svg';
import iLine from '../assets/images/shareTo/line.svg';
import iReddit from '../assets/images/shareTo/reddit.svg';
import iEye from '../assets/images/shareTo/eye.svg';

import { theme } from '../assets/styles/variables';

function ShareGame3() {
  // const defaultLang = useSelector(selectLangLocale);
  return (
    <Container>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/share-2-step">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="share.game.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <p className="first-step">3. <FormattedMessage id="share.game.3"/></p>
        <WrapperBgPink>
          <OfferList>
            <button className="share-way">
              <img src={iFb} alt="Facebook" />
            </button>
            <button className="share-way">
              <img src={iWhatsApp} alt="WhatsApp" />
            </button>
            <button className="share-way">
              <img src={iViber} alt="Viber" />
            </button>
            <button className="share-way">
              <img src={iMailRu} alt="MailRu" />
            </button>
            <button className="share-way">
              <img src={iMessanger} alt="Facebook Messanger" />
            </button>
            <button className="share-way">
              <img src={iTelegram} alt="Telegram" />
            </button>
            <button className="share-way">
              <img src={iMail} alt="Mail" />
            </button>

            <button className="share-way">
              <img src={iTwitter} alt="Twitter" />
            </button>
            <button className="share-way">
              <img src={iOk} alt="Ok" />
            </button>
            <button className="share-way">
              <img src={iVk} alt="Vk" />
            </button>
            <button className="share-way">
              <img src={iT} alt="T" />
            </button>
            <button className="share-way">
              <img src={iIn} alt="In" />
            </button>
            <button className="share-way">
              <img src={iHatena} alt="Hatena" />
            </button>
            <button className="share-way">
              <img src={iPinterest} alt="Pinterest" />
            </button>

            <button className="share-way">
              <img src={iNote} alt="Note" />
            </button>
            <button className="share-way">
              <img src={iWorkPlace} alt="WorkPlace" />
            </button>
            <button className="share-way">
              <img src={iI} alt="I" />
            </button>
            <button className="share-way">
              <img src={iArrow} alt="Arrow" />
            </button>
            <button className="share-way">
              <img src={iLine} alt="Line" />
            </button>
            <button className="share-way">
              <img src={iReddit} alt="Reddit" />
            </button>
            <button className="share-way">
              <img src={iEye} alt="Eye" />
            </button>
          </OfferList>
          <button className="share">
            <img src={imgShare} alt="imgShare" />
          </button>
          <Stepper currentStep={3} />
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ShareGame3;

const OfferList = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  .share-way {
    diaply: flex;
    flex: 1 1 25%;
    border: none;
    background: transparent;
    paddong: 0;
    margin-bottom: 10px;
  }
`;
const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 16px 16px 18px;
  overflow: hidden;
  .first-step {
    color: #000;
    margin: 10px 0 28px;
    text-align: center;
    font-size: 20px;
    line-height: 27px;
  }
  .input-share {
    margin: 0 auto;
    width: 235px;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
    margin-bottom: 33px;
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 63px);
  margin: 0 auto;
  padding: 26px 8px 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .share {
    border-radius: 20px;
    background: ${theme.secondaryBg};
    width: 61px;
    height: 24px;
    border: none;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 14px;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
