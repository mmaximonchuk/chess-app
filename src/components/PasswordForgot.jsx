import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import { recoverPasswordRequest } from '../features/user/userSlice';
import Button from './atoms/Button';
import Input from './atoms/Input';
import HeaderControls from './organism/HeaderControls';
import FormattedMessage from './atoms/FormattedMessage';

import imgLogo from '../assets/images/Лого.svg';
import imgArrowBack from '../assets/images/svg/arrow-back.svg';

import { StartContainer } from './styled';
import { theme } from '../assets/styles/variables';

function PasswordForgot() {
  const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({Email: '', Token: ''});
  const handleInputs = (e) => setFormData({...formData, [e.target.name]: e.target.value,});
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(recoverPasswordRequest(formData));
  };

  useEffect(() => {
    let token = localStorage.getItem('token');
    if (!token) {
      token = "tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7"

      localStorage.setItem('token', token);
    }
    setFormData({
      ...formData,
      Token: token,
    });
  }, []);

  return (
    <StartContainer justifyContent="flex-start">

      <StyledLinkBack to="/entrance">
        <img src={imgArrowBack} alt="imgArrowBack" />
      </StyledLinkBack>
      <StyledImg src={imgLogo} alt="" />

      <Title>Democrat chess</Title>
      <Tip>{<FormattedMessage id="passwordForgot.par.description"/>}</Tip>
      <form onSubmit={handleSubmit}>
      <Input
        bgColor="#fff"
        type="email"
        name="Email"
        onChange={handleInputs}
        value={formData.Email}
        placeholder={vocabularyObject['signUp.input.email'][defaultLang]}
      />

      <Button
        fontWeight={700}
        fontSize="14px"
        height="42px"
        margin="2vh 0 0 0"
        defType="primary"
        text={<FormattedMessage id="passwordForgot.btn.send"/>}
        type="submit"

      />
      </form>
      
    </StartContainer>
  );
}

export default PasswordForgot;

const StyledImg = styled.img`
  position: relative;
  width: 61px;
  left: 10px;
  max-width: 120px;
`;

const Title = styled.h1`
  font-size: 40px;
  line-height: 53px;
  margin-top: 0;
  margin-bottom: 2vh;
  color: #000000;
`;
const Tip = styled.p`
  color: ${theme.greyTextColor};
  margin-bottom: 2vh;
  font-size: 12px;
`;

const StyledLinkBack = styled(Link)`
  position: absolute;
  top: 80px;
  left: 8vw;
  img {
  }
`;
