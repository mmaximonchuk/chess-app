import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import Button from './atoms/Button';
import FormattedMessage from './atoms/FormattedMessage';
import {
  getTournamentResultsRequest,
  selectTournamentResults,
} from '../features/tournament/tournamentSlice';
import Loader from './atoms/Loader';

// import iconSearch from '../assets/images/svg/search.svg';
import iconUser from '../assets/images/blogs/person.svg';
import iconInfo from '../assets/images/blogs/infobtn.svg';
import iconSearch from '../assets/images/svg/search.svg';
import iconTick from '../assets/images/clan-pay/Tick.svg';
import iconShare from '../assets/images/blogs/share.svg';
import iconAdd from '../assets/images/clan-pay/AddClan.svg';
import iconUserAdd from '../assets/images/user-profile/user-add.svg';

import { theme } from '../assets/styles/variables';

function Blogs() {
  const defaultLang = useSelector(selectLangLocale);
  const getResults = useSelector(selectTournamentResults);

  const dispatch = useDispatch();
  // const [isUserInfo, setUserInfo] = useState(false);
  const [selector, setSelector] = useState(2);
  const [isActive, setActive] = useState(true);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getTournamentResultsRequest({ Token: token }));
  }, []);

  return (
    <Container>
      <StyledLinkBack to="/all-games">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span>
          <FormattedMessage id="tournament.title" />
        </span>
        <Link to="/tournament-create" className="add-clan">
          <img src={iconAdd} alt="" />
        </Link>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchContainer>
          <input
            type="text"
            placeholder={vocabularyObject['tournament.input.tournament.name'][defaultLang]}
          />
          <button>
            <img src={iconSearch} alt="" />
          </button>
        </SearchContainer>
        <BtnsWrapper>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-1" />
          <label className="label" for="custom-checkbox-1">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span>
              <FormattedMessage id="tournament.by.name" />
            </span>
          </label>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-2" />
          <label className="label" for="custom-checkbox-2">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span>
              <FormattedMessage id="tournament.by.member.name" />
            </span>
          </label>
          <div className="selector">
            <button
              onClick={() => setSelector(1)}
              className={`place ${selector === 1 ? 'active' : ''}`}
            >
              1
            </button>
            <button
              onClick={() => setSelector(2)}
              className={`place ${selector === 2 ? 'active' : ''}`}
            >
              2
            </button>
            <button
              onClick={() => setSelector(3)}
              className={`place ${selector === 3 ? 'active' : ''}`}
            >
              3
            </button>
            <button
              onClick={() => setSelector(4)}
              className={`place ${selector === 4 ? 'active' : ''}`}
            >
              4
            </button>
            <button
              onClick={() => setSelector(5)}
              className={`place ${selector === 5 ? 'active' : ''}`}
            >
              5
            </button>
            <button
              onClick={() => setSelector(6)}
              className={`place ${selector === 6 ? 'active' : ''}`}
            >
              6
            </button>
            <button
              onClick={() => setSelector(7)}
              className={`place ${selector === 7 ? 'active' : ''}`}
            >
              7
            </button>
            <button
              onClick={() => setSelector(8)}
              className={`place ${selector === 8 ? 'active' : ''}`}
            >
              8
            </button>
            <button
              onClick={() => setSelector(9)}
              className={`place ${selector === 9 ? 'active' : ''}`}
            >
              9
            </button>
            <button
              onClick={() => setSelector(10)}
              className={`place ${selector === 10 ? 'active' : ''}`}
            >
              10
            </button>
          </div>
        </BtnsWrapper>
        <WrapperBgPink>
          <div className="toggle-btns">
            <button onClick={() => setActive(true)}>
              <FormattedMessage id="tournament.active" />
            </button>
            <button onClick={() => setActive(false)}>
              <FormattedMessage id="tournament.ended" />
            </button>
          </div>

          {isActive ? (
            <>
              {getResults ? (
                getResults.results.map((game) => {
                  console.log(game.tournamentName);
                  return <TournamentItem key={game.tournamentID} data={game} />;
                })
              ) : (
                <Loader />
              )}
            </>
          ) : (
            <TournamentEnded data={getResults} />
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Blogs;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 141px);
  margin: 0 auto;
  padding: 19px 22px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .toggle-btns {
    display: flex;
    max-width: 228px;
    margin: 0 auto 82px;
    justify-content: space-between;
    button {
      displa: flex;
      width: 100px;
      height: 35px;
      background: #b0b1a1;
      border-radius: 20px;
      font-size: 12px;
      color: #ffffff;
      line-height: 16px;
      cursor: pointer;
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 60%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  margin-bottom: 26px;
  .selector {
    display: flex;
    background: #ffffff;
    border-radius: 50px;
    width: 160px;
    padding: 0px 3px;
    .place {
      font-family: Roboto;
      background: transparent;
      line-height: 16px;
      font-size: 12px;
      display: flex;
      align-items: center;
      text-align: center;
      color: #d8d8d8;
      justify-content: center;
      width: 22px;
      height: 24px;
      transition: all 0.3s;
      cursor: pointer;
      &.active {
        color: #ffffff;
        background: #b0b1a1;
        font-size: 14px;
        border-radius: 8px;
      }
    }
  }
  .custom-checkbox {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    z-index: -1;
  }
  .custom-checkbox + .label {
    cursor: pointer;
    width: 160px;
    height: 24px;
    font-size: 10px;
    line-height: 13px;
    background: #ffffff;
    border-radius: 50px;
    border: none;
    padding: 0;
    position: relative;
    margin-bottom: 3px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    .tick {
      transition: all 0.3s;

      position: absolute;
      left: 0;
      border-radius: 50%;
      position: absolute;
      // background: #b0b1a1;
      width: 24px;
      border: 3px solid #b0b1a1;
      height: 24px;
      top: 0;
      color: #656565;
      img {
        opacity: 0;
        transform: translate(2px, -1px);
      }
    }
  }
  .custom-checkbox:checked + .label .tick img {
    opacity: 1;
  }
  .custom-checkbox:checked + .label {
    .tick {
      background: #b0b1a1;
      img {
        opacity: 1;
      }
    }
  }

  @media (max-width: 342px) {
    flex-direction: column;
    // height: 120px;
    a {
      width: 100%;
    }
  }
`;

function TournamentItem({ data }) {
  // console.log('data: ', data);
  return (
    <Item>
      <div className="avatar"></div>
      <Link to="/" className="name-2">
        {data.tournamentName || 'Название'}
      </Link>
      <Link to="/" className="start-end">
        <FormattedMessage id="tournament.start.end" />
      </Link>
      <Link to="/find-partner" className="add">
        <img src={iconUserAdd} alt="" />
      </Link>
      <Link to="/share-us-tab" className="share">
        <img src={iconShare} alt="" />
      </Link>
    </Item>
  );
}

const Item = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  margin-bottom: 3px;
  position: relative;
  margin-bottom: 4px;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  .avatar {
    min-width: 34px;
    background: #b0b1a1;
    border-radius: 50%;
    text-decoration: none;
    height: 34px;
    margin-bottom: 10px;
  }
  .name-2 {
    marging-bottom: 10px;
    min-width: 41px;
    background: #b0b1a1;
    border-radius: 20px;
    text-decoration: none;
    height: 41px;
    font-size: 12px;
    line-height: 16px;
    display: flex;
    align-items: center;
    text-align: center;
    height: 34px;
    color: #ffffff;
    justify-content: center;
    margin: 0 3px 10px;
    flex: 1 1 40%;
  }
  .start-end {
    background: #b0b1a1;
    text-decoration: none;
    border-radius: 20px;

    height: 34px;

    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    font-size: 8px;
    line-height: 11px;
    margin: 0 3px 10px;
    flex: 1 1 5%;
    color: #ffffff;
    min-width: 62px;
  }
  .add {
    marging-bottom: 10px;
    border-radius: 20px;
    display: flex;
    min-width: 52px;
    height: 34px;
    background: #b0b1a1;
    border-radius: 20px;
    display: flex;
    margin: 0 3px 10px;
    flex: 1 1 5%;
    justify-content: center;
    align-items: center;
    img {
      height: 100%;
    }
  }
  .share {
    display: flex;
    margin: 0 3px 10px;
    justify-content: center;
    align-items: center;
  }
`;

function TournamentEnded({ data }) {
  return (
    <Ended>
      {data ? (
        data.results.map((item) => {
          const winner = item.members.filter((member) => member.place === 1)[0];
          const levels = item.members.map((member) => member.level);
          // console.log(levels);

          return (
            <EndedItem key={item.tournamentID}>
              <div className="wrapper-top">
                <div className="avatar"></div>
                <Link to="/" className="name-2">
                  {item.tournamentName || 'Название'}
                </Link>
                <Link to="/" className="start-end">
                  {item.members.length}
                </Link>
                <Link to="/" className="add">
                  {/* Мин и максимальный уровень участников */}
                  {`${Math.max(levels) || 'Max'} - ${Math.min(levels) || 'Min'}`}
                </Link>
              </div>
              <p className="winner">
                <FormattedMessage id="tournament.winner" />
              </p>
              <div className="wrapper-bottom">
                <div className="ava">
                  <img src={iconUser} alt="" />
                </div>
                <p className="name-3">{`${winner?.firstName || 'Имя'} ${
                  winner?.secondName || 'Фамилия'
                }`}</p>
                <button className="info">
                  <img src={iconInfo} alt="" />
                </button>
              </div>
            </EndedItem>
          );
        })
      ) : (
        <Loader />
      )}
    </Ended>
  );
}
const Ended = styled.div`
  margin-top: -20px;
`;
const EndedItem = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  margin-bottom: 3px;
  position: relative;
  margin-bottom: 4px;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  font-size: 8px;
  line-height: 11px;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  background: #ffffff;
  border-radius: 20px;
  color: #ffffff;
  padding: 17px 15px;
  .wrapper-bottom {
    display: flex;
    align-items: center;
    width: 100%;
    .ava {
      width: 34px;

      img {
        width: 100%;
      }
    }
    .name-3 {
      color: #8f8f8f;
      background: #ffffff;
      border-radius: 20px;
      border: 1px solid #e4e4e4;
      border-radius: 20px;
      margin: 0 7px;
      display: flex;
      flex: 1;
      height: 34px;
      align-items: center;
      padding: 5px 10px;
      font-size: 15px;
      line-height: 20px;

      color: #8f8f8f;
    }
    .info {
      background: transparent;
      cursor: pointer;
    }
  }
  .wrapper-top {
    display: flex;
    align-items: center;
  }
  .winner {
    font-size: 12px;
    line-height: 16px;
    text-align: center;
    margin: 5px auto;
    color: #000000;
    font-weight: 700;
  }
  .avatar {
    min-width: 34px;
    background: #b0b1a1;
    border-radius: 50%;
    text-decoration: none;
    height: 34px;
    margin-bottom: 10px;
  }
  .name-2 {
    marging-bottom: 10px;
    min-width: 41px;
    background: #b0b1a1;
    border-radius: 20px;
    text-decoration: none;
    height: 41px;
    font-size: 12px;
    line-height: 16px;
    display: flex;
    align-items: center;
    text-align: center;
    height: 34px;
    color: #ffffff;
    justify-content: center;
    margin: 0 3px 10px;
    padding: 0 15px;
    flex: 1 1 40%;
  }
  .start-end {
    background: #b0b1a1;
    text-decoration: none;
    border-radius: 20px;

    height: 34px;

    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    font-size: 15px;
    line-height: 11px;
    margin: 0 3px 10px;
    flex: 1 1 5%;
    color: #ffffff;
    min-width: 62px;
  }
  .add {
    marging-bottom: 10px;
    border-radius: 20px;
    display: flex;
    min-width: 52px;
    font-size: 9px;
    line-height: 11px;
    color: #ffffff;
    height: 34px;
    background: #b0b1a1;
    border-radius: 20px;
    display: flex;
    text-decoration: none;
    text-align: center;
    line-height: 8px;
    margin: 0 3px 10px;
    flex: 1 1 3%;
    justify-content: center;
    align-items: center;
    img {
      height: 100%;
    }
  }
  .share {
    display: flex;
    margin: 0 3px 10px;
    justify-content: center;
    align-items: center;
  }
`;
