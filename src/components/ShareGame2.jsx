import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import HeaderControls from './organism/HeaderControls';
import Stepper from './molecules/Stepper';
import Button from './atoms/Button';
import SearchedUser from './molecules/SearchedUser';

import imgArrowBack from '../assets/images/svg/balck-arrow-back.svg';
import iconSearch from '../assets/images/svg/search.svg';

function ShareGame2() {
  const defaultLang = useSelector(selectLangLocale);
  return (
    <Container>
      {/* <HeaderControls /> */}

      <StyledLinkBack to="/share-1-step">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="share.game.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <p className="first-step">2. <FormattedMessage id="share.game.2"/></p>
        <WrapperBgPink>
          <SearchContainer>
            <input
              type="text"
              placeholder={vocabularyObject['messages.input.username'][defaultLang]}
            />
            <button>
              <img src={iconSearch} alt="" />
            </button>
          </SearchContainer>
          <div className="btns">
            <Button
              text="Люди"
              type="primary"
              fontSize="10px"
              width="60px"
              $minWidth="60px"
              height="24px"
            />
            <Button
              text="Клан"
              type="primary"
              fontSize="10px"
              width="60px"
              $minWidth="60px"
              height="24px"
            />
          </div>
          <OfferList>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
            <div className="item">
              <SearchedUser to="/share-3-step" shortenDescription />
            </div>
          </OfferList>

          <Stepper currentStep={2} />
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ShareGame2;

const OfferList = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  height: 278px;
  background: #ffffff;
  border: 1px solid #a4a4a4;
  border-radius: 20px;
  padding: 20px 22px;
  overflow: overlay;
  .item {
    display: flex;
    width: 50%;
    a {
      width: 100%;
      margin-bottom: 11px;
      cursor: pointer;
      .ava {
        width: 23px;
        height: 23px;
        display: flex;
        align-items: center;
        justify-content: center;
        img {
          width: 70%;
          height: 70%;
        }
      }
      .name {
        background: #e3e3e3;
        font-size: 10px;
        color: #565656;
        line-height: 13px;
      }
    }
  }
`;
const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  .first-step {
    color: #000;
    margin: 10px 0 28px;
    text-align: center;
    font-size: 20px;
    line-height: 27px;
  }
  .input-share {
    margin: 0 auto;
    width: 235px;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
    margin-bottom: 33px;
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 63px);
  margin: 0 auto;
  padding: 26px 8px 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .btns {
    display: flex;
    margin-bottom: 14px;
    a:last-child {
      margin-left: 22px;
    }
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
