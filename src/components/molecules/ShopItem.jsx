import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import FormattedMessage from '../atoms/FormattedMessage';

import { theme } from '../../assets/styles/variables';

function ShopItem({ name, price, discountedPrice = false, condition, description }) {
  return (
    <Item>
      <h2 className="title">{name || 'Shop tab'}</h2>
      <div className="body">
        <div className="price-block">
          <p className="main">{`${price}$` || 'Free'}</p>
          <div className={`discount ${!discountedPrice ? '' : 'cross'}`}>
            {discountedPrice || price}
          </div>
        </div>
        <div className="content">
          <p className="text">{description || 'Контингент'}</p>
          {/* <p className="text">{condition || 'Условие'}</p> */}
        </div>
        <Link to="/payment">
          <FormattedMessage id="shop.good.choose" />
        </Link>
      </div>
    </Item>
  );
}

export default ShopItem;

const Item = styled.div`
  display: flex;
  padding: 26px 12px 9px;
  align-items: center;
  flex: 1 1 calc(50% - 10px);
  max-width: calc(50% - 10px);
  margin-bottom: 15px;
  flex-direction: column;
  background-color: ${theme.primaryBg};
  border-radius: 20px;
  position: relative;
  font-family: 'Playfair Display', serif;

  &:nth-child(even) {
    margin-left: 20px;
  }
  .title {
    margin-top: 0;
    margin-bottom: 60px;
    font-size: 25px;
    line-height: 33px;
    color: #ffffff;
  }
  .body {
    background: #f7f4ea;
    border-radius: 50px 50px 40px 40px;
    position: relative;
    width: 100%;
    min-height: 228px;
    padding: 80px 12px 45px;

    .price-block {
      position: absolute;
      top: -44px;
      left: 50%;
      text-align: center;
      transform: translateX(-50%);
      .main {
        width: 88px;
        height: 88px;
        border-radius: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
        background: #ffffff;
      }
      .discount {
        font-size: 8px;
        line-height: 11px;
        display: flex;
        align-items: center;
        justify-content: center;
        background: #ffffff;
        border-radius: 15px;
        margin: 3px auto 0;
        width: 59px;
        height: 17px;
        color: #000000;
        postion: relative;
        &.cross {
          &::before {
            content: '';
            width: 54px;
            height: 1px;
            background-color: #fb3838;
            position: absolute;
            transform: rotate(-10deg);
          }
        }
      }
    }
    .content {
      min-height: 104px;
      .text {
        font-size: 15px;
        line-height: 20px;
        color: #8a8a8a;
        margin-bottom: 10px;
      }
    }
    a {
      display: block;
      width: 101px;
      height: 24px;
      background: #b0b1a1;
      border-radius: 20px;
      font-size: 12px;
      justify-content: center;
      align-items: center;
      line-height: 16px;
      text-decoration: none;
      display: flex;
      color: #ffffff;
      margin: auto auto 0;
    }
  }
  @media (max-width: 435px) {
    flex: 1 1 100%;
    max-width: 100%;
    &:nth-child(even) {
      margin-left: 0;
    }
  }
`;
