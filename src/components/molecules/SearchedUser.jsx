import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import FormattedMessage from '../atoms/FormattedMessage';
import Button from '../atoms/Button';

import iconUserSmall from '../../assets/images/user-profile/small-user-icon.svg';
import iconShare from '../../assets/images/game/VectorShare.svg';
import iconInfo from '../../assets/images/info.svg';

import { theme } from '../../assets/styles/variables';

function SearchedUser({ shortenDescription = false, to, showRating = false, callback, data = {} }) {
  return (
    <ChatItem 
    onClick={(e) =>{ 
      if(!to) {
        e.preventDefault()
      }
    }} 
    to={`${to || ''}`} 
    className="chat-item">
      <Avatar className="ava">
        <img src={iconUserSmall} alt="" />
      </Avatar>
      <Body className="chat-body">
        <p className="name">{`${data?.firstName || 'Имя'} ${data?.secondName || 'Фамилия'}`}</p>
        {!showRating ? (
          ''
        ) : (
          <button className="share-btn">
            <img src={iconShare} alt="" />
          </button>
        )}
      </Body>
      {!shortenDescription ? (
        <Button
          $borderRadius="13px"
          fontSize="8px"
          $minWidth="38px"
          height="33px"
          padding="0px 5px"
          defType="primary"
          text={<FormattedMessage id="searchedUser.btn.write" />}
          to="/chat"
          isLink
        />
      ) : (
        ''
      )}
      {!showRating ? (
        ''
      ) : (
        <button onClick={() => callback()} className="info-btn">
          <img src={iconInfo} alt="" />
        </button>
      )}
    </ChatItem>
  );
}

export default SearchedUser;

const ChatItem = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none;
  margin-bottom: 27px;
  .share-btn,
  .info-btn {
    padding: 0;
    border: none;
    background: transparent;
  }
  .share-btn {
    padding: 5px;
    background: ${theme.secondaryBg};
    border-radius: 50%;
    position: absolute;
    right: 14px;
    top: 5px;
  }
  .info-btn {
  }
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
`;
const UnreadMessages = styled.div`
  position: absolute;
  background: #a9a9a9;
  display: flex;
  top: -4px;
  left: -8px;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  font-size: 15px;
  line-height: 18px;
  width: 23px;
  height: 23px;
  font-family: 'Roboto', sans-serif;
  span {
    color: #000;
  }
`;
const Body = styled.div`
  flex: 1;
  position: relative;
  .name {
    margin-left: -9px;
    padding: 8px 9px;
    background: #ffffff;
    border-radius: 20px;
    width: 100%;
    height: 100%;
    color: ${({ shortenDescription }) => (!shortenDescription ? '#505050' : '#a9a9a9')};
    font-size: 15px;
  }
`;
