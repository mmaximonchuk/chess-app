import React from 'react';
import styled from 'styled-components';

function ModalContainer({ children, closeModal }) {
  return (
    <ModalWrapper>
      <ModalFallback onClick={() => closeModal((prev) => !prev)} />
      {children}
    </ModalWrapper>
  );
}

export default ModalContainer;
const ModalWrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 11;
`;
const ModalFallback = styled.div`
  position: fixed;
  &::before {
    content: '';
    position: fixed;
    left: 0;
    top: 0;
    width: 100vw;
    height: 100vh;
    background-color: #000;
    opacity: 0.6;
    z-index: 10;
  }
`;
