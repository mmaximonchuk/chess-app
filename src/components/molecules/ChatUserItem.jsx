import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { setChatUser } from '../../features/message/messageSlice';

function ChatUserItem(props) {
  return (
    <ChatItem to={`/chat/${props.senderUserID}`}>
      <Avatar>
        {props.message.unreadMessages && (
          <UnreadMessages>
            <span>{props.message.unreadMessages}</span>
          </UnreadMessages>
        )}
      </Avatar>
      <Body>
        <p className="name">
          {props.message.senderFullName.trim().length === 0
            ? 'Имя Фамилия'
            : props.message.senderFullName}
        </p>
        <p className="message">Сообщение</p>
      </Body>
      <Detector />
    </ChatItem>
  );
}

export default ChatUserItem;

const ChatItem = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none;
  margin-bottom: 31px;
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
`;
const UnreadMessages = styled.div`
  position: absolute;
  background: #a9a9a9;
  display: flex;
  top: -4px;
  left: -8px;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  font-size: 15px;
  line-height: 18px;
  width: 23px;
  height: 23px;
  font-family: 'Roboto', sans-serif;
  span {
    color: #000;
  }
`;
const Body = styled.div`
  flex: 1;
  .message {
    margin-left: -9px;
    padding: 8px 9px;
    background: #ffffff;
    border-radius: 20px;
    width: 100%;
    color: #a9a9a9;
  }
  .name {
    color: #343434;
    margin-bottom: 3px;
    font-size: 15px;
  }
`;
const Detector = styled.div`
  background: #b0b1a1;
  border-radius: 50%;
  width: 15px;
  height: 15px;
  margin-left: 5px;
  transform: translateY(-3px);
`;
