import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { getGUID } from '../../utils/getGUID';
import { gameSetupRequest } from '../../features/game/gameSlice';
import FormattedMessage from '../atoms/FormattedMessage';
import Button from '../atoms/Button';

import iconUserAdd from '../../assets/images/user-profile/user-add.svg';
import iconUserSearch from '../../assets/images/user-profile/user-search.svg';
import iconUserCross from '../../assets/images/user-profile/user-cross.svg';
import iconUserI from '../../assets/images/user-profile/user-i.svg';

function CompetitorItem(props) {
  const handleCallBack = () => {
    props.callBack()
  }

  return (
    <Item>
      <Button
        $borderRadius="20px"
        fontSize="14px"
        $minWidth="136px"
        height="41px"
        padding="0px 5px"
        defType="primary"
        text={<FormattedMessage id="games.game"/>}
        to="/game"
        isLink
      />
      <Body>
        <Link to="/create-game">
          <img src={iconUserAdd} alt="" />
        </Link>
        <Link to="/find-partner">
          <img src={iconUserSearch} alt="" />
        </Link>
        <Link to="/create-game">
          <img src={iconUserCross} alt="" />
        </Link>
        <Link onClick={handleCallBack}>
          <img src={iconUserI} alt="" />
        </Link>
      </Body>
    </Item>
  );
}

export default CompetitorItem;

const Item = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  margin-bottom: 27px;
  @media (max-width: 405px) {
    flex-direction: column;
  }
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
`;

const Body = styled.div`
  a {
    background-color: transparent;
    border: none;
    height: 40px;
    width: 40px;
    padding: 0;
    margin-left: 8px;
    display: inline-flex;
    img {
      width: 100%;
      height: 100%;
    }
  }
  @media (max-width: 405px) {
    margin-top: 1vh;
  }
`;
