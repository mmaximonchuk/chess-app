import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

function Stepper({ currentStep }) {
  return (
    <StepperContainer>
      <div className="top">
        <Link to="/share-1-step" className={`unit ${currentStep >= 1 ? 'done' : ''}`}>1</Link>
        <div className="line"></div>
        <Link to="/share-2-step" className={`unit ${currentStep > 1 ? 'done' : ''}`}>2</Link>
        <div className="line"></div>
        <Link to="/share-3-step" className={`unit ${currentStep > 2 ? 'done' : ''}`}>3</Link>
      </div>
      <div className="bottom">
        <p className="text">
          Выберите что <br /> шерить
        </p>
        <p className="text"> Выберите кому шерить</p>
        <p className="text">
          Выбеите как
          <br /> шерить
        </p>
      </div>
    </StepperContainer>
  );
}

export default Stepper;

const StepperContainer = styled.div`
  margin-top: auto;
  background: #ffffff;
  border-radius: 50px;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 8px 25px;
  flex-direction: column;
  .top {
    display: flex;
    align-items: center;
    width: 100%;
    justify-content: space-between;
    .unit {
      min-width: 48px;
      min-height: 48px;
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      background: #ddded4;
      font-size: 15px;
      line-height: 20px;
      margin: 0 5px;
      text-decoration: none;
      color: #000;

      &.done {
        background: #b0b1a1;
        color: #ffffff;
      }
    }
    .line {
      border-top: 2px solid #9a9a9a;
      display: flex;
      width: 100%;
    }
  }
  .bottom {
    display: flex;
    align-items: center;
    width: 100%;
    justify-content: space-between;
    font-size: 10px;
    line-height: 13px;
    .text {
      text-align: center;
    }
  }
`;
