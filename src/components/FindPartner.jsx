import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import HeaderControls from './organism/HeaderControls';

import SearchedUser from './molecules/SearchedUser';
import iconSearch from '../assets/images/svg/search.svg';

function FindPartner() {
  const defaultLang = useSelector(selectLangLocale);
  return (
    <Container>
      <StyledLinkBack to="/profile">
        <span><FormattedMessage id="partner.title"/></span>
      </StyledLinkBack>
      <SearchContainer>
        <input type="text" placeholder={vocabularyObject['messages.input.username'][defaultLang]} />
        <button>
          <img src={iconSearch} alt="" />
        </button>
      </SearchContainer>
      <WrapperBgTurquoise>
        <BtnsWrapper>
          <Button text={<FormattedMessage id="partner.people"/>} defType="primary" height="35px" width="30%" padding="10px 8px" />
          <Button text={<FormattedMessage id="partner.clan"/>} defType="primary" height="35px" width="30%" padding="10px 8px" />
          <Button
            text={<FormattedMessage id="partner.invite"/>}
            defType="primary"
            height="35px"
            width="30%"
            padding="10px 8px"
          />
        </BtnsWrapper>
        <WrapperBgPink>
          <SearchedUser shortenDescription />
          <SearchedUser shortenDescription />
          <SearchedUser shortenDescription />
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default FindPartner;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 144px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 60px);
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 25px;
  @media (max-width: 342px) {
    flex-direction: column;
    height: 120px;
    a {
      width: 100%;
    }
  }
`;
