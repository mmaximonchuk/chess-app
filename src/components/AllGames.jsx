import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import HeaderControls from './organism/HeaderControls';
import { gameSetupRequest, selectGameSetup } from '../features/game/gameSlice';
import Modal from './molecules/ModalContainer';

// import imgArrowBack from '../assets/images/svg/balck-arrow-back.svg';
import CompetitorItem from './molecules/CompetitorItem';
import iconUser from '../assets/images/user-profile/default-user.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iconUserI from '../assets/images/user-profile/user-i2.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg'
import { theme } from '../assets/styles/variables';

function AllGames() {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [openedAccordeons, setOpenedAccordeons] = useState([]);
  const [gameDescription, setGameDescription] = useState('');

  const gameData = useSelector(selectGameSetup);
  const gameMainCategories = gameData?.gameMainCategories;
  const activeGames = gameData?.games;

  const handleAccordeonToOpen = (itemId) => {
    if (!openedAccordeons.includes(itemId)) {
      const { mainGameID } = gameMainCategories.filter((item) => itemId === item.mainGameID)[0];
      setOpenedAccordeons([mainGameID]);
    } else {
      const newItemToReset = openedAccordeons.filter((mainGameID) => itemId !== mainGameID);
      setOpenedAccordeons([...newItemToReset]);
    }
  };

  const handleGameCategoriesRequest = () => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(gameSetupRequest({ Token: token }));
  };
  useEffect(() => {
    handleGameCategoriesRequest();
  }, []);
  useEffect(() => {
    handleGameCategoriesRequest();
  }, [dispatch]);
  // console.log(gameMainCategories.mainGameDescription);
  return (
    <Container>
      {/* <HeaderControls /> */}
      {openModal && (
        <Modal closeModal={setOpenModal}>
          <ModalInner>
            <img onClick={() => setOpenModal(prev => !prev)} className="c-btns" src={iCloseBtn} alt="" />
            {openModal ? (
              <button className="select-category-btn">
                <span>{gameDescription}</span>
              </button>
            ) : (
              ''
            )}
          </ModalInner>
        </Modal>
      )}
      <StyledUserInfoWrapper>
        <span>
          <FormattedMessage id="welcome.par.welcome" />
        </span>
        <UserInnerWrap>
          <Stats>
            <p>24-<FormattedMessage id="welcome.par.wins" /></p>
            <p>4-<FormattedMessage id="welcome.par.loses" /></p>
          </Stats>
          <User to="/profile">
            <img src={iconUser} alt="iconUser" />
            <UserRating>133</UserRating>
          </User>
          <Stats>
            <p>
            <FormattedMessage id="welcome.par.playedMatches" /> <br /> 24
            </p>
          </Stats>
        </UserInnerWrap>
        <p className="user-name">Имя/Фамилия</p>
      </StyledUserInfoWrapper>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          <p className="choice-competitor"><FormattedMessage id="welcome.par.selectFromList" /></p>
          {gameMainCategories ? (
            gameMainCategories.map((item) => {
              return (
                <Collapse key={item.mainGameID}>
                  <div
                    className="trigger"
                    role="button"
                    onClick={(e) => {
                      if (e.target.classList.value === 'collapse-info') {
                        setGameDescription(item.mainGameDescription);
                        setOpenModal(!openModal);
                      } else {
                        handleAccordeonToOpen(item.mainGameID);
                      }
                    }}
                  >
                    {item.mainGameName}
                    <img
                      src={iconArrow}
                      alt="iconArrow"
                      className={`collapse-arrow ${
                        openedAccordeons.includes(item.mainGameID) ? 'active' : ''
                      }`}
                    />
                    <img src={iconUserI} alt="iconArrow" className={`collapse-info`} />
                  </div>
                  <div
                    className={`collapse-body ${
                      openedAccordeons.includes(item.mainGameID) ? 'active' : ''
                    }`}
                  >
                    <div className="inner">
                      <div className="details">
                        {/* <img className="arrow-back" src={iconArrowGrey} alt="iconArrowGrey" /> */}
                        <div className="content">
                          <div className="controls"></div>
                        </div>
                      </div>
                      <>
                        {activeGames
                          ? activeGames
                              .filter((game) => game.mainGameID === item.mainGameID)
                              .map((game, index) => {
                                return (
                                  <CompetitorItem
                                    className="item"
                                    key={index + 1}
                                    callBack={() => {
                                      setOpenModal((prev) => !prev);
                                      setGameDescription(game.description);
                                    }}
                                  />
                                );
                              })
                          : ''}
                      </>
                    </div>
                  </div>
                </Collapse>
              );
            })
          ) : (
            <Loader>
              <div class="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </Loader>
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default AllGames;

const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const Collapse = styled.div`
  margin-bottom: 20px;
  .board {
    .chess-board {
      max-width: 141px;
    }
  }
  .trigger {
    background: ${theme.secondaryBg};
    min-height: 42px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 12px;
    position: relative;
    padding: 10px 60px 10px 10px;
    cursor: pointer;
    .collapse-arrow {
      right: 49px;
      top: 18px;
      position: absolute;
      transition: all 0.3s;
      &.active {
        transform: rotate(180deg);
      }
    }
    .collapse-info {
      right: 6px;
      top: 2px;
      position: absolute;
    }
  }
  .collapse-body {
    width: calc(100% - 20px);
    margin: 0 auto;
    border-radius: 0 0 50px 50px;
    background-color: #d1e6e7;
    padding: 0 12px 10px;
    height: 0;
    opacity: 0;
    overflow: hidden;
    transform: scaleY(0);
    transform-origin: top;
    position: relative;
    .share {
      position: absolute;
      display: block;
      left: 50%;
      bottom: -30px;
      transform: translateX(-50%);
      font-size: 10px;
      height: 24px;
      line-height: 13px;
      display: flex;
      align-items: center;
      text-align: center;
      background: ${theme.secondaryBg};
      border-radius: 20px;
      border: none;
      color: #ffffff;
      img {
        width: 14px;
        height: 12.5px;
        margin-left: 3px;
      }
    }
    &.active {
      height: 100%;
      opacity: 1;
      overflow: visible;
      animation: faze 300ms ease;
      transform: scaleY(1);
      margin-bottom: 50px;
    }

    .inner {
      border-radius: 0 0 50px 50px;
      background-color: #f7f4ea;
      padding: 20px 20px;
      .details {
        position: relative;
        .arrow-back {
          position: absolute;
          top: 0;
          left: 0;
          width: 12px;
        }
        .controls {
          display: flex;
          justify-content: center;
          margin: 0 auto;
          button {
            padding: 0 8px;
            border: none;
            background: transparent;
          }
          .up-down {
            img:first-child {
              transform: translateY(5px);
            }
            img:last-child {
              transform: translateY(-5px);
            }
          }
        }
      }
      .item {
        display: flex;
        justify-content: space-between;
        color: #000000;
        margin-bottom: 20px;
        .number {
          min-width: 25px;
        }
        .number,
        .name,
        .start-btn {
          background: #b0b1a1;
          border-radius: 20px;
          display: flex;
          justify-content: center;
          align-items: center;
          height: 22px;
          font-size: 10px;
          line-height: 13px;
        }
        .name {
          margin: 0 10px;
          flex: 1 1 80%;
          padding-right: 0;
        }
        .start-btn {
          width: 59px;
          border: none;
        }
      }
    }
  }
  @keyframes faze {
    0% {
      opacity: 1;
      transform: scaleY(0);
    }
    100% {
      transform: scaleY(1);
      overflow: visible;
    }
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 300px;
  width: 60%;
  min-height: 170px;
  padding: 21px 11px;
  background: #e5e5e5;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  .c-btns {
    position: absolute;
    top: 5px;
    right: 5px;
    cursor: pointer;
  }
  span {
    font-size: 20px;
    line-height: 27px;
    color: #000000;
  }
  .select-category-btn {
    cursor: pointer;
    height: 83px;
    width: 100%;
    padding: 6px 30px;
    font-size: 18px;
    line-height: 24px;
    border: none;
    background: transparent;
    display: flex;
    align-items: center;
    margin-bottom: 11px;
    span {
      width: 100%;
      text-align: center;
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 280px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 45px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: 100%;
  margin: 0 auto;
  padding: 5px 22px 19px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .choice-competitor {
    text-align: center;
    margin-bottom: 2vh;
    font-size: 20px;
    line-height: 27px;
    color: #444;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledUserInfoWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 0;
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
    display: block;
    margin-bottom: 1vh;
  }
  .user-name {
    margin-top: 39px;
    font-size: 16px;
    line-height: 21px;
    margin-bottom: 16px;
    color: #000000;
  }
`;

const UserInnerWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  min-width: 266px;
  margin: 0 auto;
`;
const Stats = styled.div`
  background: ${theme.secondaryBg};
  border-radius: 20px;
  min-width: 64px;
  min-height: 33px;
  padding: 5px 5px;
  font-size: 8px;
  line-height: 9px;
  color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;
const User = styled(Link)`
  background: ${theme.secondaryBg};
  border-radius: 50%;
  padding: 19px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  img {
    width: 70px;
    height: 70px;
  }
`;
const UserRating = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${theme.secondaryBg};
  border-radius: 50%;
  width: 56px;
  height: 56px;
  color: #fff;
  left: 50%;
  bottom: -25px;
  transform: translateX(-50%);
`;

// const SearchContainer = styled.div`
//   button {
//     margin-left: 5px;
//     background: #b0b1a1;
//     border-radius: 20px;
//     border: none;
//     width: 32px;
//     height: 24px;
//     display: flex;
//     align-items: center;
//     justify-content: center;
//   }
// `;
