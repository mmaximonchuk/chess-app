import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { langs } from '../utils/languages';
import { permissions } from '../utils/permissions';
import { setLang } from '../features/langs/langsSlice';
import { selectLangLocale } from '../features/langs/langsSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import { selectUserData, updateUserSettingsRequest } from '../features/user/userSlice';
import Input from './atoms/Input';
import Button from './atoms/Button';
import Select from './atoms/Select';
import FormattedMessage from './atoms/FormattedMessage';
import Modal from './molecules/ModalContainer';
import HeaderControls from './organism/HeaderControls';

import imgArrowBack from '../assets/images/svg/arrow-back.svg';
import imgDefaultUser from '../assets/images/user-profile/default-user.svg';
import imgEdit from '../assets/images/user-profile/edit.svg';
import imgAdd from '../assets/images/user-profile/add.svg';
import imgDelete from '../assets/images/user-profile/delete.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg'
import { theme } from '../assets/styles/variables';
import { StartContainer } from './styled';

function Profile() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isModalOpen, setModalOpen] = useState(false);
  const defaultLang = useSelector(selectLangLocale);
  const userData = useSelector(selectUserData);
  const [formData, setFormData] = useState({
    Token: '',
    FirstName: '',
    SecondName: '',
    Password: '',
    ImageURL: '',
    Image: '',
    AllowedInvitation: 1,
    AllowedSearch: 1,
  });

  const handleInputs = (e) => {
    // debugger;
    if (!e?.target) {
      setFormData({
        ...formData,
        [e.name]: e.valueInIndex,
      });
    } else {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    }

    console.log(formData);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateUserSettingsRequest(formData));
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');
    if (!token) {
      token = "tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7"

      localStorage.setItem('token', token);
    }
    setFormData({
      ...formData,
      Password: userData?.password,
      FirstName: userData?.firstName,
      SecondName: userData?.secondName,
      AllowedInvitation: userData?.allowedInvitation,
      AllowedSearch: userData?.allowedSearch,
      Token: token,
    });
  }, []);
  useEffect(() => {
    if(userData.userID === '') {
      history.push('/entrance')
    }
  }, [userData]);
  const dot = (img = langs[defaultLang].flag) => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
      background: `url(${img})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'contain',
      borderRadius: 20,
      content: '" "',
      display: 'block',
      height: 37,
      width: 50,
    },
  });

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });

  const searchStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      width: '198px',
      cursor: 'pointer',
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '173px',
      fontSize: 10,
      justifyContent: 'space-between',
      color: '#000',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '173px',
      fontSize: 10,

      justifyContent: 'space-between',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };

  const colourStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      border: 'none',
      cursor: 'pointer',
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '5px 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      ...dot(),
      textAlign: 'center',
      width: '150px',
      justifyContent: 'space-between',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;
      return {
        ...styles,
        // ...dot(img),
        cursor: isDisabled ? 'not-allowed' : 'default',
        ':active': {
          ...styles[':active'],
          backgroundImage: isDisabled ? null : isSelected ? `url(${img})` : null,
        },
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        display: 'flex',
        color: theme.languagesColor,
        ':before': {
          background: `url(${img})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'contain',
          borderRadius: 20,
          content: '" "',
          display: 'block',
          marginRight: 35,
          height: 37,
          width: 50,
        },
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      ...dot(data.flagImg),
      width: '150px',
      justifyContent: 'space-between',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };
  return (
    <StartContainer>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/all-games">
        <img src={imgArrowBack} alt="imgArrowBack" />
      </StyledLinkBack>

      <PersonAvatar>
        <img src={imgDefaultUser} alt="Avatar" />
      </PersonAvatar>
      <Wrapper>
        <PersonPreferences>
          <img src={imgEdit} alt="" />
        </PersonPreferences>
        <PersonPreferences>
          <img src={imgAdd} alt="" />
        </PersonPreferences>
        <PersonPreferences>
          <img src={imgDelete} alt="" />
        </PersonPreferences>
      </Wrapper>
      <form onSubmit={handleSubmit}>
        <Input
          bgColor="#fff"
          type="edit"
          name="FirstName"
          placeholder={vocabularyObject['profile.input.name'][defaultLang]}
          onChange={handleInputs}
        />
        <Input
          bgColor="#fff"
          type="edit"
          name="SecondName"
          placeholder={vocabularyObject['profile.input.surname'][defaultLang]}
          onChange={handleInputs}
        />
        <Button
          height="39px"
          margin="2vh 0 1vh 0"
          padding="0 38px"
          defType="no-bg"
          text={<FormattedMessage id="profile.input.change.email" />}
          to="/email-change"
        />
        <Button
          height="39px"
          margin="2vh 0 2vh 0"
          padding="0 38px"
          defType="no-bg"
          text={<FormattedMessage id="profile.input.change.password" />}
          to="/password-change"
        />
        <NotificationButton onClick={() => setModalOpen(!isModalOpen)}>
          <span>{<FormattedMessage id="profile.select.access" />}</span>
          <img src={iconArrow} alt="" />
        </NotificationButton>
        {isModalOpen && (
          <Modal closeModal={setModalOpen}>
            <ModalInner>
              <img onClick={() => setModalOpen(prev => !prev)} className="c-btns" src={iCloseBtn} alt="" />
              <NotificationWrapper>
                <p>{<FormattedMessage id="profile.modal.description" />}</p>
              </NotificationWrapper>
              <BtnContainer>
                <Button
                  fontWeight={700}
                  fontSize="15px"
                  height="47px"
                  padding="0px 0px"
                  $minWidth="57px"
                  defType="primary"
                  text={<FormattedMessage id="profile.modal.btn.yes" />}
                  onClick={() => {
                    setFormData({ ...formData, AllowedSearch: 1 });
                    setModalOpen(!isModalOpen);
                  }}
                />
                <Button
                  fontWeight={700}
                  fontSize="15px"
                  $minWidth="57px"
                  height="47px"
                  padding="0px 0px"
                  defType="primary"
                  text={<FormattedMessage id="profile.modal.btn.no" />}
                  onClick={() => {
                    setFormData({ ...formData, AllowedSearch: 2 });
                    setModalOpen(!isModalOpen);
                  }}
                />
              </BtnContainer>
            </ModalInner>
          </Modal>
        )}

        <div>
          <Select
            defaultValue={permissions[0]}
            placeholder={'видимость в поиске'}
            options={permissions}
            styles={searchStyles}
            name="AllowedInvitation"
            onChange={handleInputs}
          />
        </div>
        <div>
          <LangPar>{<FormattedMessage id="start.lang.choice" />}</LangPar>
          <Select
            onChange={(selectedOption) => dispatch(setLang(selectedOption.value))}
            defaultValue={defaultLang}
            placeholder={langs[defaultLang].value}
            options={langs}
            styles={colourStyles}
          />
        </div>

        <Button
          fontWeight={700}
          fontSize="20px"
          height="52px"
          margin="2vh 0 0 0"
          padding="0 38px"
          defType="primary"
          text={<FormattedMessage id="profile.btn.ready" />}
          // to="/profile"
          type="submit"
        />
      </form>
    </StartContainer>
  );
}

export default Profile;

const PersonAvatar = styled.button`
  background: ${theme.secondaryBg};
  border-radius: 50%;
  width: 194px;
  height: 194px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2vh;
  border: none;
  cursor: pointer;
`;
const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 160px;
  margin-bottom: 5vh;
`;
const PersonPreferences = styled.button`
  background: ${theme.secondaryBg};
  border-radius: 50%;
  width: 42px;
  height: 42px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: none;
`;
const StyledLinkBack = styled(Link)`
  position: absolute;
  top: 80px;
  left: 8vw;
`;
const LangPar = styled.p`
  font-size: 14px;
  line-height: 19px;
  margin-bottom: 2vh;
`;
const NotificationButton = styled.div`
  font-size: 10px;
  background-color: #fff;
  width: 198px;
  padding: 0 20px 0 5px;
  height: 35px;
  border: none;
  border-radius: 12px;
  position: relative;
  margin-bottom: 2vh;
  display: flex;
  align-items: center;
  cursor: pointer;
  img {
    position: absolute;
    right: 5px;
  }
  span {
    margin: 0 auto;
  }
`;
const BtnContainer = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 40%;
`;
const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-width: 197px;
  width: 60%;
  min-height: 170px;
  padding: 6px 6px 12px;
  background-color: #fff;
  z-index: 12;
  transform: translateY(-50%);
  .c-btns {
    position: absolute;
    top: 15px;
    right: 15px;
    cursor: pointer;
  }
`;
const NotificationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 90px;
  border: 1px solid ${theme.inputBorderColor};
  border-radius: 20px;
`;
