import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import Slider from 'react-slick';
// import '~slick-carousel/slick/slick.css';
// import '~slick-carousel/slick/slick-theme.css';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import { getPuzzlesRequest, selectPuzzles } from '../features/puzzles/puzzleSlice';
import ChessBoard from './organism/ChessBoard';
import { selectUsers, getUsersRequest } from '../features/game/gameSlice';
import Loader from './atoms/Loader';

import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconUser from '../assets/images/blogs/person.svg';

function PuzzlesCreate() {
  // const defaultLang = useSelector(selectLangLocale);
  const puzzles = useSelector(selectPuzzles);
  const users = useSelector(selectUsers);
  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getPuzzlesRequest({ Token: token }));
    dispatch(getUsersRequest({ Token: token }));
  }, []);

  const settings = {
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: '576px',
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  return (
    <Container>
      <WrapperBgTurquoise>
        <StyledLinkBack to="/puzzles">
          <img src={imgArrowBack} alt="imgArrowBack" />
          <span>
            <FormattedMessage id="share.puzzles.title" />
          </span>
        </StyledLinkBack>
        <WrapperBgPink>
          {puzzles ? (
            <ImgSlider {...settings}>
              {puzzles &&
                users &&
                puzzles.map((puzzle) => {
                  let currentUser;
                  for (let i = 0; i < users.length; i++) {
                    if (users[i].userID === puzzle.creatorUserID) currentUser = users[i];
                  }
                  // console.log(currentUser);

                  return (
                    <div>
                      <PazzleItem
                        firstName={currentUser.firstName}
                        secondName={currentUser.secondName}
                        position={puzzle.fen}
                      />
                    </div>
                  );
                })}
            </ImgSlider>
          ) : (
            <Loader />
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}
export default PuzzlesCreate;
const ImgSlider = styled(Slider)`
  margin-top: 30px;
  .slick-slide {
    // width: 276px !important;
    // margin-left: 15px;
  }
  .slick-track {
    display: flex;
  }
  @media (max-width: 576px) {
    .slick-slide {
      width: 276px !important;
      margin-right: 15px;
    }
  }
`;
const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 70px auto 0;
  height: calc(var(--app-height) - 71px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 0 18px;
  overflow: hidden;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 53px);
  margin: 0 auto;
  padding: 10px 22px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .lds-ring {
    transform: translate(-50%, 100%);
  }
`;

const Container = styled.div`
  margin-top: auto;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  max-width: 414px;
  margin: 0 auto 22px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

function PazzleItem({ position, firstName, secondName }) {
  return (
    <PuzzleItem>
      <div className="user">
        <img src={iconUser} alt="" />
        {`${firstName || 'Имя'} ${secondName || 'Фамилия'}`}
      </div>
      <ChessBoard width="199px" position={position} />
      <Link to="/game" className="game">
        <FormattedMessage id="share.puzzles.play" />
      </Link>
    </PuzzleItem>
  );
}

const PuzzleItem = styled.div`
  max-width: 276px;
  width: 100%;
  background: #d1e6e7;
  border-radius: 50px;
  padding: 33px;
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 404px;
  .user {
    display: flex;
    align-items: center;
    margin-bottom: 24px;
    font-size: 12px;
    line-height: 16px;
    align-self: flex-start;
    color: #424242;
    img {
      width: 41px;
      height: 41px;
      margin-right: 8px;
    }
  }
  .game {
    font-size: 20px;
    line-height: 27px;
    text-align: center;
    text-decoration: none;
    background: #b0b1a1;
    border-radius: 20px;
    margin: 0 auto;
    display: inline-block;
    padding: 5px 15px;
    width: auto;
    color: #ffffff;
  }
`;
