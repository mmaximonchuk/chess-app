import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
// import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import FormattedMessage from '../components/atoms/FormattedMessage';

import iLine from '../assets/images/league/lines.svg';
import i20 from '../assets/images/league/20.svg';
import i19 from '../assets/images/league/19.svg';
import i18 from '../assets/images/league/18.svg';
import i17 from '../assets/images/league/17.svg';
import i16 from '../assets/images/league/16.svg';
import i15 from '../assets/images/league/15.svg';
import i14 from '../assets/images/league/14.svg';
import i13 from '../assets/images/league/13.svg';
import i12 from '../assets/images/league/12.svg';
import i11 from '../assets/images/league/11.svg';
import i10 from '../assets/images/league/10.svg';
import i9 from '../assets/images/league/9.svg';
import i8 from '../assets/images/league/8.svg';
import i7 from '../assets/images/league/7.svg';
import i6 from '../assets/images/league/6.svg';
import i5 from '../assets/images/league/5.svg';
import i4 from '../assets/images/league/4.svg';
import i3 from '../assets/images/league/3.svg';
import i2 from '../assets/images/league/2.svg';
import i1 from '../assets/images/league/1.svg';

import { theme } from '../assets/styles/variables';

function League() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);

  return (
    <Container>
      <StyledLinkBack to="/shop">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span>League overview</span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          <div className="inner">
            <div className="box box--20">
              <img src={i20} alt="" />
            </div>
            <div className="box box--19">
              <img src={i19} alt="" />
            </div>
            <div className="box box--18">
              <img src={i18} alt="" />
            </div>
            <div className="box box--17">
              <img src={i17} alt="" />
            </div>
            <div className="box box--16">
              <img src={i16} alt="" />
            </div>
            <div className="box box--15">
              <img src={i15} alt="" />
            </div>
            <div className="box box--14">
              <img src={i14} alt="" />
            </div>
            <div className="box box--13">
              <img src={i13} alt="" />
            </div>
            <div className="box box--12">
              <img src={i12} alt="" />
            </div>
            <div className="box box--11">
              <img src={i11} alt="" />
            </div>
            <div className="box box--10">
              <img src={i10} alt="" />
            </div>
            <div className="box box--9">
              <img src={i9} alt="" />
            </div>
            <div className="box box--8">
              <img src={i8} alt="" />
            </div>
            <div className="box box--7">
              <img src={i7} alt="" />
            </div>
            <div className="box box--6">
              <img src={i6} alt="" />
            </div>
            <div className="box box--5">
              <img src={i5} alt="" />
            </div>
            <div className="box box--4">
              <img src={i4} alt="" />
            </div>
            <div className="box box--3">
              <img src={i3} alt="" />
            </div>
            <div className="box box--2">
              <img src={i2} alt="" />
            </div>
            <div className="box box--1">
              <img src={i1} alt="" />
            </div>
            <img className="line" src={iLine} alt="" />
          </div>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default League;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100%);
  margin: 0 auto;
  padding: 230px 22px 50px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .inner {
    position: relative;
    .line {
      width: 69%;
      margin: 0 auto;
      display: block;
    }
  }
  .box {
    background: #ffffff;
    border: 1px solid #949494;
    border-radius: 20px;
    overflow: visible;
    position: absolute;
    height: 84px;
    width: 84px;
    img {
      transform: translate(8px, -64%);
    }
    &--3 {
      top: 1261px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(5px, -7%);
      }
    }
    &--2 {
      top: 1261px;
      left: 50%;
      transform: translateX(-50%);
      img {
        transform: translate(13px,-30%);
      }
    }
    &--1 {
      top: 1261px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(16px, -29%);
      }
    }
    &--6 {
      top: 1076px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(5px, -7%);
      }
    }
    &--5 {
      top: 1076px;
      left: 50%;
      transform: translateX(-50%);
      img {
        transform: translate(2px, -12%);
      }
    }
    &--4 {
      top: 1076px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(-4px, -9%);
      }
    }
    &--9 {
      top: 885px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(7px, 3%);
      }
    }
    &--8 {
      top: 885px;
      left: 50%;
      transform: translateX(-50%);
      img {
        transform: translate(-4px, 14%);
      }
    }
    &--7 {
      top: 885px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(-11px, -9%);
      }
    }
    &--12 {
      top: 704px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(-2px, -23%);
      }
    }
    &--11 {
      top: 704px;
      left: 50%;
      transform: translateX(-50%);
      img {
        transform: translate(-4px, -15%);
      }
    }
    &--10 {
      top: 704px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(-6px, -9%);
      }
    }
    &--13 {
      top: 514px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(-16px, -21%);
      }
    }
    &--14 {
      top: 514px;
      left: 50%;
      transform: translateX(-50%);
      img {
        transform: translate(2px, -15%);
      }
    }
    &--15 {
      top: 514px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(3px, -29%);
      }
    }
    &--17 {
      top: 325px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(-18px, -39%);
      }
    }
    &--16 {
      top: 325px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(6px, -35%);
      }
    }
    &--20 {
      height: 110px;
      width: 110px;
      top: -52px;
      left: 50%;
      transform: translateX(-50%);
    }
    &--19 {
      top: 133px;
      left: 34%;
      transform: translateX(-50%);
      img {
        transform: translate(-8px, -39%);
      }
    }
    &--18 {
      top: 133px;
      left: 66%;
      transform: translateX(-50%);
      img {
        transform: translate(-8px, -39%);
      }
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
