import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import ChessBoard from './organism/ChessBoard';
import { gameHistoriesRequest, selectGameHistories } from '../features/game/gameSlice';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iconArrowGrey from '../assets/images/svg/arrow-back-grey.svg';
import iconVector from '../assets/images/shareTo/Vector.svg';

import iK from '../assets/images/games/K.svg';
import iKR from '../assets/images/games/KR.svg';
import iL from '../assets/images/games/left.svg';
import iR from '../assets/images/games/right.svg';
import iU from '../assets/images/games/up.svg';
import iD from '../assets/images/games/down.svg';

import { theme } from '../assets/styles/variables';

const accordeonData = [
  {
    id: 1,
    title: 'Таблица незаконченных игр',
  },
  {
    id: 2,
    title: 'Таблица незаконченных игр',
  },
  {
    id: 3,
    title: 'Таблица незаконченных игр',
  },
];

function GameHistory() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const gameHistory = useSelector(selectGameHistories);
  const [openedAccordeons, setOpenedAccordeons] = useState([]);
  const [visibleDetails, setVisibleDetails] = useState([]);
  // let uncompleted = gameHistory.filter(item => item.gameResult === 0)
  // let histories = gameHistory.filter(item => item.gameResult > 0)

  const handleAccordeonToOpen = (itemId) => {
    if (!openedAccordeons.includes(itemId)) {
      const { id } = accordeonData.filter((item) => itemId === item.id)[0];
      setOpenedAccordeons([...openedAccordeons, id]);
    } else {
      const newItemToReset = openedAccordeons.filter((id) => itemId !== id);
      setOpenedAccordeons([...newItemToReset]);
    }
  };
  const handleVisibleDetails = (itemId) => {
    if (!visibleDetails.includes(itemId)) {
      const { id } = accordeonData.filter((item) => itemId === item.id)[0];
      setVisibleDetails([...visibleDetails, id]);
    } else {
      const newItemToReset = visibleDetails.filter((id) => itemId !== id);
      setVisibleDetails([...newItemToReset]);
    }
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(gameHistoriesRequest({ Token: token }));
    // dispatch(getNewsRequest({ Token: token }));
  }, []);

  return (
    <Container>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="#">
        <span><FormattedMessage id="history.title" /></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          {accordeonData.map((item) => {
            return (
              <Collapse key={item.id}>
                <div
                  className="trigger"
                  role="button"
                  onClick={() => handleAccordeonToOpen(item.id)}
                >
                  {item.title}
                  <img
                    src={iconArrow}
                    alt="iconArrow"
                    className={`collapse-arrow ${
                      openedAccordeons.includes(item.id) ? 'active' : ''
                    }`}
                  />
                </div>
                <div
                  className={`collapse-body ${openedAccordeons.includes(item.id) ? 'active' : ''}`}
                >
                  <div className="inner">
                    {visibleDetails.includes(item.id) ? (
                      <div className="details">
                        <img
                          className="arrow-back"
                          src={iconArrowGrey}
                          alt="iconArrowGrey"
                          onClick={() => handleVisibleDetails(item.id)}
                        />
                        <div className="content">
                          <div className="board">
                            <ChessBoard></ChessBoard>
                          </div>
                          <div className="controls">
                            <button>
                              <img src={iK} alt="" />
                            </button>
                            <button>
                              <img src={iL} alt="" />
                            </button>
                            <button className="up-down">
                              <img src={iD} alt="" />
                              <img src={iU} alt="" />
                            </button>
                            <button>
                              <img src={iR} alt="" />
                            </button>
                            <button>
                              <img src={iKR} alt="" />
                            </button>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <>
                        {gameHistory
                          ? gameHistory.map((game) => {
                              return (
                                <div className="item">
                                  <span className="number">1</span>
                                  <p className="name">{game.gameName || 'Название игры'}</p>
                                  <button
                                    className="start-btn"
                                    onClick={() => {
                                      handleVisibleDetails(item.id);
                                    }}
                                  >
                                    <FormattedMessage id="history.start" />
                                  </button>
                                </div>
                              );
                            })
                          : 'Loading...'}
                      </>
                    )}
                  </div>
                  {visibleDetails.includes(item.id) && (
                    <Link to="/share-us-tab" className="share">
                      <FormattedMessage id="history.share" /> <img src={iconVector} alt="iconVector" />{' '}
                    </Link>
                  )}
                </div>
              </Collapse>
            );
          })}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default GameHistory;

const Collapse = styled.div`
  margin-bottom: 20px;
  .board {
    .chess-board {
      max-width: 141px;
    }
  }
  .trigger {
    background: ${theme.secondaryBg};
    min-height: 42px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 12px;
    position: relative;
    cursor: pointer;
    padding: 10px 30px 10px 10px;
    .collapse-arrow {
      right: 12px;
      top: 18px;
      position: absolute;
      transition: all 0.3s;
      &.active {
        transform: rotate(180deg);
      }
    }
  }
  .collapse-body {
    width: calc(100% - 20px);
    margin: 0 auto;
    border-radius: 0 0 50px 50px;
    background-color: #d1e6e7;
    padding: 0 12px 10px;
    height: 0;
    opacity: 0;
    overflow: hidden;
    transform: scaleY(0);
    transform-origin: top;
    position: relative;
    .share {
      position: absolute;
      display: block;
      left: 50%;
      bottom: -30px;
      transform: translateX(-50%);
      font-size: 10px;
      height: 24px;
      line-height: 13px;
      display: flex;
      align-items: center;
      justify-content: center;
      text-align: center;
      text-decoration: none;
      padding: 5px 5px;
      width: 82px;
      background: ${theme.secondaryBg};
      border-radius: 20px;
      border: none;
      color: #ffffff;
      img {
        width: 14px;
        height: 12.5px;
        margin-left: 3px;
      }
    }
    &.active {
      height: 100%;
      opacity: 1;
      overflow: visible;
      animation: faze 300ms ease;
      transform: scaleY(1);
      margin-bottom: 50px;
    }

    .inner {
      border-radius: 0 0 50px 50px;
      background-color: #f7f4ea;
      padding: 20px 20px;
      .details {
        position: relative;
        .arrow-back {
          position: absolute;
          top: 0;
          left: 0;
          cursor: pointer;
          width: 12px;
        }
        .controls {
          display: flex;
          justify-content: center;
          margin: 0 auto;
          button {
            cursor: pointer;
            padding: 0 8px;
            border: none;
            background: transparent;
          }
          .up-down {
            img:first-child {
              transform: translateY(5px);
            }
            img:last-child {
              transform: translateY(-5px);
            }
          }
        }
      }
      .item {
        display: flex;
        justify-content: space-between;
        color: #000000;
        margin-bottom: 20px;
        .number {
          min-width: 25px;
        }
        .number,
        .name,
        .start-btn {
          background: #b0b1a1;
          border-radius: 20px;
          display: flex;
          justify-content: center;
          align-items: center;
          height: 22px;
          font-size: 10px;
          line-height: 13px;
        }
        .name {
          margin: 0 10px;
          flex: 1 1 80%;
          padding-right: 0;
        }
        .start-btn {
          width: 59px;
          cursor: pointer;
          border: none;
        }
      }
    }
  }
  @keyframes faze {
    0% {
      opacity: 1;
      transform: scaleY(0);
    }
    100% {
      transform: scaleY(1);
      overflow: visible;
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: 100%;
  margin: 0 auto;
  padding: 26px 12px;

  background: #fff;
  border-radius: 50px 50px 0px 0px;
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
