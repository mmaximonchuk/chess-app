import React, { useState } from 'react';

import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import iconShare from '../assets/images/clan-pay/share-us-tab.svg';
import Select from './atoms/Select';
import ChessBoard from './organism/ChessBoard';
import { gameRanksRequest, selectGameRanks } from '../features/game/gameSlice';
import { puzzle } from '../utils/puzzle';

import wChessPawn from '../assets/images/white-chess/б-пешка.svg';
import wChessTour from '../assets/images/white-chess/б-тура.svg';
import wChessElephant from '../assets/images/white-chess/б-слон.svg';
import wChessHorse from '../assets/images/white-chess/б-конь.svg';
import wChessKing from '../assets/images/white-chess/б-король.svg';
import wChessQueen from '../assets/images/white-chess/б-королева.svg';
import bChessPawn from '../assets/images/black-chess/ч-пешка.svg';
import bChessTour from '../assets/images/black-chess/ч-тура.svg';
import bChessElephant from '../assets/images/black-chess/ч-слон.svg';
import bChessHorse from '../assets/images/black-chess/ч-конь.svg';
import bChessKing from '../assets/images/black-chess/ч-король.svg';
import bChessQueen from '../assets/images/black-chess/ч-королева.svg';
import iDelete from '../assets/images/game/delete.svg';
import iTap from '../assets/images/game/tap.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';

import { theme } from '../assets/styles/variables';

function PuzzlesCreate() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);
  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });
  const searchStyles = {
    menu: (styles) => ({
      ...styles,
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      maxWidth: '279px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      fontSize: 14,
      justifyContent: 'space-between',
      color: '#fff',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '173px',
      fontSize: 14,
      justifyContent: 'space-between',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({
      padding: '5px 10px',
      width: 'calc(100% - 37px)',
      height: '42px',
    }),
  };
  return (
    <Container>
      <WrapperBgTurquoise>
        <StyledLinkBack to="/puzzles">
          <img src={imgArrowBack} alt="imgArrowBack" />
          <span><FormattedMessage id="share.puzzles.title"/></span>
        </StyledLinkBack>
        <WrapperBgPink>
          <MetaField>
            <MetaItem>
              <img src={iTap} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessPawn} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessTour} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessElephant} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessHorse} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessKing} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessQueen} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={iDelete} alt="" />
            </MetaItem>
          </MetaField>
          <ChessBoard />
          <MetaField>
            <MetaItem>
              <img src={iTap} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessPawn} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessTour} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessElephant} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessHorse} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessKing} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={bChessQueen} alt="" />
            </MetaItem>
            <MetaItem>
              <img src={iDelete} alt="" />
            </MetaItem>
          </MetaField>
          <div className="wrapper-bottom">
            <div>
              <Select
                defaultValue={puzzle[0]}
                placeholder={<FormattedMessage id="share.puzzles.based"/>}
                options={puzzle}
                styles={searchStyles}
              />
            </div>
            <Link to="/share-us-tab" className="share">
              <img src={iconShare} alt="" />
            </Link>
          </div>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}
export default PuzzlesCreate;

const MetaField = styled.div`
  background: #ffffff;
  border: 1px solid #cfcfcf;
  border-radius: 20px;
  width: 100%;
  max-width: 302px;
  height: 34px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding: 4px 9px;
  margin: 10px auto;
`;
const MetaItem = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 0 1 12.5%;
  border: 1px solid #b9b9b9;
  height: 100%;
  border-radius: 8px;
  background-color: transparent;
  img {
    width: 100%;
    height: 100%;
  }
  &:not(:first-child) {
    margin-left: 6px;
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 70px auto 0;
  height: calc(var(--app-height) - 71px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 0 18px;
  overflow: hidden;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 53px);
  margin: 0 auto;
  padding: 10px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .wrapper-bottom {
    display: flex;
    justify-content: center;
    max-width: 352px;
    width: 100%;
    margin: 15px auto;
  }
  .share {
    cursor: pointer;
    margin-left: 23px;
    max-width: 299px;
    height: 40px;
    min-width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    img {
      width: 100%;
    }
  }
`;

const Container = styled.div`
  margin-top: auto;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  max-width: 414px;
  margin: 0 auto 22px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
