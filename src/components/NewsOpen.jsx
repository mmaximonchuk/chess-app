import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { getNewsRequest, selectNews } from '../features/news/newsSlice';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
// import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import FormattedMessage from '../components/atoms/FormattedMessage';

import iconRect from '../assets/images/blogs/rect.svg';
import iconTick from '../assets/images/blogs/share.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

import { theme } from '../assets/styles/variables';
import Loader from './atoms/Loader';

function NewsOpen() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const params = useParams();
  const news = useSelector(selectNews);

  const [newData, setNewData] = useState(null);

  useEffect(() => {
    setNewData(...news.filter((item) => item.newsID == params.newsId));
  }, [news]);

  return (
    <Container>
      <WrapperBgTurquoise>
        <StyledLinkBack to="/news">
          <img src={imgArrowBack} alt="imgArrowBack" />
          <span>{newData && newData.subject}</span>
        </StyledLinkBack>
        <WrapperBgPink>
          {newData ? (
            <>
              <div className="photo">
                <img src={newData.iamgeURL || iconRect} alt="" />
              </div>
              <div className="descritpion">
                <span>{newData.newsText}</span>
              </div>
              <Link to="/share-us-tab" className="link-share">
                <img src={iconTick} alt="" />
              </Link>
            </>
          ) : (
            <Loader />
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default NewsOpen;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: auto auto 0;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  width: 100%;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 35px);
  margin: 0 auto;
  padding: 25px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .link-share {
    margin-left: auto;
    display: flex;
    width: 50px;
    height: 50px;
    margin-top: 10px;
    img {
      width: 100%;
    }
  }
  .descritpion {
    text-align: center;
    padding: 20px;
    background: #ffffff;
    border: 1px solid #dcdcdc;
    border-radius: 33px;
    max-height: 298px;
    height: 100%;
    width: 100%;
    span {
      font-size: 15px;
      line-height: 20px;

      color: #979797;
    }
  }
  .photo {
    background: #e7e7e7;
    border-radius: 20px;
    // max-height: 189px;
    width: 100%;
    overflow: hidden;
    margin-bottom: 15px;
    img {
      width: 100%;
      height: 100%;
      max-height: 189px;
      object-fit: cover;
      object-position: center center;
    }
  }
`;

const Container = styled.div`
  overflow: hidden;
  height: calc(var(--app-height));
  display: flex;
  align-items: flex-end;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: -5px auto 15px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
