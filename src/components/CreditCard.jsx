import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import { langs } from '../utils/languages';
import { setLang } from '../features/langs/langsSlice';

import Select from './atoms/Select';
import Input from './atoms/Input';

import iconStar from '../assets/images/user-profile/input-user.svg';
import iconCard from '../assets/images/clan-pay/credit-c.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';

import { theme } from '../assets/styles/variables';

function Paypal() {
  const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);
  const dot = (img = langs[defaultLang].flag) => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
      background: `url(${img})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'contain',
      borderRadius: 20,
      content: '" "',
      display: 'block',
      height: 37,
      width: 50,
    },
  });

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 10,
    svg: {
      display: 'none',
    },
  });
  const colourStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '100%',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, 0)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: '#B0B1A1',
      borderRadius: '12px',
      border: 'none',
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '5px 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      ...dot(),
      textAlign: 'center',
      width: '150px',
      justifyContent: 'space-between',
      color: '#fff',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;
      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        ':active': {
          ...styles[':active'],
          backgroundImage: isDisabled ? null : isSelected ? `url(${img})` : null,
        },
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        display: 'flex',
        color: theme.languagesColor,
        ':before': {
          background: `url(${img})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'contain',
          borderRadius: 20,
          content: '" "',
          display: 'block',
          marginRight: 35,
          height: 37,
          width: 50,
        },
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      ...dot(data.flagImg),
      width: '150px',
      justifyContent: 'space-between',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };
  return (
    <Container>
      <StyledLinkBack to="/payment">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="payment.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <h2 className="title">Credit cards</h2>
        <WrapperBgPink>
          <Form>
            <Input $bgColor="#fff" type="edit" placeholder={vocabularyObject["payment.cardname"][defaultLang]} />
            <Input $bgColor="#fff" type="card" placeholder="0000 0000 0000 0000" />
            <div className="inner-wrap">
              <Input $maxWidth="108px" $bgColor="#fff" placeholder="ММ/ГГ" />
              <Input $maxWidth="108px" $bgColor="#fff" placeholder="CVC/CVV" />
            </div>
            <div>
              <Select
                onChange={(selectedOption) => dispatch(setLang(selectedOption.value))}
                defaultValue={defaultLang}
                value={langs[defaultLang].value}
                placeholder={langs[defaultLang].value}
                options={langs}
                styles={colourStyles}
              />
            </div>
            <button className="enter-btn"><FormattedMessage id="entrance.btn.enter"/></button>
          </Form>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Paypal;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  .inner-wrap {
    display: flex;
    justify-content: space-between;
    max-width: 232px;
    width: 100%;
  }
  input {
    padding: 15px 20px 15px;
  }
  .enter-btn {
    background: #b0b1a1;
    border-radius: 20px;
    height: 42px;
    padding: 5px 20px;
    font-size: 20px;
    border: none;
    cursor: pointer;
    color: #ffffff;
    margin-top: 24px;
    font-weight: 700;
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  .title {
    font-size: 30px;
    line-height: 40px;
    color: #444444;
    font-weight: 400;
    text-align: center;
    margin: 0 0 15px;
  }
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 50px);
  margin: 0 auto;
  padding: 35px 22px;
  background: #fff;
  border-radius: 50px 50px 0px 0px;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
