import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import iPlay from '../assets/images/game/play.svg';
import iUser from '../assets/images/game/user.svg';
import iRating from '../assets/images/game/rating.svg';
import iMessages from '../assets/images/game/messages.svg';
import iPlayAgain from '../assets/images/game/play-again.svg';
import iShare from '../assets/images/game/share.svg';
import iCart from '../assets/images/game/cart.svg';
import iPlayers from '../assets/images/game/users.svg';
import iTrophie from '../assets/images/game/trophies.svg';
import iPuzzle from '../assets/images/game/puzzle.svg';
import iWriteMsg from '../assets/images/game/write-message.svg';
import iNews from '../assets/images/game/news.svg';
import iTopRating from '../assets/images/game/top-rating.svg';
import iSettings from '../assets/images/game/settings.svg';
import iSupport from '../assets/images/game/support.svg';


import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
}


function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: 'transparent',
    },
  }));
  
export default function Layout() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
  
    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons="on"
            indicatorColor="primary"
            textColor="primary"
            aria-label="scrollable force tabs example"
          >
            <Tab aria-label="all-games" icon={<img src={iPlay} alt="" />} {...a11yProps(0)} />
            <Tab aria-label="entrance" icon={<img src={iUser} alt="" />} {...a11yProps(1)} />
            <Tab aria-label="rating-tab" icon={<img src={iRating} alt="" />} {...a11yProps(2)} />
            <Tab aria-label="messages" icon={<img src={iMessages} alt="" />} {...a11yProps(3)} />
            <Tab aria-label="game-history" icon={<img src={iPlayAgain} alt="" />} {...a11yProps(4)} />
            <Tab aria-label="share-us-tab" icon={<img src={iShare} alt="" />} {...a11yProps(5)} />
            <Tab aria-label="shop" icon={<img src={iCart} alt="" />} {...a11yProps(6)} />
            <Tab aria-label="clans" icon={<img src={iPlayers} alt="" />} {...a11yProps(7)} />
            <Tab aria-label="tournament" icon={<img src={iTrophie} alt="" />} {...a11yProps(8)} />
            <Tab aria-label="puzzles" icon={<img src={iPuzzle} alt="" />} {...a11yProps(9)} />
            <Tab aria-label="blogs" icon={<img src={iWriteMsg} alt="" />} {...a11yProps(10)} />
            <Tab aria-label="news" icon={<img src={iNews} alt="" />} {...a11yProps(11)} />
            <Tab aria-label="league-overview" icon={<img src={iTopRating} alt="" />} {...a11yProps(12)} />
            <Tab aria-label="settings" icon={<img src={iSettings} alt="" />} {...a11yProps(13)} />
            <Tab aria-label="support" icon={<img src={iSupport} alt="" />} {...a11yProps(14)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          Item One
        </TabPanel>
        <TabPanel value={value} index={1}>
          Item Two
        </TabPanel>
        <TabPanel value={value} index={2}>
          Item Three
        </TabPanel>
        <TabPanel value={value} index={3}>
          Item Four
        </TabPanel>
        <TabPanel value={value} index={4}>
          Item Five
        </TabPanel>
        <TabPanel value={value} index={5}>
          Item Six
        </TabPanel>
        <TabPanel value={value} index={6}>
          Item Seven
        </TabPanel>
      </div>
    );
  }

