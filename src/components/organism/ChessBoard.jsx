import React, { useState } from 'react';
import styled from 'styled-components';
// import wChessPawn from '../../assets/images/white-chess/б-пешка.svg';
// import wChessTour from '../../assets/images/white-chess/б-тура.svg';
// import wChessElephant from '../../assets/images/white-chess/б-слон.svg';
// import wChessHorse from '../../assets/images/white-chess/б-конь.svg';
// import wChessKing from '../../assets/images/white-chess/б-король.svg';
// import wChessQueen from '../../assets/images/white-chess/б-королева.svg';

// import bChessPawn from '../../assets/images/black-chess/ч-пешка.svg';
// import bChessTour from '../../assets/images/black-chess/ч-тура.svg';
// import bChessElephant from '../../assets/images/black-chess/ч-слон.svg';
// import bChessHorse from '../../assets/images/black-chess/ч-конь.svg';
// import bChessKing from '../../assets/images/black-chess/ч-король.svg';
// import bChessQueen from '../../assets/images/black-chess/ч-королева.svg';
import 'chessboard-element';

function ChessBoard({width, ...props}) {
  const [flipped, setFlipped] = useState(true);
  
  return (
    <BoardContainer width={width} className="chess">
      <chess-board
        orientation={flipped ? 'black' : 'white'}
        draggable-pieces
        class="chess-board"
        {...props}
      ></chess-board>
    </BoardContainer>
  );
}

export default ChessBoard;

const BoardContainer = styled.div`
  .chess-board {
    max-width: 301px;
    width: ${({ width }) => width || '100%'};
    margin: 0 auto;
    div[part~="board"] {
      border: none;
    }
  }
`;

const MetaField = styled.div`
  background: #ffffff;
  border: 1px solid #cfcfcf;
  border-radius: 20px;
  width: 80%;
  max-width: 370px;
  height: 34px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding: 4px 9px;
  margin: 1vh auto;
`;
const MetaItem = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 0 1 12.5%;
  border: 1px solid #b9b9b9;
  height: 100%;
  border-radius: 8px;
  background-color: transparent;
  img {
    width: 100%;
    height: 100%;
  }
  &:not(:first-child) {
    margin-left: 6px;
  }
`;