import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import iPlay from '../../assets/images/game/play.svg';
import iUser from '../../assets/images/game/user.svg';
import iRating from '../../assets/images/game/rating.svg';
import iMessages from '../../assets/images/game/messages.svg';
import iPlayAgain from '../../assets/images/game/play-again.svg';
import iShare from '../../assets/images/game/share.svg';
import iCart from '../../assets/images/game/cart.svg';
import iPlayers from '../../assets/images/game/users.svg';
import iTrophie from '../../assets/images/game/trophies.svg';
import iPuzzle from '../../assets/images/game/puzzle.svg';
import iWriteMsg from '../../assets/images/game/write-message.svg';
import iNews from '../../assets/images/game/news.svg';
import iTopRating from '../../assets/images/game/top-rating.svg';
import iSettings from '../../assets/images/game/settings.svg';
import iSupport from '../../assets/images/game/support.svg';

function HeaderControls() {
  return (
    <StyledHeaderControls className="header-control">
      <Link to="/all-games">
        <img src={iPlay} alt="" />
      </Link>
      <Link to="/entrance">
        <img src={iUser} alt="" />
      </Link>
      <Link to="/rating-tab">
        <img src={iRating} alt="" />
      </Link>
      <Link to="/messages">
        <img src={iMessages} alt="" />
      </Link>
      <Link to="/game-history">
        <img src={iPlayAgain} alt="" />
      </Link>
      <Link to="/share-us-tab">
        <img src={iShare} alt="" />
      </Link>
      <Link to="/shop">
        <img src={iCart} alt="" />
      </Link>
      <Link to="/clans">
        <img src={iPlayers} alt="" />
      </Link>
      <Link to="/tournament">
        <img src={iTrophie} alt="" />
      </Link>
      <Link to="/puzzles">
        <img src={iPuzzle} alt="" />
      </Link>
      <Link to="/blogs">
        <img src={iWriteMsg} alt="" />
      </Link>
      <Link to="/news">
        <img src={iNews} alt="" />
      </Link>
      <Link to="/league-overview">
        <img src={iTopRating} alt="" />
      </Link>
      <Link to="/settings">
        <img src={iSettings} alt="" />
      </Link>
      <Link to="/support">
        <img src={iSupport} alt="" />
      </Link>
    </StyledHeaderControls>
  );
}

export default HeaderControls;

const StyledHeaderControls = styled.div`
  height: 60px;
  overflow: overlay;
  display: flex;
  margin: 0 auto 1vh;
  max-width: 490px;
  width: 100%;
  a {
    display: flex;
    margin-right: 16px;
  }
`;
