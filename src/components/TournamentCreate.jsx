import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import { tournamentCreate, visible } from '../utils/tournamentCreate';
import Select from './atoms/Select';

// import iconSearch from '../assets/images/svg/search.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';

import { theme } from '../assets/styles/variables';

function TournamentCreate() {
  const defaultLang = useSelector(selectLangLocale);

  const dispatch = useDispatch();
  const [selector1, setSelector1] = useState(4);
  const [selector2, setSelector2] = useState(8);

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });
  const searchStyles = {
    menu: (styles) => ({
      ...styles,
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      maxWidth: '300px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      fontSize: 14,
      justifyContent: 'space-between',
      color: '#fff',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '173px',
      fontSize: 14,
      justifyContent: 'space-between',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({
      padding: '5px 10px',
      width: 'calc(100% - 37px)',
      height: '42px',
    }),
  };
  return (
    <Container>
      <StyledLinkBack to="/tournament">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="tournament.title"/></span>
        {/* <Link to="/tournament-create" className="add-clan">
          <img src={iconAdd} alt="" />
        </Link> */}
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchContainer>
          <input type="text" placeholder={vocabularyObject['tournament.input.tournament.name'][defaultLang]} />
          {/* <button>
            <img src={iconSearch} alt="" />
          </button> */}
        </SearchContainer>
        <BtnsWrapper></BtnsWrapper>
        <WrapperBgPink>
          <p className="min-limit-p"><FormattedMessage id="tournament.par.min.player.level"/></p>
          <p className="selector-pointer">{selector1}</p>
          <div className="selector">
            <button
              onClick={() => setSelector1(1)}
              className={`place ${selector1 === 1 ? 'active' : ''}`}
            >
              1
            </button>
            <button
              onClick={() => setSelector1(2)}
              className={`place ${selector1 === 2 ? 'active' : ''}`}
            >
              2
            </button>
            <button
              onClick={() => setSelector1(3)}
              className={`place ${selector1 === 3 ? 'active' : ''}`}
            >
              3
            </button>
            <button
              onClick={() => setSelector1(4)}
              className={`place ${selector1 === 4 ? 'active' : ''}`}
            >
              4
            </button>
            <button
              onClick={() => setSelector1(5)}
              className={`place ${selector1 === 5 ? 'active' : ''}`}
            >
              5
            </button>
            <button
              onClick={() => setSelector1(6)}
              className={`place ${selector1 === 6 ? 'active' : ''}`}
            >
              6
            </button>
            <button
              onClick={() => setSelector1(7)}
              className={`place ${selector1 === 7 ? 'active' : ''}`}
            >
              7
            </button>
            <button
              onClick={() => setSelector1(8)}
              className={`place ${selector1 === 8 ? 'active' : ''}`}
            >
              8
            </button>
            <button
              onClick={() => setSelector1(9)}
              className={`place ${selector1 === 9 ? 'active' : ''}`}
            >
              9
            </button>
            <button
              onClick={() => setSelector1(10)}
              className={`place ${selector1 === 10 ? 'active' : ''}`}
            >
              10
            </button>
          </div>
          <p className="min-limit-p"><FormattedMessage id="tournament.par.max.player.level"/></p>
          <p className="selector-pointer">{selector2}</p>
          <div className="selector">
            <button
              onClick={() => setSelector2(1)}
              className={`place ${selector2 === 1 ? 'active' : ''}`}
            >
              1
            </button>
            <button
              onClick={() => setSelector2(2)}
              className={`place ${selector2 === 2 ? 'active' : ''}`}
            >
              2
            </button>
            <button
              onClick={() => setSelector2(3)}
              className={`place ${selector2 === 3 ? 'active' : ''}`}
            >
              3
            </button>
            <button
              onClick={() => setSelector2(4)}
              className={`place ${selector2 === 4 ? 'active' : ''}`}
            >
              4
            </button>
            <button
              onClick={() => setSelector2(5)}
              className={`place ${selector2 === 5 ? 'active' : ''}`}
            >
              5
            </button>
            <button
              onClick={() => setSelector2(6)}
              className={`place ${selector2 === 6 ? 'active' : ''}`}
            >
              6
            </button>
            <button
              onClick={() => setSelector2(7)}
              className={`place ${selector2 === 7 ? 'active' : ''}`}
            >
              7
            </button>
            <button
              onClick={() => setSelector2(8)}
              className={`place ${selector2 === 8 ? 'active' : ''}`}
            >
              8
            </button>
            <button
              onClick={() => setSelector2(9)}
              className={`place ${selector2 === 9 ? 'active' : ''}`}
            >
              9
            </button>
            <button
              onClick={() => setSelector2(10)}
              className={`place ${selector2 === 10 ? 'active' : ''}`}
            >
              10
            </button>
          </div>

          <DataPickerWrap>
            <div className="start">
              <span><FormattedMessage id="tournament.start"/></span>
              <input type="date" />
            </div>
            <div className="end">
              <span><FormattedMessage id="tournament.end"/></span>
              <input type="date" />
            </div>
          </DataPickerWrap>
          <div>
            <Select
              defaultValue={visible[0]}
              placeholder={<FormattedMessage id="tournament.privacy"/>}
              options={visible}
              styles={searchStyles}
            />
          </div>
          <div>
            <Select
              defaultValue={tournamentCreate[0]}
              placeholder={<FormattedMessage id="tournament.basis"/>}
              options={tournamentCreate}
              styles={searchStyles}
            />
          </div>
          <div className="wrapper">
            <button className="create"><FormattedMessage id="tournament.create" /></button>
            <button className="delete"><FormattedMessage id="tournament.delete" /></button>
          </div>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}
export default TournamentCreate;

const DataPickerWrap = styled.div`
  display: flex;
  width: 80%;
  justify-content: space-between;
  margin-bottom: 46px;
  .start,
  .end {
    text-align: center;
    span {
      display: block;
      margin-bottom: 20px;
      font-size: 15px;
      line-height: 20px;
      color: #353535;
    }
    input {
      // width: 88px;
    }
  }
  @media (max-width: 462px) {
    flex-direction: column;
    align-items: center;
    .start,
    .end {
      text-align: center;
      span {
        display: block;
        margin-bottom: 20px;
        margin-top: 10px;
      }
    }
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 88px);
  margin: 0 auto;
  padding: 19px 22px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  display: flex;
  flex-direction: column;
  align-items: center;
  .wrapper {
    display: flex;
    justify-content: space-between;
    min-width: 290px;

    margin-top: 48px;
    .create,
    .delete {
      font-size: 15px;
      line-height: 20px;
      display: flex;
      align-items: center;
      text-align: center;
      color: #5c5c5c;
      height: 38px;
      padding: 5px 30px;
      background: transparent;
      font-weight: 700;
      cursor: pointer;
    }
    .delete {
      padding: 5px 25px;
    }
    .create {
      color: #fff;
      background: #b0b1a1;
      border-radius: 20px;
    }
  }
  .min-limit-p {
    margin-bottom: 10px;
    text-align: center;
  }
  .selector-pointer {
    font-weight: 500;
    font-size: 15px;
    line-height: 18px;
    background: #b0b1a1;
    border-radius: 8px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: Roboto;
    width: 32px;
    height: 24px;
    color: #ffffff;
    margin-bottom: 5px;
  }
  .selector {
    display: flex;
    background: #ffffff;
    border-radius: 50px;
    width: 160px;
    padding: 0px 3px;
    margin-bottom: 20px;
    .place {
      font-family: Roboto;
      background: transparent;
      line-height: 16px;
      font-size: 12px;
      display: flex;
      align-items: center;
      text-align: center;
      color: #d8d8d8;
      justify-content: center;
      width: 22px;
      height: 24px;
      transition: all 0.3s;
      cursor: pointer;
      &.active {
        color: #ffffff;
        background: #b0b1a1;
        font-size: 14px;
        border-radius: 8px;
      }
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 60%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 14px auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 15px;
    line-height: 13px;
    height: 37px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  margin-bottom: 26px;
  .selector {
    display: flex;
    background: #ffffff;
    border-radius: 50px;
    width: 160px;
    padding: 0px 3px;
    .place {
      font-family: Roboto;
      background: transparent;
      line-height: 16px;
      font-size: 12px;
      display: flex;
      align-items: center;
      text-align: center;
      color: #d8d8d8;
      justify-content: center;
      width: 22px;
      height: 24px;
      transition: all 0.3s;
      cursor: pointer;
      &.active {
        color: #ffffff;
        background: #b0b1a1;
        font-size: 14px;
        border-radius: 8px;
      }
    }
  }
  .custom-checkbox {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    z-index: -1;
  }
  .custom-checkbox + .label {
    cursor: pointer;
    width: 160px;
    height: 24px;
    font-size: 10px;
    line-height: 13px;
    background: #ffffff;
    border-radius: 50px;
    border: none;
    padding: 0;
    position: relative;
    margin-bottom: 3px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    .tick {
      transition: all 0.3s;

      position: absolute;
      left: 0;
      border-radius: 50%;
      position: absolute;
      // background: #b0b1a1;
      width: 24px;
      border: 3px solid #b0b1a1;
      height: 24px;
      top: 0;
      color: #656565;
      img {
        opacity: 0;
        transform: translate(2px, -1px);
      }
    }
  }
  .custom-checkbox:checked + .label .tick img {
    opacity: 1;
  }
  .custom-checkbox:checked + .label {
    .tick {
      background: #b0b1a1;
      img {
        opacity: 1;
      }
    }
  }

  @media (max-width: 342px) {
    flex-direction: column;
    // height: 120px;
    a {
      width: 100%;
    }
  }
`;
const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
