import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { selectLangLocale } from '../features/langs/langsSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import { signInRequest, selectUserData } from '../features/user/userSlice';
import { getGUID } from '../utils/getGUID';
import HeaderControls from './organism/HeaderControls';
import imgLogo from '../assets/images/Лого.svg';
import Button from './atoms/Button';
import Input from './atoms/Input';
import FormattedMessage from './atoms/FormattedMessage';

import { theme } from '../assets/styles/variables';
import { StartContainer } from './styled';

function Entrance() {
  const dispatch = useDispatch();
  const history = useHistory();
  const defaultLang = useSelector(selectLangLocale);
  const userData = useSelector(selectUserData);
  const [formData, setFormData] = useState({Email: '', Password: '', Token: ''});
  // console.log(userData);
  const handleInputs = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(signInRequest(formData));
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');
    if (!token) {
      token = "tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7"

      localStorage.setItem('token', token);
    }
    setFormData({
      ...formData,
      Token: token,
    });
  }, []);

  useEffect(() => {
      if(userData.userID !== '') {
        history.push('/profile')
      }
  }, [userData]);

  return (
    <StartContainer justifyContent="flex-start">
      {/* <HeaderControls /> */}
      <StyledImg src={imgLogo} alt="" />

      <Title>Democrat chess</Title>
      <Tip>
        <FormattedMessage id="signUp.tip.signIn" />
      </Tip>
      <StyledButton>Facebook</StyledButton>
      <StyledButton isGoogle>Google</StyledButton>
      <form onSubmit={handleSubmit}>
        <Input
          name="Email"
          type="email"
          placeholder={vocabularyObject['signUp.input.email'][defaultLang]}
          onChange={handleInputs}
          value={formData.Email}
        />
        <Input
          name="Password"
          type="password"
          placeholder={vocabularyObject['signUp.input.password'][defaultLang]}
          onChange={handleInputs}
          value={formData.Password}
        />

        <ActLink to="/password-forgot">
          <FormattedMessage id="entrance.btn.forgotPassQuestion" />
        </ActLink>

        <Button
          fontWeight={700}
          fontSize={'20px'}
          height="42px"
          margin="2vh auto 2vh"
          defType="primary"
          text={<FormattedMessage id="entrance.btn.enter" />}
          type="submit"
        />
        <ActLink to="/sign-up">
          <FormattedMessage id="signUp.btn.signUp" />
        </ActLink>
        <Wrapper>
          <Button
            fontWeight={700}
            fontSize={'20px'}
            defType="no-bg"
            text={<FormattedMessage id="entrance.btn.play.immediately" />}
            to="/all-games"
            $isLg
          />
        </Wrapper>
      </form>
    </StartContainer>
  );
}

export default Entrance;

const StyledImg = styled.img`
  position: relative;
  width: 61px;
  left: 10px;
  max-width: 120px;
`;

const Title = styled.h1`
  font-size: 40px;
  line-height: 53px;
  margin-top: 0;
  margin-bottom: 2vh;
  color: #000000;
`;
const Tip = styled.p`
  color: ${theme.greyTextColor};
  margin-bottom: 2vh;
`;
const Wrapper = styled.div`
  margin-top: auto;
`;
const StyledButton = styled.button`
  display: flex;
  margin-bottom: 2vh;
  height: 39px;
  cursor: pointer;
  width: 232px;
  align-items: center;
  justify-content: center;
  background: ${({ isGoogle }) => (isGoogle ? '#12A44C' : '#2E137A')};
  border-radius: 20px;
  color: #fff;
  font-size: 25px;
  border: none;
  padding: 5px 0;
`;
const ActLink = styled(Link)`
  color: #545454;
  // margin-bottom: 2vh;
  font-size: 12px;
  text-decoration: none;
`;
