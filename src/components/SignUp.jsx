import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import { selectLangLocale } from '../features/langs/langsSlice';
import { getGUID } from '../utils/getGUID';
import { signUpRequest, selectSignUpStatus } from '../features/user/userSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import Button from './atoms/Button';
import Input from './atoms/Input';
import FormattedMessage from './atoms/FormattedMessage';
import HeaderControls from './organism/HeaderControls';

import imgLogo from '../assets/images/Лого.svg';
import imgArrowBack from '../assets/images/svg/arrow-back.svg';

import { theme } from '../assets/styles/variables';
import { StartContainer } from './styled';

function SignUp() {
  const history = useHistory();
  const dispatch = useDispatch();
  const defaultLang = useSelector(selectLangLocale);
  const signUpStatus = useSelector(selectSignUpStatus);
  const [formData, setFormData] = useState({ Email: '', Password: '', Token: '', Provider: 'DC' });
  const [passwordRepeat, setPasswordRepeat] = useState('');

  const handleInputs = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
    // console.log(formData);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (passwordRepeat === formData.Password) {
      dispatch(signUpRequest(formData));
    }
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');
    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    setFormData({
      ...formData,
      Token: token,
    });
  }, []);
  useEffect(() => {
    if (signUpStatus === 'success') {
      history.push('/entrance');
    }
  }, [signUpStatus]);
  return (
    <StartContainer justifyContent="flex-start">
      {/* <HeaderControls /> */}

      <StyledLinkBack to="/entrance">
        <img src={imgArrowBack} alt="imgArrowBack" />
      </StyledLinkBack>
      <StyledImg src={imgLogo} alt="" />

      <Title>Democrat chess</Title>
      <Tip>
        <FormattedMessage id="signUp.tip.signIn" />
      </Tip>
      <StyledButton>Facebook</StyledButton>
      <StyledButton isGoogle>Google</StyledButton>
      <form onSubmit={handleSubmit}>
        <Input
          name="Email"
          bgColor="#fff"
          type="email"
          placeholder={vocabularyObject['signUp.input.email'][defaultLang]}
          onChange={handleInputs}
          value={formData.Email}
        />
        <Input
          name="Password"
          bgColor="#fff"
          type="password"
          placeholder={vocabularyObject['signUp.input.password'][defaultLang]}
          onChange={handleInputs}
          value={formData.Password}
        />
        <Input
          name="Password"
          bgColor="#fff"
          type="password"
          placeholder={vocabularyObject['signUp.input.password.again'][defaultLang]}
          onChange={(e) => setPasswordRepeat(e.target.value)}
          value={passwordRepeat}
        />
        <Button
          fontWeight={700}
          fontSize="14px"
          height="42px"
          defType="primary"
          margin="0 auto"
          text={<FormattedMessage id="signUp.btn.signUp" />}
          //to="#"
          type="submit"
        />
      </form>
    </StartContainer>
  );
}

export default SignUp;

const StyledImg = styled.img`
  position: relative;
  width: 61px;
  left: 10px;
  max-width: 120px;
`;

const Title = styled.h1`
  font-size: 40px;
  line-height: 53px;
  margin-top: 0;
  margin-bottom: 2vh;
  color: #000000;
`;
const Tip = styled.p`
  color: ${theme.greyTextColor};
  margin-bottom: 2vh;
`;
// const Wrapper = styled.div`
//   margin-top: auto;
// `;
const StyledButton = styled.button`
  display: flex;
  margin-bottom: 2vh;
  height: 39px;
  width: 232px;
  align-items: center;
  justify-content: center;
  background: ${({ isGoogle }) => (isGoogle ? '#12A44C' : '#2E137A')};
  border-radius: 20px;
  color: #fff;
  font-size: 25px;
  border: none;
`;

const StyledLinkBack = styled(Link)`
  position: absolute;
  top: 80px;
  left: 8vw;
  img {
  }
`;
