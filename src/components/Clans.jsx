import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import iconUserSmall from '../assets/images/user-profile/small-user-icon.svg';
import iconShare from '../assets/images/game/VectorShare.svg';
import iconInfo from '../assets/images/info.svg';
import Loader from './atoms/Loader';
import { getClansRequest, selectClans } from '../features/clans/clansSlice';

// import iconSearch from '../assets/images/svg/search.svg';
import iconSearch from '../assets/images/svg/search.svg';
import iconTick from '../assets/images/clan-pay/Tick.svg';
import iconAdd from '../assets/images/clan-pay/AddClan.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

import { theme } from '../assets/styles/variables';

function Clans() {
  const defaultLang = useSelector(selectLangLocale);
  const getClans = useSelector(selectClans);

  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getClansRequest({ Token: token }));
  }, []);

  return (
    <Container>
      <StyledLinkBack to="/all-games">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="clans.title"/></span>
        <Link to="/clan-create" className="add-clan">
          <img src={iconAdd} alt="" />
        </Link>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchContainer>
          <input type="text" placeholder={'Введите название'} />
          <button>
            <img src={iconSearch} alt="" />
          </button>
        </SearchContainer>
        <BtnsWrapper>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-1" />
          <label className="label" for="custom-checkbox-1">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="tournament.by.name"/></span>
          </label>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-2" />
          <label className="label" for="custom-checkbox-2">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="tournament.by.member.name"/></span>
          </label>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-3" />
          <label className="label" for="custom-checkbox-3">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="clans.by.rating"/></span>
          </label>
        </BtnsWrapper>
        <WrapperBgPink>
          {getClans ? (
            getClans.map((clan) => {
              return <SearchedUser data={clan} />;
            })
          ) : (
            <Loader />
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Clans;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 145px);
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
  .lds-ring {
    transform: translate(-50%,50%)
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 60%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  margin-bottom: 26px;

  .custom-checkbox {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    z-index: -1;
  }
  .custom-checkbox + .label {
    cursor: pointer;
    width: 160px;
    height: 24px;
    font-size: 10px;
    line-height: 13px;
    background: #ffffff;
    border-radius: 50px;
    border: none;
    padding: 0;
    position: relative;
    margin-bottom: 3px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    .tick {
      transition: all 0.3s;

      position: absolute;
      left: 0;
      border-radius: 50%;
      position: absolute;
      // background: #b0b1a1;
      width: 24px;
      border: 3px solid #b0b1a1;
      height: 24px;
      top: 0;
      color: #656565;
      img {
        opacity: 0;
        transform: translate(2px, -1px);
      }
    }
  }
  .custom-checkbox:checked + .label .tick img {
    opacity: 1;
  }
  .custom-checkbox:checked + .label {
    .tick {
      background: #b0b1a1;
      img {
        opacity: 1;
      }
    }
  }

  @media (max-width: 342px) {
    flex-direction: column;
    height: 120px;
    a {
      width: 100%;
    }
  }
`;

function SearchedUser({ to, data = {} }) {
  return (
    <ChatItem className="chat-item">
      <Avatar className="ava">
        <img src={iconUserSmall} alt="" />
      </Avatar>
      <Body className="chat-body">
        <p className="name">{`${data?.clanName || 'Имя'}`}</p>
        <Link to="/share-us-tab" className="share-btn">
          <img src={iconShare} alt="" />
        </Link>
        <Link to={`/clan-info/${data.clanID}`} className="info-btn">
          <img src={iconInfo} alt="" />
        </Link>
      </Body>
    </ChatItem>
  );
}

const ChatItem = styled.div`
  display: flex;
  align-items: center;
  text-decoration: none;
  margin-bottom: 27px;
  .share-btn,
  .info-btn {
    cursor: pointer;
    padding: 0;
    border: none;
    background: transparent;
    position: absolute;
    right: 50px;
    top: 5px;
    width: 33px;
    height: 33px;
    img {
      width: 100%;
    }
  }
  .share-btn {
    padding: 5px;
    background: ${theme.secondaryBg};
    border-radius: 50%;
  }
  .info-btn {
    right: 12px;
    top: 5px;
  }
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
  overflow: hidden;
  // img {
  //   width: 100%;
  //   height: 100%;
  //   object-fit: cover;
  // }
`;

const Body = styled.div`
  flex: 1;
  position: relative;
  .name {
    margin-left: -9px;
    padding: 8px 9px;
    background: #ffffff;
    border-radius: 20px;
    width: 100%;
    height: 100%;
    color: ${({ shortenDescription }) => (!shortenDescription ? '#505050' : '#a9a9a9')};
    font-size: 15px;
  }
`;
