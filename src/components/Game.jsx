import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ChessBoard from '../components/organism/ChessBoard';
import Modal from '../components/molecules/ModalContainer';
import FormattedMessage from './atoms/FormattedMessage';

import wChessPawn from '../assets/images/white-chess/б-пешка.svg';
import wChessTour from '../assets/images/white-chess/б-тура.svg';
import wChessElephant from '../assets/images/white-chess/б-слон.svg';
import wChessHorse from '../assets/images/white-chess/б-конь.svg';
import wChessKing from '../assets/images/white-chess/б-король.svg';
import wChessQueen from '../assets/images/white-chess/б-королева.svg';
import bChessPawn from '../assets/images/black-chess/ч-пешка.svg';
import bChessTour from '../assets/images/black-chess/ч-тура.svg';
import bChessElephant from '../assets/images/black-chess/ч-слон.svg';
import bChessHorse from '../assets/images/black-chess/ч-конь.svg';
import bChessKing from '../assets/images/black-chess/ч-король.svg';
import bChessQueen from '../assets/images/black-chess/ч-королева.svg';

import iconUser from '../assets/images/user-profile/small-user-icon.svg';
import iconWhiteSide from '../assets/images/game/white-side.svg';
import iconBlackSIde from '../assets/images/game/black-side.svg';
import iShareRect from '../assets/images/game/share-rect.svg';
import iSendRect from '../assets/images/game/send-rect.svg';
import iLockRect from '../assets/images/game/lock-rect.svg';
import iUnlockRect from '../assets/images/game/unlock-rect.svg';
import iSwapRect from '../assets/images/game/steps-rect.svg';
import iDrawRect from '../assets/images/game/draw-rect.svg';
import iBanRect from '../assets/images/game/report-rect.svg';
import iManuRect from '../assets/images/game/menu-rect.svg';
import iDrawOffer from '../assets/images/game/draw-offer.svg';
import iCapitOffer from '../assets/images/game/capit-offer.svg';
import iTick from '../assets/images/game/tick.svg';
import iQuitOffer from '../assets/images/game/quit-offer.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg';
import iSmile from '../assets/images/game/smile.svg';
import iSendMsg from '../assets/images/game/send-1.svg';
import iDelete from '../assets/images/game/delete.svg';
import iTap from '../assets/images/game/tap.svg';

import { theme } from '../assets/styles/variables';

function Game() {
  const [isOpenedPopup, setPopupOpen] = useState(false);
  const [isLockedOponent, setLockedOponent] = useState(false);
  const [isDrawOffer, setDrawOffer] = useState(false);
  const [isCapitOffer, setCapitOffer] = useState(false);
  const [isQuitOffer, setQuitOffer] = useState(false);
  const [isSendMessageModal, setSendMessageModal] = useState(false);
  const [isShowEmoji, setShowEmoji] = useState(false);

  // useEffect(() => {}, [isLockedOponent]);
  return (
    <Container>
      {/* <HeaderControls /> */}
      {isLockedOponent && (
        <Modal closeModal={setLockedOponent}>
          <ModalInner>
            <img
              onClick={() => setLockedOponent((prev) => !prev)}
              className="c-btns"
              src={iCloseBtn}
              alt=""
            />
            <span><FormattedMessage id="game.m.ban"/></span>
          </ModalInner>
        </Modal>
      )}
      {isDrawOffer && (
        <Modal closeModal={setDrawOffer}>
          <ModalInnerOffer>
            <img
              onClick={() => setDrawOffer((prev) => !prev)}
              className="c-btns"
              src={iCloseBtn}
              alt=""
            />
            <img src={iDrawOffer} alt="iDrawOffer" />
            <p>
            <FormattedMessage id="game.m.draw" />
            </p>
            <div className="btns">
              <button><FormattedMessage id="game.m.agree" /></button>
              <button><FormattedMessage id="game.m.disagree" /></button>
            </div>
            <input type="checkbox" className="custom-checkbox" id="check" />
            <label for="check" className="label">
              <img src={iTick} alt="iTick" />
              <FormattedMessage id="game.m.draw.input" />
            </label>
          </ModalInnerOffer>
        </Modal>
      )}
      {isCapitOffer && (
        <Modal closeModal={setCapitOffer}>
          <ModalInnerOffer>
            <img
              onClick={() => setCapitOffer((prev) => !prev)}
              className="c-btns"
              src={iCloseBtn}
              alt=""
            />
            <img src={iCapitOffer} alt="iCapitOffer" />
            <p>Действительно ли вы хотите сдаться?</p>
            <div className="btns">
            <button>
                <FormattedMessage id="profile.modal.btn.yes" />
              </button>
              <button>
                <FormattedMessage id="profile.modal.btn.no" />
              </button>
            </div>
          </ModalInnerOffer>
        </Modal>
      )}
      {isQuitOffer && (
        <Modal closeModal={setQuitOffer}>
          <ModalInnerOffer>
            <img
              onClick={() => setQuitOffer((prev) => !prev)}
              className="c-btns"
              src={iCloseBtn}
              alt=""
            />
            <img src={iQuitOffer} alt="iCapitOffer" />
            <p>
              <FormattedMessage id="game.m.quit" />
            </p>
            <div className="btns">
              <button>
                <FormattedMessage id="profile.modal.btn.yes" />
              </button>
              <button>
                <FormattedMessage id="profile.modal.btn.no" />
              </button>
            </div>
          </ModalInnerOffer>
        </Modal>
      )}
      {isSendMessageModal && (
        <Modal closeModal={setSendMessageModal}>
          <ModalInnerSendMessage>
            <div className="wrapper">
              <textarea name="" id="" cols="30" rows="10"></textarea>
              <button onClick={() => setSendMessageModal((prev) => !prev)} className="remove-btn">
                <img src={iCloseBtn} alt="iCloseBtn" />
              </button>
              <div className="emoji">
                <button
                  onClick={() => setShowEmoji(!isShowEmoji)}
                  className="toggle-emoji-dropdown"
                >
                  <img src={iSmile} alt="iSmile" />
                </button>
                <div className={`dropdown ${isShowEmoji ? 'visible' : ''}`}>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                  <div className="smile"></div>
                </div>
              </div>
              <button className="send-btn">
                <FormattedMessage id="passwordForgot.btn.send" />
                <img src={iSendMsg} alt="iSendMsg" />
              </button>
            </div>
          </ModalInnerSendMessage>
        </Modal>
      )}
      <User>
        <div className="user-avatar">
          <img src={iconUser} alt="" />
        </div>
        <p className="user-name">Имя Фамилия игрока</p>
      </User>

      <PlayerBar>
        <span>
          <FormattedMessage id="game.step" />
        </span>
        <span className="score">280</span>
        <span>20:20:20</span>
      </PlayerBar>

      <MetaField>
        <MetaItem>
          <img src={iTap} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessPawn} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessTour} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessElephant} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessHorse} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessKing} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessQueen} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={iDelete} alt="" />
        </MetaItem>
      </MetaField>
      <BoardWrapper>
        <ChessBoard position="start"></ChessBoard>
      </BoardWrapper>
      <MetaField className="highter">
        <MetaItem>
          <img src={iTap} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessPawn} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessTour} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessElephant} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessHorse} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessKing} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={bChessQueen} alt="" />
        </MetaItem>
        <MetaItem>
          <img src={iDelete} alt="" />
        </MetaItem>
      </MetaField>
      <PlayerBar>
        <span>
          <FormattedMessage id="game.step" />
        </span>
        <span className="score">140</span>
        <span>20:20:20</span>
      </PlayerBar>

      <User>
        <div className="user-avatar">
          <img src={iconUser} alt="" />
        </div>
        <p className="user-name">Имя Фамилия игрока</p>
        <Options>
          <img src={iconBlackSIde} alt="" />
          <div>
            <button>
              <FormattedMessage id="game.best.step" />
            </button>
            <button className="disabled">
              <FormattedMessage id="game.hide.step" />
            </button>
          </div>
        </Options>
      </User>

      <PopUp className={`${isOpenedPopup ? 'opened' : ''}`}>
        <div className="trigger" onClick={() => setPopupOpen(!isOpenedPopup)}></div>
        <div className="inner">
          <Link to="/share-1-step">
            <img src={iShareRect} alt="" />
          </Link>
          <button onClick={() => setSendMessageModal(!isSendMessageModal)}>
            <img src={iSendRect} alt="" />
          </button>
          <button onClick={() => setLockedOponent(!isLockedOponent)}>
            {isLockedOponent ? <img src={iLockRect} alt="" /> : <img src={iUnlockRect} alt="" />}
          </button>

          <button>
            <img src={iSwapRect} alt="" />
          </button>
          <button onClick={() => setDrawOffer(!isDrawOffer)}>
            <img src={iDrawRect} alt="" />
          </button>
          <button onClick={() => setCapitOffer(!isCapitOffer)}>
            <img src={iBanRect} alt="" />
          </button>
          <button onClick={() => setQuitOffer(!isQuitOffer)}>
            <img src={iManuRect} alt="" />
          </button>
        </div>
      </PopUp>
    </Container>
  );
}

export default Game;

const MetaField = styled.div`
  background: #ffffff;
  border: 1px solid #cfcfcf;
  border-radius: 20px;
  width: 100%;
  max-width: 302px;
  height: 34px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding: 4px 9px;
  margin: 10px auto;
  &.highter {
    margin-top: -38px;
  }
`;
const MetaItem = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 0 1 12.5%;
  border: 1px solid #b9b9b9;
  height: 100%;
  border-radius: 8px;
  background-color: transparent;
  img {
    width: 100%;
    height: 100%;
  }
  &:not(:first-child) {
    margin-left: 6px;
  }
`;

const BoardWrapper = styled.div`
  .chess-board {
    max-width: 410px;
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 300px;
  width: 60%;
  min-height: 170px;
  padding: 21px 11px;
  background: #e5e5e5;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  span {
    font-size: 20px;
    line-height: 27px;
    color: #000000;
  }
  .c-btns {
    position: absolute;
    top: 5px;
    right: 5px;
    cursor: pointer;
  }
`;

const ModalInnerSendMessage = styled(ModalInner)`
  background: #fff;
  .wrapper {
    margin-top: 50px;
    position: relative;
    width: 100%;
    textarea {
      border: 1px solid ${theme.inputBorderColor};
      border-radius: 20px;
      resize: none;
      padding: 16px 12px;
      font-size: 14px;
      line-height: 19px;
      color: #8F8F8F;
      width: 100%;
    }
    .remove-btn {
      padding: 0;
      background: transparent;
      top: 12px;
      right: 12px;
      border: none;
      position: absolute;
    }
    .send-btn {
      padding: 0;
      background: ${theme.secondaryBg};
      border-radius: 20px;
      right: 12px;
      bottom: 14px;
      border: none;
      position: absolute;
      font-size: 12px;
      line-height: 16px;
      color: #fff;
      display: flex;align-items: center;
      justify-content: center;
      width: 123px;
      height: 38px;
    }
    .emoji {
      position: absolute;
      bottom: 14px;
      left: 12px;
    }
    .toggle-emoji-dropdown {
      border: none;
      padding: 0;
      background: transparent;
    }
    .dropdown {
      position: absolute;
      border: 1px solid ${theme.inputBorderColor};
      border-radius: 20px;
      display: none;
      width: 138px;
      height: 120px;
      flex-wrap: wrap;
      padding: 16px;
      background: #fff;
      justify-content: space-between;
      align-items: center;space-between;
      &.visible {
        display: flex;
      }
    }
    .smile {
      width: 18px;
      height: 18px;
      border-radius: 50%;
      background: #c4c4c4;
    }
  }
`;

const ModalInnerOffer = styled(ModalInner)`
  background: ${theme.primaryBg};
  padding: 11px 34px 30px;
  max-width: 349px;
  p {
    margin: 3vh 0;
    font-size: 20px;
    line-height: 27px;
  }
  .btns {
    display: flex;
    width: 100%;
    justify-content: space-between;
    button {
      background: #b0b1a1;
      border-radius: 20px;
      height: 38px;
      width: 123px;
      border: none;
      color: #ffffff;
    }
  }
  label {
    font-size: 8px;
    line-height: 11px;
    color: #8f8f8f;
    margin-top: 3vh;
    position: relative;
    img {
      transition: all 0.3s;
      position: absolute;
      left: -29px;
      width: 30px;
      height: 24px;
      z-index: 2;
      bottom: -2px;
      opacity: 0;
    }
  }
  .custom-checkbox {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    z-index: -1;
  }
  .custom-checkbox + label::before {
    content: '';
    width: 22px;
    height: 20px;
    background: #ffffff;
    border: 0.5px solid #a9a9a9;
    position: absolute;
    z-index: -1;
    bottom: -5px;
    left: -29px;
  }
  .custom-checkbox:checked + label {
    img {
      opacity: 1;
    }
  }
`;

const PopUp = styled.div`
  height: 71px;
  padding: 0 12px 8px;
  max-width: 414px;
  background: ${theme.primaryBg};
  position: absolute;
  bottom: -32px;
  left: 50%;
  transform: translateX(-50%);
  width: 100vw;
  transition: all 0.3s;

  &.opened {
    bottom: 17px;
  }
  .trigger {
    width: 150px;
    height: 40px;
    background: #fff;
    margin: 0 auto;
    border: 8px solid ${theme.primaryBg};
    border-radius: 20px;
    cursor: pointer;
  }
  .inner {
    overflow: overlay;
    display: flex;
  }
  a,
  button {
    padding: 0;
    display: flex;
    margin-right: 10px;
    border: none;
    background: transparent;
  }
`;

const Options = styled.div`
  position: absolute;
  right: 0;
  display: flex;
  div {
    display: flex;
    flex-direction: column;
    margin-left: 7px;
    button {
      padding: 0;
      background: #fff;
      border: none;
      width: 44px;
      min-height: 19px;
      border-radius: 20px;
      margin-bottom: 5px;
      font-size: 6px;
      line-height: 8px;
    }
    .disabled {
      background: #e1e1e1;
      color: #949494;
    }
  }
  @media (max-width: 389px) {
    position: static;
    margin-right: 5px;
  }
`;

const PlayerBar = styled.div`
  background: #ffffff;
  border: 1px solid #cfcfcf;
  border-radius: 20px;
  height: 21px;
  width: 300px;
  margin: 2vh auto;
  display: flex;
  padding: 5px 30px;
  align-items: center;
  justify-content: space-between;
  span {
    font-size: 15px;
    line-height: 18px;
    font-family: 'Roboto';
    font-weight: 600;
  }
  .score {
    color: #23ff10;
  }
`;

const User = styled.div`
  display: flex;
  align-items: center;
  width: 90%;
  margin: 0 auto;
  max-width: 368px;
  position: relative;
  .user-avatar {
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 38px;
    min-height: 38px;
    border-radius: 50%;
    background: #c4c4c4;
  }
  .user-name {
    margin-left: 2vw;
  }
`;

const Container = styled.div`
  min-height: 650px;
  height: calc(var(--app-height));
  background-color: ${theme.primaryBg};
  padding: 6px 11px 30px;
  overflow: hidden;
  position: relative;
`;
