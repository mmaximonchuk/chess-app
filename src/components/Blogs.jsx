import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import iconStarY from '../assets/images/blogs/StarYellow.svg';
import iconStarE from '../assets/images/blogs/StarEmpty.svg';
import iconMessage from '../assets/images/blogs/message.svg';
import iconSearch from '../assets/images/svg/search.svg';
import iconTick from '../assets/images/clan-pay/Tick.svg';
import iconShare from '../assets/images/blogs/share.svg';
import iconAdd from '../assets/images/clan-pay/AddClan.svg';

function Blogs() {
  const defaultLang = useSelector(selectLangLocale);

  const dispatch = useDispatch();

  return (
    <Container>
      <StyledLinkBack to="/all-games">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span>Blogs</span>
        <Link to="/blog-create" className="add-clan">
          <img src={iconAdd} alt="" />
        </Link>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchContainer>
          <input type="text" placeholder={vocabularyObject["messages.input.username"][defaultLang]} />
          <button>
            <img src={iconSearch} alt="" />
          </button>
        </SearchContainer>
        <BtnsWrapper>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-1" />
          <label className="label" for="custom-checkbox-1">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="blogs.by.themes"/></span>
          </label>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-2" />
          <label className="label" for="custom-checkbox-2">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="clans.by.rating"/></span>
          </label>
          <input type="checkbox" className="custom-checkbox" id="custom-checkbox-3" />
          <label className="label" for="custom-checkbox-3">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            <span><FormattedMessage id="blogs.by.member.name"/></span>
          </label>
        </BtnsWrapper>
        <WrapperBgPink>
          <div className="top">
            <div className="person-s">
              <div className="avatar"></div>
              <p className="name">Имя/Фамилия</p>
            </div>
            <div className="rating">
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarE} alt="" />
              </button>
              <button>
                <img src={iconStarE} alt="" />
              </button>
            </div>
          </div>
          <p className="topic">Название/тема блока</p>
          <div className="block"></div>
          <button className="block-description">Описание блока</button>
          <div className="bottom">
            <div className="rating rating--l">
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarY} alt="" />
              </button>
              <button>
                <img src={iconStarE} alt="" />
              </button>
              <button>
                <img src={iconStarE} alt="" />
              </button>
            </div>
            <div className="links">
              <Link to="/messages">
                <img src={iconMessage} alt="" />
              </Link>
              <Link to="/share-us-tab">
                <img src={iconShare} alt="" />
              </Link>
            </div>
          </div>
          <div className="comments">
            <div className="person">
              <div className="avatar"></div>
              <div className="inner">
                <p className="name">Имя Фамилия</p>
                <p className="comment">Комментарий</p>
              </div>
              <button className="reply">Ответить</button>
            </div>
            <div className="person">
              <div className="avatar"></div>
              <div className="inner">
                <p className="name">Имя Фамилия</p>
                <p className="comment">Комментарий</p>
              </div>
              <button className="reply">Ответить</button>
            </div>
            <div className="person">
              <div className="avatar"></div>
              <div className="inner">
                <p className="name">Имя Фамилия</p>
                <p className="comment">Комментарий</p>
              </div>
              <button className="reply">Ответить</button>
            </div>
          </div>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Blogs;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 142px);
  margin: 0 auto;
  padding: 19px 22px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .person {
    display: flex;
    flex: 1;
    align-items: center;
    margin-bottom: 3px;
    margin-left: 30px;
    position: relative;
    margin-bottom: 23px;
    align-items: flex-start;
    .reply {
      cursor: pointer;
      position: absolute;
      font-size: 12px;
      line-height: 16px;
      right: 60px;
      color: #4d4949;
      background: transparent;
      bottom: 0;
    }
    .avatar {
      min-width: 41px;
      background: #c4c4c4;
      border-radius: 50%;
      height: 41px;
    }
    .name {
      font-size: 12px;
      line-height: 16px;
      color: #5f5f5f;
      margin-bottom: 9px;
    }
    .comment {
      font-size: 12px;
      line-height: 16px;
      color: #a9a9a9;
    }
    .inner {
      display: flex;
      flex-direction: column;
      margin-left: 13px;
    }
  }

  .top {
    display: flex;
    .person-s {
      display: flex;
      flex: 1;
      align-items: center;
      margin-bottom: 3px;
      margin-left: 30px;
      .avatar {
        min-width: 30px;
        background: #c4c4c4;
        border-radius: 50%;
        height: 30px;
      }
      .name {
        font-size: 10px;
        margin-left: 6px;
        color: #5f5f5f;
      }
    }
    @media (max-width: 377px) {
      flex-direction: column;
      align-items: center;
      .person-s {
        margin-left: 0;
      }
    }
  }
  .topic {
    font-size: 10px;
    line-height: 13px;
    text-align: center;
    margin-bottom: 11px;
    color: #2d2d2d;
  }
  .bottom {
    display: flex;
    justify-content: space-between;
    .links {
      a:last-child {
        margin-left: 20px;
      }
    }
  }
  .block {
    background: #c4c4c4;
    border-radius: 20px;
    height: 229px;
    width: 100%;
    margin-bottom: 19px;
  }
  .block-description {
    cursor: pointer;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    box-sizing: border-box;
    border-radius: 20px;
    width: 100%;
    padding: 8px 10px;
    font-size: 10px;
    line-height: 13px;
    text-align: center;
    margin-bottom: 8px;
    color: #5b5b5b;
  }
  .rating {
    button {
      cursor: pointer;
      margin-left: 3px;
      background: transparent;
    }
    &--l {
      button {
        width: 30px;
      }
    }
    img {
      width: 100%;
    }
  }
  @media (max-width: 377px) {
    flex-direction: column;
    align-items: center;
    .person {
      margin-left: 0;
      .reply {
        right: 0px;
      }
    }

    .bottom {
      .links {
        a:last-child {
          margin-left: 5px;
        }
      }
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 60%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  margin-bottom: 26px;

  .custom-checkbox {
    opacity: 0;
    visibility: hidden;
    position: absolute;
    z-index: -1;
  }
  .custom-checkbox + .label {
    cursor: pointer;
    width: 160px;
    height: 24px;
    font-size: 10px;
    line-height: 13px;
    background: #ffffff;
    border-radius: 50px;
    border: none;
    padding: 0;
    position: relative;
    margin-bottom: 3px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    .tick {
      transition: all 0.3s;

      position: absolute;
      left: 0;
      border-radius: 50%;
      position: absolute;
      // background: #b0b1a1;
      width: 24px;
      border: 3px solid #b0b1a1;
      height: 24px;
      top: 0;
      color: #656565;
      img {
        opacity: 0;
        transform: translate(2px, -1px);
      }
    }
  }
  .custom-checkbox:checked + .label .tick img {
    opacity: 1;
  }
  .custom-checkbox:checked + .label {
    .tick {
      background: #b0b1a1;
      img {
        opacity: 1;
      }
    }
  }

  @media (max-width: 342px) {
    flex-direction: column;
    // height: 120px;
    a {
      width: 100%;
    }
  }
`;
const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
