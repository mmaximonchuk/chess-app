import React from 'react';
import styled, { css } from 'styled-components';
import iconArrow from '../../assets/images/svg/arrow-dropdown-down.svg';
import { Link } from 'react-router-dom';
import { theme } from '../../assets/styles/variables';

function Button({ defType, text, isLink = false, ...props }) {
  switch (defType) {
    case 'primary':
      if(isLink) {
        return <BtnPrimaryLink {...props}>{text}</BtnPrimaryLink>
      } else {
        return <BtnPrimary {...props}>{text}</BtnPrimary>
      }
    case 'no-bg':
      return (
        <BtnNoBg {...props}>
          {text} <img src={iconArrow} alt="" />{' '}
        </BtnNoBg>
      );
    default:
      return <BtnPrimaryLink {...props}>{text}</BtnPrimaryLink>;
  }
}

export default Button;

const common = css`
  transition: all 0.3s;
  border-radius: ${({ $borderRadius }) => $borderRadius || '20px'};
  align-items: center;
  display: flex;
  justify-content: center;
  min-width: ${({ $minWidth }) => $minWidth || '100px'};
  width: ${({ width }) => width || 'auto'};
  border: none;
  cursor: pointer;
  &:hover {
    img {
      transition: all 0.3s;
      transform: rotate(-90deg) translateY(5px);
    }
  }
`;

const BtnPrimaryLink = styled(Link)`
  background: ${theme.secondaryBg};
  margin: ${({ margin }) => margin || '0 0 0 0'};
  color: #fff;
  font-size: ${(props) => props.fontSize || '18px'};
  padding: ${({ padding }) => padding || '0 16px'};
  text-decoration: none;
  font-weight: ${({ fontWeight }) => fontWeight || '400'};
  height: ${({ height }) => height || '59px'};
  ${common};
`;

const BtnPrimary = styled.button`
  background: ${theme.secondaryBg};
  margin: ${({ margin }) => margin || '0 0 0 0'};
  color: #fff;
  font-size: ${(props) => props.fontSize || '18px'};
  padding: ${({ padding }) => padding || '0 16px'};
  text-decoration: none;
  font-weight: ${({ fontWeight }) => fontWeight || '400'};
  height: ${({ height }) => height || '59px'};
  ${common};
  // width: 100%;

`;

const BtnNoBg = styled(Link)`
  background: transparent;
  color: ${({ $isLg }) => ($isLg ? theme.noBgLgBtnColor : theme.noBgBtnColor)};
  font-size: ${({ fontSize }) => fontSize || '18px'};
  text-decoration: none;
  margin: ${({ margin }) => margin || '0 0 0 0'};
  line-height: 24px;
  height: ${({ height }) => height || '59px'};
  font-weight: ${({ $isLg }) => ($isLg ? '700' : '400')};
  padding: ${({ $isLg }) => ($isLg ? '15px 10px' : '10px 10px')};
  img {
    width: ${({ $isLg }) => ($isLg ? '16px' : '13px')};
    margin-left: 15px;
    transform: rotate(-90deg);
  }
  ${common};
`;
