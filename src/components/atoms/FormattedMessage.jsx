import React from 'react'
import { useSelector } from 'react-redux';

import { vocabularyObject } from '../../locales/vocabularyObject';
import { selectLangLocale } from '../../features/langs/langsSlice';

function FormattedMessage({id}) {
    const defaultLang = useSelector(selectLangLocale);
    return vocabularyObject[id][defaultLang]
}

export default FormattedMessage;
