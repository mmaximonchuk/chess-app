import React from 'react';
import styled from 'styled-components';
import iconDog from '../../assets/images/svg/input-dog.svg';
import iconLock from '../../assets/images/svg/input-padlock.svg';
import iconPerson from '../../assets/images/user-profile/input-user.svg';
import iconCard from '../../assets/images/clan-pay/credit-c.svg';
import { theme } from '../../assets/styles/variables';

function Input({ type, width, margin, $textAlign, $maxWidth, ...props }) {
  let inputIcon = null;
  switch (type) {
    case 'password':
      inputIcon = iconLock;
      break;
    case 'email':
      inputIcon = iconDog;
      break;
    case 'edit':
      inputIcon = iconPerson;
      break;
    case 'card':
      inputIcon = iconCard;
      break;
    case 'text':
      inputIcon = null;
      break;
    default:
      inputIcon = null;
  }
  return (
    <Container $maxWidth={$maxWidth} width={width} margin={margin}>
      <StyledInput type={type} {...props} />
      {inputIcon && <img src={inputIcon} alt="" />}
    </Container>
  );
}

export default Input;

const StyledInput = styled.input`
  background-color: ${({ $bgColor }) => $bgColor || theme.inputBg};
  color: ${theme.inputColor};
  font-size: ${(props) => props.fontSize || '12px'};
  border: 1px solid ${theme.inputBorderColor};
  border-radius: 20px;
  width: 100%;
  padding: ${({ padding }) => padding || '15px 20px 15px 45px'};
  text-align: ${({ $textAlign }) => $textAlign || 'center'};
`;

const Container = styled.div`
  width: ${({ width }) => width || '100%'};
  max-width: ${({ $maxWidth }) => $maxWidth || '232px'};
  margin: ${({ margin }) => margin || '0 0 0 0'};
  position: relative;
  margin-bottom: 14px;
  img {
    position: absolute;
    left: 18px;
    top: 14px;
  }
`;
