import React from 'react';
import Select from 'react-select';
import styled from 'styled-components';

export default function ReactSelect({
  options,
  $minWidth,
  $maxWidth,
  input,
  name,
  styles = false,
  ...rest
}) {
  let validOptions = options;
  if (validOptions?.EN?.value || validOptions?.humanVSHuman?.value) {
    validOptions = Object.keys(options).map((value, index) => ({
      value,
      valueInIndex: index + 1,
      label: options[value].value,
      flagImg: options[value].flag,
    }));
  } else {
    validOptions = Object.keys(options).map((value, index) => ({
      value,
      valueInIndex: index + 1,
      label: options[value],
      name
    }));
  }

  // console.log('validOptions: ', validOptions);
  return (
    <SelectWrapper $minWidth={$minWidth}>
      <Select
        styles={styles}
        options={validOptions}
        defaultValue={validOptions[0]}
        input={input}
        {...rest}
      />
    </SelectWrapper>
  );
}

const SelectWrapper = styled.div`
  min-width: ${({ $minWidth }) => $minWidth || '244px'};
  max-width: ${({ $maxWidth }) => $maxWidth || 'auto'};
`;
