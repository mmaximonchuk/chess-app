import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import { selectUsers, getUsersRequest } from '../features/game/gameSlice';
import { permissions } from '../utils/permissions';
import { gameOponent } from '../utils/gameOponent';
import { gameRestrictions } from '../utils/gameRestrictions';
import { gameRestrictionsType } from '../utils/gameRestrictionsType';
import { gameStartPosition } from '../utils/gameStartPosition';
import { gameType } from '../utils/gameType';
import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import Select from './atoms/Select';
// import SearchedUser from './molecules/SearchedUser';
import Modal from './molecules/ModalContainer';
import ChessBoard from './organism/ChessBoard';
import HeaderControls from './organism/HeaderControls';

import iconSearch from '../assets/images/svg/search.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iconBOpponent from '../assets/images/b-opponent.svg';
import iconWOpponent from '../assets/images/w-opponent.svg';
import iconClose from '../assets/images/close.svg';
import iconUser from '../assets/images/user-profile/user-default-circle.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg'
import { theme } from '../assets/styles/variables';

function CreateGame() {
  const dispatch = useDispatch();
  const defaultLang = useSelector(selectLangLocale);
  const getUsers = useSelector(selectUsers);
  const [isChoosenPartner, setChoosenPartner] = useState('');
  const [whiteGuys, setWhiteGuys] = useState([]);
  const [blackGuys, setBlackGuys] = useState([]);
  const [selectedGuy, setSelectedGuy] = useState({});

  const dot = (img) => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
      background: `url(${img})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'contain',
      borderRadius: 20,
      content: '" "',
      display: 'block',
      height: 33,
      minWidth: 50,
    },
  });

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });

  const searchStyles = {
    menu: (styles) => ({
      ...styles,
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      maxWidth: '300px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      fontSize: 14,
      justifyContent: 'space-between',
      color: '#000',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '173px',
      fontSize: 14,
      justifyContent: 'space-between',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };
  const colourStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '250px',
      maxWidth: '400px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      maxWidth: '300px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-70%, -50%)',
      fontSize: 14,
      justifyContent: 'space-between',
      color: '#000',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;
      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        ':active': {
          ...styles[':active'],
          backgroundImage: isDisabled ? null : isSelected ? `url(${img})` : null,
        },
        width: '250px',
        justifyContent: 'flex-start',
        alignItems: 'center',
        display: 'flex',
        color: theme.languagesColor,
        fontSize: 10,
        ':before': {
          background: `url(${img})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'contain',
          borderRadius: 50,
          content: '" "',
          display: 'block',
          marginRight: 10,
          height: 37,
          width: 20,
          height: 20,
        },
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      ...dot(data.flagImg),
      fontSize: 14,
      justifyContent: 'space-between',
      left: '50%',
      top: '50%',
      transform: 'translate(-55%, -50%)',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };
  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getUsersRequest({ Token: token }));
  }, []);

  const moveAllToRight = () => {
    const uniqeArray = new Set([...whiteGuys, ...blackGuys]);
    setWhiteGuys([...uniqeArray]);
    setBlackGuys([]);
  };
  const moveAllToLeft = () => {
    const uniqeArray = new Set([...whiteGuys, ...blackGuys]);
    setBlackGuys([...uniqeArray]);
    setWhiteGuys([]);
  };
  const moveOneToRight = () => {
    if (!containsElement(whiteGuys, selectedGuy)) {
      setBlackGuys([...blackGuys.filter((guy) => guy?.userID !== selectedGuy?.userID)]);
      setWhiteGuys([...whiteGuys, selectedGuy]);
    }
  };
  const moveOneToLeft = () => {
    if (!containsElement(blackGuys, selectedGuy)) {
      setWhiteGuys([...whiteGuys.filter((guy) => guy?.userID !== selectedGuy?.userID)]);
      setBlackGuys([...blackGuys, selectedGuy]);
    }
  };
  function containsElement(arrOfObj1, guy) {
    for (let i = 0; i < arrOfObj1.length; i++) {
      if (arrOfObj1[i].userID === guy.userID) {
        return true;
      }
    }
    return false;
  }

  // console.log(selectedGuy);
  return (
    <Container>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/all-games">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="create.game.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchContainer>
          <input type="text" placeholder={vocabularyObject["create.game.input.name"][defaultLang]} />
        </SearchContainer>
        <WrapperBgPink>
          <textarea type="text" placeholder={vocabularyObject["create.game.input.description"][defaultLang]} />
          <div style={{ marginBottom: '3vh' }}>
            <ChessBoard></ChessBoard>
          </div>

          <div>
            <Select placeholder={'Выберите тип игры'} options={gameType} styles={colourStyles} />
          </div>
          <div>
            <Select
              defaultValue={gameStartPosition[0]}
              placeholder={<FormattedMessage id="create.game.start.pos"/>}
              options={gameStartPosition}
              styles={searchStyles}
            />
          </div>
          <div>
            <Select
              defaultValue={gameRestrictionsType[0]}
              placeholder={<FormattedMessage id="create.game.limit.type"/>}
              options={gameRestrictionsType}
              styles={searchStyles}
            />
          </div>
          <div>
            <Select
              defaultValue={gameRestrictions[0]}
              placeholder={<FormattedMessage id="create.game.opponent"/>}
              options={gameRestrictions}
              styles={searchStyles}
            />
          </div>
          <div>
            <Select
              onChange={(selectedOption) => {
                setChoosenPartner(selectedOption.value);
                console.log(selectedOption.value);
              }}
              defaultValue={gameOponent[0]}
              placeholder={<FormattedMessage id="create.game.opponent"/>}
              options={gameOponent}
              styles={searchStyles}
            />
            {/* Если выбран человек, то появится модалка с подальшими настройками */}
            {isChoosenPartner === 'human' ? (
              <Modal closeModal={setChoosenPartner}>
                <ModalInner>
              <img onClick={() => setChoosenPartner(prev => !prev)} className="c-btns" src={iCloseBtn} alt="" />
                  <SearchContainerModal>
                    <input
                      type="text"
                      placeholder={vocabularyObject['messages.input.username'][defaultLang]}
                    />
                    <button>
                      <img src={iconSearch} alt="" />
                    </button>
                  </SearchContainerModal>
                  <SuggestionDetails>
                    {getUsers &&
                      getUsers.map((user) => {
                        return (
                          <OpponentItem key={user.userID}>
                            <img className="user-img" src={iconUser} alt="iconUser" />
                            <img
                              className="black"
                              onClick={() => {
                                if (!blackGuys.includes(user)) setBlackGuys([...blackGuys, user]);
                              }}
                              src={iconBOpponent}
                              alt=""
                            />
                            <span>{`${user.firstName || 'Имя'} ${
                              user.secondName || 'Фамилия'
                            }`}</span>
                            <img
                              onClick={() => {
                                if (!whiteGuys.includes(user)) setWhiteGuys([...whiteGuys, user]);
                              }}
                              className="white"
                              src={iconWOpponent}
                              alt=""
                            />
                          </OpponentItem>
                        );
                      })}
                  </SuggestionDetails>

                  <div className="middleware">
                    <div className="black">
                      <img src={iconBOpponent} alt="" />
                      <span>Black</span>
                    </div>
                    <div className="white">
                      <span>White</span>
                      <img src={iconWOpponent} alt="" />
                    </div>
                  </div>

                  <SuggestionDetails2>
                    <Section>
                      {blackGuys &&
                        blackGuys.map((user) => {
                          return (
                            <AvailableOpponent
                              key={user.userID}
                              onClick={() => setSelectedGuy(user)}
                              style={{
                                background:
                                  selectedGuy.userID === user.userID
                                    ? 'rgb(239 228 228)'
                                    : 'transparent',
                              }}
                            >
                              <div className="circle"></div>
                              <span>{`${user.firstName || 'Имя'} ${
                                user.secondName || 'Фамилия'
                              }`}</span>
                              <img
                                onClick={() => {
                                  setBlackGuys((guys) => {
                                    return blackGuys.filter((guy) => {
                                      return guy.userID !== user.userID;
                                    });
                                  });
                                }}
                                src={iconClose}
                                alt="iconClose"
                              />
                            </AvailableOpponent>
                          );
                        })}
                    </Section>
                    <InterSection>
                      <button onClick={moveAllToRight}>{'>>'}</button>
                      <button onClick={moveOneToRight}>{'>'}</button>
                      <button onClick={moveOneToLeft}>{'<'}</button>
                      <button onClick={moveAllToLeft}>{'<<'}</button>
                    </InterSection>
                    <Section>
                      {whiteGuys &&
                        whiteGuys.map((user) => {
                          return (
                            <AvailableOpponent
                              key={user.userID}
                              onClick={() => setSelectedGuy(user)}
                              style={{
                                background:
                                  selectedGuy.userID === user.userID
                                    ? 'rgb(239 228 228)'
                                    : 'transparent',
                              }}
                            >
                              <div className="circle"></div>
                              <span>{`${user.firstName || 'Имя'} ${
                                user.secondName || 'Фамилия'
                              }`}</span>
                              <img
                                onClick={() => {
                                  setWhiteGuys((guys) => {
                                    return whiteGuys.filter((guy) => {
                                      return guy.userID !== user.userID;
                                    });
                                  });
                                }}
                                src={iconClose}
                                alt="iconClose"
                              />
                            </AvailableOpponent>
                          );
                        })}
                    </Section>
                  </SuggestionDetails2>
                </ModalInner>
              </Modal>
            ) : (
              ''
            )}
          </div>
          <div>
            <Select
              defaultValue={permissions[0]}
              placeholder={<FormattedMessage id="tournament.privacy"/>}
              options={permissions}
              styles={searchStyles}
            />
          </div>
          <Button
            defType="primary"
            text={<FormattedMessage id="tournament.create"/>}
            $fontSize="20px"
            width="123px"
            height="52px"
            margin="3vh auto 5vh"
          />
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default CreateGame;

const Section = styled.div`
  border: 1px solid ${theme.inputBorderColor};
  display: flex;
  flex: 1 1 80%;
  border-radius: 20px;
  flex-direction: column;
  align-items: center;
  padding: 10px;
  overflow: overlay;
`;
const InterSection = styled.div`
  display: flex;
  flex: 1 1 20%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  button {
    background-color: ${theme.secondaryBg};
    width: 25px;
    height: 25px;
    font-size: 15px;
    font-weight: 900;
    color: #fff;
    cursor: pointer;
    border: none;
    padding: 0;
    border-radius: 5px;
    margin-top: 1vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
const AvailableOpponent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 5px;
  border-radius: 20px;
  cursor: pointer;
  span {
    font-size: 14px;
    line-height: 1.3;
  }
  .circle {
    min-width: 16px;
    height: 16px;
    border-radius: 50%;
    background-color: ${theme.secondaryBg};
  }
`;

const SuggestionDetails2 = styled.div`
  width: 100%;
  background: #fff;
  border: 1px solid ${theme.inputBorderColor};
  height: 160px;
  overflow: overlay;
  border-radius: 20px;
  display: flex;
  justify-content: space-between;
  padding: 4px;
`;
const SuggestionDetails = styled.div`
  width: 100%;
  background: #fff;
  border: 1px solid ${theme.inputBorderColor};
  height: 270px;
  overflow: overlay;
  border-radius: 20px;
`;
const OpponentItem = styled(Link)`
  display: flex;
  text-decoration: none;
  align-items: center;
  color: ${theme.inputColor};
  font-size: 14px;
  position: relative;
  justify-content: center;
  margin-top: 3vh;
  span {
    margin-left: 15px;
    margin-right: 15px;
  }
  .black,
  .white {
    width: 15px;
  }
  .user-img {
    position: absolute;
    left: 20px;
  }
`;
const SearchContainerModal = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 300px;
  width: 90%;
  max-width: 563px;
  min-height: 170px;
  padding: 21px 11px;
  background: #f7f4ea;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  .c-btns {
    position: absolute;
    top: 5px;
    right: 5px;
    cursor: pointer;
  }
  .middleware {
    display: flex;
    align-items: center;
    width: 203px;
    justify-content: space-between;
    margin: 2vh auto;
    .black,
    .white {
      display: flex;
      align-items: center;
      span {
        margin-left: 10px;
        margin-right: 10px;
      }
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 17px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 60px);
  margin: 0 auto;
  padding: 19px 22px 80px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  text-align: center;
  textarea {
    // dispaly: block;
    resize: none;
    width: 80%;
    height: 130px;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 14px;
    color: ${theme.textFieldColor};
    padding: 5px;
    margin: 0 auto 5vh;
  }
`;

const Container = styled.div`
  //   overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 20px;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

const SearchContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 14px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 14px;
    line-height: 13px;
    height: 50px;
    color: ${theme.textFieldColor};
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Board = styled.div`
  display: flex;
  flex-wrap: wrap;
  min-height: 300px;
  max-width: 439px;
  margin: 0 auto;
  .cell {
    position: relative;
    display: flex;
    flex: 1 1 12.5%;
    height: 40px;
    background-color: #b58863;
    justify-content: center;
    align-items: center;
    img {
      width: 90%;
      height: 90%;
    }
    .row-count {
      position: absolute;
      top: 1px;
      left: 1px;
      font-size: 10px;
      color: #b58863;
    }
    .column-count {
      position: absolute;
      bottom: 1px;
      right: 1px;
      font-size: 10px;
      color: #b58863;
      &--another-color {
        color: #f0d9b5;
      }
    }
    &.reversed {
      &:nth-child(2n + 1) {
        background-color: #b58863;
      }
      background-color: #f0d9b5;
      .row-count {
        color: #f0d9b5;
      }
    }
    &:nth-child(2n + 1) {
      background-color: #f0d9b5;
    }
  }
`;
