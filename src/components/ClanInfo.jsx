import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';


import iconUserSmall from '../assets/images/user-profile/small-user-icon.svg';
import { getClansRequest, selectClans } from '../features/clans/clansSlice';

// import iconSearch from '../assets/images/svg/search.svg';
import iconShare from '../assets/images/clan-pay/share-us-tab.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

import { theme } from '../assets/styles/variables';

function ClanInfo() {
  // const defaultLang = useSelector(selectLangLocale);
  const getClans = useSelector(selectClans);

  const params = useParams();
  // console.log('params: ', params);
  const dispatch = useDispatch();
  const [clanInfo, setClanInfo] = useState(null);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getClansRequest({ Token: token }));
  }, []);

  useEffect(() => {
    if (getClans) {
      setClanInfo(getClans.filter((clan) => clan.clanID == params.clanId)[0]);
    }
    console.log(clanInfo);
  }, [getClans]);

  return (
    <Container>
      <StyledLinkBack to="/clans">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="clans.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <SearchedUser data={clanInfo} />
        <WrapperBgPink>
          <ClanInner>
            <p className="textarea">
              <article>{clanInfo?.clanDescription || 'Информация о клане'}</article>
            </p>
            <Link to="rating-tab" className="participants">
            <FormattedMessage id="clans.members"/>: {clanInfo?.membersCount}
            </Link>
            <Link to="rating" className="rating">
            <FormattedMessage id="clans.reting"/>: {clanInfo?.clanRating}
            </Link>
            <button className="connect"><FormattedMessage id="clans.connect"/></button>
            <Link to="share-us-tab" className="share">
              <img src={iconShare} alt="" />
            </Link>
          </ClanInner>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ClanInfo;
const ClanInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .share {
    margin-top: 35px;
  }
  .participants,
  .rating,
  .connect {
    background: #b0b1a1;
    border-radius: 20px;
    padding: 10px 15px;
    text-decoration: none;
    font-size: 15px;
    line-height: 20px;
    text-align: center;
    margin-bottom: 2vh;
    min-width: 196px;
    color: #ffffff;
  }
  .rating {
    min-width: 154px;
  }
  .connect {
    margin-top: 4vh;
  }
  .textarea {
    dispaly: flex;
    align-items: center;
    justify-content: center;
    resize: none;
    width: 80%;
    height: 116px;
    overflow: overlay;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 14px;
    color: ${theme.textFieldColor};
    padding: 5px;
    margin: 0 auto 2vh;
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 67px);
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

function SearchedUser({ data = {} }) {
  return (
    <ChatItem className="chat-item">
      <Avatar className="ava">
        <img src={iconUserSmall} alt="" />
      </Avatar>
      <Body className="chat-body">
        <p className="name">{`${data?.clanName || 'Имя'}`}</p>
      </Body>
    </ChatItem>
  );
}

const ChatItem = styled.div`
  display: flex;
  align-items: center;
  text-decoration: none;
  width: 90%;
  margin: 0 auto 27px;
  .share-btn,
  .info-btn {
    cursor: pointer;
    padding: 0;
    border: none;
    background: transparent;
    position: absolute;
    right: 50px;
    top: 5px;
    width: 33px;
    height: 33px;
    img {
      width: 100%;
    }
  }
  .share-btn {
    padding: 5px;
    background: ${theme.secondaryBg};
    border-radius: 50%;
  }
  .info-btn {
    right: 12px;
    top: 5px;
  }
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
  overflow: hidden;
  // img {
  //   width: 100%;
  //   height: 100%;
  //   object-fit: cover;
  // }
`;

const Body = styled.div`
  flex: 1;
  position: relative;
  .name {
    margin-left: -9px;
    padding: 8px 9px;
    background: #ffffff;
    border-radius: 20px;
    width: 100%;
    height: 100%;
    color: ${({ shortenDescription }) => (!shortenDescription ? '#505050' : '#a9a9a9')};
    font-size: 15px;
  }
`;
