import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { selectLangLocale } from '../features/langs/langsSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import Button from './atoms/Button';
import Input from './atoms/Input';
import FormattedMessage from './atoms/FormattedMessage';
import HeaderControls from './organism/HeaderControls';

import imgLogo from '../assets/images/Лого.svg';
import imgArrowBack from '../assets/images/svg/arrow-back.svg';

import { theme } from '../assets/styles/variables';
import { StartContainer } from './styled';

function EmailPasswordForgot() {
  const defaultLang = useSelector(selectLangLocale);
  return (
    <StartContainer justifyContent="flex-start">
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/entrance">
        <img src={imgArrowBack} alt="imgArrowBack" />
      </StyledLinkBack>
      <StyledImg src={imgLogo} alt="" />

      <Title>Democrat chess</Title>
      <Tip>
        <FormattedMessage id="emailPasswordForgot.par.enterNewPassword" />
      </Tip>

      <Input
        bgColor="#fff"
        type="password"
        placeholder={vocabularyObject['signUp.input.password'][defaultLang]}
      />
      <Input
        bgColor="#fff"
        type="password"
        placeholder={vocabularyObject['signUp.input.password.again'][defaultLang]}
      />

      <Button
        fontWeight={700}
        fontSize="14px"
        height="42px"
        margin="2vh 0 0 0"
        defType="primary"
        text={<FormattedMessage id="passwordForgot.btn.send" />}
        to="/entrance"
      />
    </StartContainer>
  );
}

export default EmailPasswordForgot;

const StyledImg = styled.img`
  position: relative;
  width: 61px;
  left: 10px;
  max-width: 120px;
`;

const Title = styled.h1`
  font-size: 40px;
  line-height: 53px;
  margin-top: 0;
  margin-bottom: 2vh;
  color: #000000;
`;
const Tip = styled.p`
  color: ${theme.greyTextColor};
  margin-bottom: 2vh;
  font-size: 14px;
`;

const StyledLinkBack = styled(Link)`
  position: absolute;
  top: 80px;
  left: 8vw;
  img {
  }
`;
