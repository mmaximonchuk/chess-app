import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import HeaderControls from './organism/HeaderControls';
import ChatUserItem from '../components/molecules/ChatUserItem';
import FormattedMessage from './atoms/FormattedMessage';
import { getUnreadMessageRequest, selectNewMessages } from '../features/message/messageSlice';

import imgArrowBack from '../assets/images/svg/balck-arrow-back.svg';

function Messages() {
  const dispatch = useDispatch();
  const messagesData = useSelector(selectNewMessages);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getUnreadMessageRequest({ Token: token }));
  }, []);

  return (
    <Container>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/profile">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span>{<FormattedMessage id="messages.par.messages" />}</span>
      </StyledLinkBack>
      <WrapperBgTurquoise>
        <WrapperBgPink>
          {messagesData
            ? messagesData.map((message) => {
                console.log(message);
                return (
                  <ChatUserItem
                    key={message.senderUserID}
                    message={message}
                    senderUserID={message.senderUserID}
                  />
                );
              })
            : ''}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Messages;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 105px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 26px 16px 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: 100%;
  margin: 0 auto;
  padding: 36px 40px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
