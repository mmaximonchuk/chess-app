import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Input from './atoms/Input';

import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

function Paypal() {
  const defaultLang = useSelector(selectLangLocale);

  return (
    <Container>
      <StyledLinkBack to="/payment">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="payment.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          <Form>
            <div className="title">PayPal</div>
            <Input $bgColor="#fff" type="email" placeholder={vocabularyObject["signUp.input.email"][defaultLang]} />
            <Input $bgColor="#fff" type="password" placeholder={vocabularyObject["signUp.input.password"][defaultLang]} />
            <button className="enter-btn"><FormattedMessage id="entrance.btn.enter"/></button>
          </Form>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Paypal;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  img {
    display: none;
  }
  input {
    padding: 15px 20px 15px;
  }
  .enter-btn {
    background: #b0b1a1;
    border-radius: 20px;
    height: 42px;
    padding: 5px 20px;
    font-size: 20px;
    border: none;
    cursor: pointer;
    font-weight: 700;
    color: #ffffff;
    margin-top: 24px;
  }
  .title {
    font-size: 30px;
    line-height: 40px;

    color: #444444;
    margin-bottom: 19px;
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100%);
  margin: 0 auto;
  padding: 20vh 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
