// import { Link } from 'react-router-dom';
import styled from "styled-components";
import {theme} from "../assets/styles/variables";

const appHeight = () => {
  const doc = document.documentElement
  doc.style.setProperty('--app-height', `${window.innerHeight - 72}px`)
}
window.addEventListener('resize', appHeight)
appHeight()

export const StartContainer = styled.div`
  background: ${(props) => props.bgColor ? props.bgColor : theme.primaryBg};
  // width: 100vw;
  min-height: var(--app-height);
  padding: 2vh 37px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: ${({justifyContent}) => justifyContent || 'center'};
  text-align: center;
  position: relative;
  form {
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .head {
    position: fixed;
    top: 0;
    .header-control {
      width: 100vw;
    }
  }
`;


