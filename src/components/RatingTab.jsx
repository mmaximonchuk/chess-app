import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import Modal from './molecules/ModalContainer';
import Select from './atoms/Select';
import HeaderControls from './organism/HeaderControls';
import { gameRanksRequest, selectGameRanks } from '../features/game/gameSlice';

import SearchedUser from './molecules/SearchedUser';
// import iconSearch from '../assets/images/svg/search.svg';
import iconUser from '../assets/images/user-profile/default-user.svg';
import iconStar from '../assets/images/user-profile/Star.svg';
import iconSendR from '../assets/images/user-profile/VectorSendReverse.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg'
import { theme } from '../assets/styles/variables';

function RatingTab() {
  // const defaultLang = useSelector(selectLangLocale);
  const getRanks = useSelector(selectGameRanks);

  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);
  const [userData, setUserData] = useState({});

  const getInfoCallBack = (user) => {
    setUserData(user);
    setUserInfo(!isUserInfo);
  };

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });
  const searchStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '138px',
      left: '50%',
      transform: 'translate(-50%, 30px)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto',
      border: 'none',
      maxWidth: '138px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '110px',
      left: '43%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      fontSize: 10,
      justifyContent: 'space-between',
      color: '#000',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '138px',
      fontSize: 10,
      justifyContent: 'space-between',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(gameRanksRequest({ Token: token }));
  }, []);

  return (
    <Container>

      <StyledLinkBack to="/profile">
        <span><FormattedMessage id="share.rating.title"/></span>
      </StyledLinkBack>
      {isUserInfo && (
        <Modal closeModal={setUserInfo}>
          <ModalInner>
            <UserInnerWrap>
            <img onClick={() => setUserInfo(prev => !prev)} className="c-btns" src={iCloseBtn} alt="" />
              <div className="container">
                <div className="data">
                  <div className="circle">
                    <img src={iconUser} alt="iconUser" />
                  </div>
                  <p className="name-surname">
                    {userData?.firstName || 'Имя'} <br />
                    {userData?.secondName || 'Фамилия'}
                  </p>
                </div>
                <div className="rating">
                  <Stats>
                    <p>24-<FormattedMessage id="share.rating.wins"/></p>
                    <p>4-<FormattedMessage id="share.rating.loses"/></p>
                  </Stats>
                  <User to="/profile">
                    <span>133</span>
                  </User>
                  <Stats>
                    <p>
                    <FormattedMessage id="share.rating.matches"/> <br /> 24
                    </p>
                  </Stats>
                </div>
                <div className="meta-data">
                  <Stats>
                  <FormattedMessage id="share.rating.loses"/> <br />5
                  </Stats>
                  <Stats>
                  <FormattedMessage id="share.rating.draws"/> <br />6
                  </Stats>
                </div>
              </div>

              <div className="bottom">
                <button className="option-btn"><FormattedMessage id="share.rating.friend.request"/></button>
                <button className="option-btn option-btn--send">
                  <img src={iconSendR} alt="iconSendR" />
                </button>
                <Select
                  defaultValue={gameRequest[0]}
                  placeholder={<FormattedMessage id="share.rating.game.request"/>}
                  options={gameRequest}
                  styles={searchStyles}
                  className="select"
                  $minWidth="100px"
                  $maxWidth="138px"
                />
                <button className="option-btn option-btn--large"><FormattedMessage id="share.rating.close"/></button>
              </div>
            </UserInnerWrap>
          </ModalInner>
        </Modal>
      )}
      <WrapperBgTurquoise>
        <BtnsWrapper>
          <Button text={<FormattedMessage id="share.rating.local"/>} defType="primary" height="35px" width="30%" padding="10px 8px" />
          <Button text={<FormattedMessage id="share.rating.world"/>}  defType="primary" height="35px" width="30%" padding="10px 8px" />
          <Button text={<FormattedMessage id="share.rating.friend"/>}  defType="primary" height="35px" width="30%" padding="10px 8px" />
        </BtnsWrapper>
        <WrapperBgPink>
          {getRanks ? (
            getRanks.map((user) => {
              // console.log(user);
              return (
                <SearchedUser
                  data={user}
                  callback={() => getInfoCallBack(user)}
                  to=""
                  shortenDescription
                  showRating
                />
              );
            })
          ) : (
            <Loader>
              <div class="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
            </Loader>
          )}

          {/* <SearchedUser callback={setUserInfo} to="" shortenDescription showRating />
          <SearchedUser callback={setUserInfo} to="" shortenDescription showRating /> */}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default RatingTab;

const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
const UserInnerWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  min-width: 320px;
  width: 100%;
  // margin: 0 auto;
  flex-direction: column;
  .c-btns {
    position: absolute;
    top: 5px;
    right: 5px;
    cursor: pointer;
  }
  .bottom {
    background: #f7f4ea;
    height: 62px;
    display: flex;
    align-items: center;
    width: 100%;
    min-width: 320px;
    padding: 8px 9px;
    justify-content: center;
    .select {
      width: 138px;
      margin-left: 5px;
      margin-right: 5px;
    }
    .option-btn {
      width: 68px;
      height: 43px;
      border-radius: 15px;
      border: none;
      font-size: 8px;
      display: flex;
      align-items: center;
      justify-content: center;
      padding: 0;
      color: #fff;
      background: ${theme.secondaryBg};
      line-height: 13px;
      margin-right: auto;
      &--large {
        margin-left: auto;
        font-size: 12px;
        line-height: 16px;
      }
      &--send {
        width: 26px;
        height: 26px;
        margin-left: 5px;
        img {
          width: 50%;
        }
      }
    }
  }
  .container {
    max-width: 100%;
    margin: 0 auto;
    padding: 0 23px;
  }
  .meta-data {
    width: 60%;
    display: flex;
    justify-content: space-around;
    margin: 5px auto 16px;
  }
  .data {
    display: flex;
    font-size: 15px;
    line-height: 20px;
    text-align: left;
    align-self: flex-start;
    align-items: center;
    margin-bottom: 3px;
    // margin: 0 auto 3px;
    color: #8f8f8f;
    .circle {
      background: ${theme.secondaryBg};
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      position: relative;
      width: 54px;
      height: 54px;
      margin-right: 12px;
      img {
        width: 60%;
        height: 60%;
      }
    }
  }
  .rating {
    width: 252px;
    margin: 0 uto;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
const Stats = styled.div`
  background: ${theme.secondaryBg};
  border-radius: 20px;
  min-width: 64px;
  min-height: 33px;
  padding: 5px 5px;
  font-size: 8px;
  line-height: 9px;
  color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;
const User = styled.div`
  background: ${theme.secondaryBg};
  border-radius: 50%;
  width: 88px;
  height: 88px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  span {
    z-index: 2;
    font-weight: 900;
    font-size: 20px;
    line-height: 23px;
    font-family: 'Roboto';
    color: #fff;
  }
  &::before {
    content: '';
    position: absolute;
    width: 90px;
    height: 87px;
    background-image: url(${iconStar});
    z-index: 1;
    left: -5px;
    top: -4px;
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 320px;
  max-width: 352px;
  width: 60%;
  min-height: 170px;
  padding: 10px 0 0;
  background: #efecdc;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;

  // span {
  //   font-size: 20px;
  //   line-height: 27px;
  //   color: #000000;
  // }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 101px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 9px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 73px);
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 56px auto 18px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

// const SearchContainer = styled.div`
//   width: 70%;
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   margin: 0 auto 14px;
//   input {
//     width: 100%;
//     background: #ffffff;
//     border: 1px solid #cfcfcf;
//     border-radius: 20px;
//     text-align: center;
//     font-size: 10px;
//     line-height: 13px;
//     height: 24px;
//     color: #c8c8c8;
//   }
//   button {
//     margin-left: 5px;
//     background: #b0b1a1;
//     border-radius: 20px;
//     border: none;
//     width: 32px;
//     height: 24px;
//     display: flex;
//     align-items: center;
//     justify-content: center;
//   }
// `;
const BtnsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 26px;
  @media (max-width: 342px) {
    flex-direction: column;
    height: 120px;
    a {
      width: 100%;
    }
  }
`;
