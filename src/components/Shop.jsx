import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import HeaderControls from './organism/HeaderControls';
import ChatUserItem from '../components/molecules/ChatUserItem';
import FormattedMessage from './atoms/FormattedMessage';
import Loader from './atoms/Loader';
import { getProductsRequest, selectProducts } from '../features/products/productsSlice';

import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import ShopItem from './molecules/ShopItem';

function Shop() {
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getProductsRequest({ Token: token }));
  }, []);

  return (
    <Container>
      {/* <HeaderControls /> */}
      <StyledLinkBack to="/profile">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="shop.title"/></span>
      </StyledLinkBack>
      <WrapperBgTurquoise>
        <WrapperBgPink>
          {products ? (
            products.map((product) => {
              console.log(product);
              return (
                <ShopItem
                  description={product.description}
                  name={product.name}
                  discountedPrice={product.discount}
                  price={product.price}
                />
              );
            })
          ) : (
            <Loader />
          )}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Shop;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 26px 0 18px;
  overflow: hidden;
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: 100%;
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  padding: 36px 40px;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .loader {
    width: 100%;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
