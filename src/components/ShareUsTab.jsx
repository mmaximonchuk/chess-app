import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
// import Stepper from './molecules/Stepper';
import SearchedUser from './molecules/SearchedUser';
import Modal from './molecules/ModalContainer';

import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
// import imgShare from '../assets/images/game/VectorShare.svg';
import iFb from '../assets/images/shareTo/fb.svg';
import iWhatsApp from '../assets/images/shareTo/whats-app.svg';
import iViber from '../assets/images/shareTo/viber.svg';
import iMailRu from '../assets/images/shareTo/mail-ru.svg';
import iMessanger from '../assets/images/shareTo/messanger.svg';
import iTelegram from '../assets/images/shareTo/telega.svg';
import iMail from '../assets/images/shareTo/mail.svg';
import iTwitter from '../assets/images/shareTo/twitter.svg';
import iOk from '../assets/images/shareTo/ok.svg';
import iVk from '../assets/images/shareTo/vk.svg';
import iT from '../assets/images/shareTo/t.svg';
import iIn from '../assets/images/shareTo/in.svg';
import iHatena from '../assets/images/shareTo/hatena.svg';
import iPinterest from '../assets/images/shareTo/pinterest.svg';
import iNote from '../assets/images/shareTo/note.svg';
import iWorkPlace from '../assets/images/shareTo/w-place.svg';
import iI from '../assets/images/shareTo/i.svg';
import iArrow from '../assets/images/shareTo/arrow.svg';
import iLine from '../assets/images/shareTo/line.svg';
import iReddit from '../assets/images/shareTo/reddit.svg';
import iEye from '../assets/images/shareTo/eye.svg';
import iconShare from '../assets/images/game/VectorShare.svg';
import iconSearch from '../assets/images/svg/search.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg'
import { theme } from '../assets/styles/variables';

function ShareGame3() {
  const defaultLang = useSelector(selectLangLocale);
  const [isOpenShareModal, setOpenShareModal] = useState(false);

  return (
    <Container>
      {/* <HeaderControls /> */}

      <StyledLinkBack to="/game">
        <span><FormattedMessage id="share.us.tab.title"/></span>
      </StyledLinkBack>
      {isOpenShareModal && (
        <Modal closeModal={setOpenShareModal}>
          <ModalInnerSendMessage>
            
            <div className="wrapper">

              <img onClick={() => setOpenShareModal(prev => !prev)} className="c-btns" src={iCloseBtn} alt="" />
              <StyledLinkBackModal onClick={() => setOpenShareModal(!isOpenShareModal)}>
                <img src={imgArrowBack} alt="imgArrowBack" />
                <span><FormattedMessage id="share.us.tab.title"/></span>
              </StyledLinkBackModal>
              <SearchContainer>
                <input
                  type="text"
                  placeholder={vocabularyObject['messages.input.username'][defaultLang]}
                />
                <button>
                  <img src={iconSearch} alt="" />
                </button>
              </SearchContainer>
              <OfferListModal>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
                <div className="item">
                  <SearchedUser to="#" shortenDescription />
                </div>
              </OfferListModal>
              <button className="send-btn">
                <FormattedMessage id="passwordForgot.btn.send"/>
                <img src={iconShare} alt="iSendMsg" />
              </button>
            </div>
          </ModalInnerSendMessage>
        </Modal>
      )}
      <WrapperBgTurquoise>
        <WrapperBgPink>
          <OfferList onClick={() => setOpenShareModal(!isOpenShareModal)}>
            <button className="share-way">
              <img src={iFb} alt="Facebook" />
            </button>
            <button className="share-way">
              <img src={iWhatsApp} alt="WhatsApp" />
            </button>
            <button className="share-way">
              <img src={iViber} alt="Viber" />
            </button>
            <button className="share-way">
              <img src={iMailRu} alt="MailRu" />
            </button>
            <button className="share-way">
              <img src={iMessanger} alt="Facebook Messanger" />
            </button>
            <button className="share-way">
              <img src={iTelegram} alt="Telegram" />
            </button>
            <button className="share-way">
              <img src={iMail} alt="Mail" />
            </button>

            <button className="share-way">
              <img src={iTwitter} alt="Twitter" />
            </button>
            <button className="share-way">
              <img src={iOk} alt="Ok" />
            </button>
            <button className="share-way">
              <img src={iVk} alt="Vk" />
            </button>
            <button className="share-way">
              <img src={iT} alt="T" />
            </button>
            <button className="share-way">
              <img src={iIn} alt="In" />
            </button>
            <button className="share-way">
              <img src={iHatena} alt="Hatena" />
            </button>
            <button className="share-way">
              <img src={iPinterest} alt="Pinterest" />
            </button>

            <button className="share-way">
              <img src={iNote} alt="Note" />
            </button>
            <button className="share-way">
              <img src={iWorkPlace} alt="WorkPlace" />
            </button>
            <button className="share-way">
              <img src={iI} alt="I" />
            </button>
            <button className="share-way">
              <img src={iArrow} alt="Arrow" />
            </button>
            <button className="share-way">
              <img src={iLine} alt="Line" />
            </button>
            <button className="share-way">
              <img src={iReddit} alt="Reddit" />
            </button>
            <button className="share-way">
              <img src={iEye} alt="Eye" />
            </button>
          </OfferList>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ShareGame3;

const SearchContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 26px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const OfferListModal = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  height: 137px;
  background: #ffffff;
  border: 1px solid #a4a4a4;
  border-radius: 20px;
  padding: 20px 22px;
  overflow: overlay;
  @media (max-width: 514px) {
    padding: 10px 10px;
  }
  .item {
    display: flex;
    width: 50%;
    .chat-item {
      width: 100%;
      margin-bottom: 11px;
      .chat-body {
        height: 22px;
      }
      .ava {
        width: 23px;
        height: 23px;
        display: flex;
        align-items: center;
        justify-content: center;
        img {
          width: 70%;
          height: 70%;
        }
      }
      .name {
        padding: 5px;
        background: #e3e3e3;
        font-size: 10px;
        color: #8f8f8f;
        line-height: 13px;
        text-align: left;
      }
    }
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 300px;
  width: 60%;
  min-height: 170px;
  padding: 4px 11px 13px;
  background: #e5e5e5;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  span {
    font-size: 20px;
    line-height: 27px;
    color: #000000;
  }
`;

const ModalInnerSendMessage = styled(ModalInner)`
  background: #fff;
  
  .wrapper {
    position: relative;
    width: 100%;
    .c-btns {
      position: absolute;
      top: 5px;
      right: 5px;
      cursor: pointer;
    }
    .send-btn {
      cursor: pointer;
        margin-top: 15px;
        margin-left: auto;
        margin-right: 15px;
      padding: 0;
      background: ${theme.secondaryBg};
      border-radius: 20px;
      border: none;
      font-size: 8px;
      line-height: 11px;
      color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 82px;
      height: 24px;
      img {
        cursor: pointer;
          margin-left: 5px;
          width: 16px;
          height: 14px;
      }
    }
    .emoji {
      position: absolute;
      bottom: 14px;
      left: 12px;
    }
    .toggle-emoji-dropdown {
      border: none;
      padding: 0;
      background: transparent;
    }
    .dropdown {
      position: absolute;
      border: 1px solid ${theme.inputBorderColor};
      border-radius: 20px;
      display: none;
      width: 138px;
      height: 120px;
      flex-wrap: wrap;
      padding: 16px;
      background: #fff;
      justify-content: space-between;
      align-items: center;space-between;
      &.visible {
        display: flex;
      }
    }
    .smile {
      width: 18px;
      height: 18px;
      border-radius: 50%;
      background: #c4c4c4;
    }
  }
`;

const OfferList = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  .share-way {
    cursor: pointer;
    diaply: flex;
    flex: 1 1 25%;
    border: none;
    background: transparent;
    paddong: 0;
    margin-bottom: 10px;
  }
`;
const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 72px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  .first-step {
    color: #000;
    margin: 26px 0;
    text-align: center;
    font-size: 20px;
    line-height: 27px;
  }
  .input-share {
    margin: 0 auto;
    width: 235px;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
    margin-bottom: 33px;
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 95%;
  max-height: 95%;
  margin: 0 auto;
  padding: 26px 8px 90px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .share {
    border-radius: 20px;
    background: ${theme.secondaryBg};
    width: 61px;
    height: 24px;
    border: none;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 14px;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
const StyledLinkBackModal = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 0 auto 1vh;
  img {
    cursor: pointer;
    position: absolute;
    left: 0;
    width: 16px;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
    font-weight: 700;
  }
`;
