import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { selectLangLocale } from '../features/langs/langsSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import Button from './atoms/Button';
import Input from './atoms/Input';
import FormattedMessage from './atoms/FormattedMessage';
import HeaderControls from './organism/HeaderControls';

import imgArrowBack from '../assets/images/svg/arrow-back.svg';

import { StartContainer } from './styled';

function PasswordChange() {
  const defaultLang = useSelector(selectLangLocale);
  return (
    <StartContainer>
      <StyledLinkBack to="/profile">
        <img src={imgArrowBack} alt="imgArrowBack" />
      </StyledLinkBack>

      <Input
        $bgColor="#fff"
        fontSize="15px"
        $maxWidth="320px"
        type="password"
        placeholder={vocabularyObject['signUp.input.password'][defaultLang]}
      />
      <Input
        $bgColor="#fff"
        fontSize="15px"
        $maxWidth="320px"
        type="password"
        placeholder={vocabularyObject['emailPasswordForgot.par.enterNewPassword'][defaultLang]}
      />

      <Button
        fontWeight={700}
        fontSize="20px"
        height="52px"
        margin="2vh 0 0 0"
        padding="0 38px"
        defType="primary"
        text={<FormattedMessage id="profile.btn.ready" />}
        to="/profile"
      />
    </StartContainer>
  );
}

export default PasswordChange;

const StyledLinkBack = styled(Link)`
  position: absolute;
  top: 80px;
  left: 8vw;
`;
