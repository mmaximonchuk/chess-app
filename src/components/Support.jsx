import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { gameRequest } from '../utils/gameRequest';
// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
// import FormattedMessage from './atoms/FormattedMessage';
import Button from './atoms/Button';
import FormattedMessage from '../components/atoms/FormattedMessage';

import iconUserSmall from '../assets/images/user-profile/small-user-icon.svg';
import iconShare from '../assets/images/game/VectorShare.svg';
import iconInfo from '../assets/images/info.svg';
import Modal from './molecules/ModalContainer';
import Select from './atoms/Select';
import HeaderControls from './organism/HeaderControls';
import { gameRanksRequest, selectGameRanks } from '../features/game/gameSlice';

// import iconSearch from '../assets/images/svg/search.svg';
import iconStar from '../assets/images/user-profile/input-user.svg';
import iconCard from '../assets/images/clan-pay/credit-c.svg';
import iconTick from '../assets/images/clan-pay/TickReversed.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

import { theme } from '../assets/styles/variables';

function Support() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);

  return (
    <Container>
      <StyledLinkBack to="/shop">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span>Support</span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          {/* <Link to="/paypal" className="links active">
            PayPal
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
          </Link>
          <Link to="/credit-card" className="links ">
            Credit cards
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
          </Link> */}
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default Support;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100%);
  margin: 0 auto;
  padding: 20vh 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .links {
    text-decoration: none;
    color: #fff;
    cursor: pointer;
    width: 100%;
    font-size: 10px;
    margin: 0 auto 43px;
    line-height: 13px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    padding: 0;
    max-width: 299px;
    height: 50px;
    position: relative;
    margin-bottom: 3px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    font-size: 25px;
    line-height: 33px;
    .tick {
      opacity: 0;
      position: absolute;
      right: 9px;
      top: 9px;
      border-radius: 50%;
      position: absolute;
      background: #fff;
      width: 32px;
      height: 32px;
      color: #656565;
      img {
        width: 30px;
        height: 24px;
        transform: translate(5px, -2px);
      }
    }
    &.active {
      .tick {
        opacity: 1;
      }
    }
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
