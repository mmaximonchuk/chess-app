import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Link, useParams, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import HeaderControls from './organism/HeaderControls';
import { addMessage, selectMessages, getUserChatRequest } from '../features/message/messageSlice';
import { selectUserData } from '../features/user/userSlice';
import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconSend from '../assets/images/svg/telegram.svg';

import { theme } from '../assets/styles/variables';

function ChatInner() {
  const params = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const currentMesages = useSelector(selectMessages);
  const defaultLang = useSelector(selectLangLocale);
  const { userID } = useSelector(selectUserData);
  const [inputMessage, setInputMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if (inputMessage.length) {
      dispatch(addMessage({text: inputMessage, userId: userID}));
    }
    setInputMessage('');
  };

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');
    // if(userID === '') {
    //   history.push('/entrance');
    // }
    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getUserChatRequest({ Token: token, Users: [params.userId] }));
  }, [])

  console.log(params.userId);
  return (
    <div>
      <ChatContainer onSubmit={handleSubmit}>
        <StyledLinkBack to="/messages">
          <img src={imgArrowBack} alt="imgArrowBack" />
        </StyledLinkBack>
        <MessagesField>
          {/* <p className="not-my">Нет, спасибо.</p>
          <p className="my">спасибо.</p>
          <p className="not-my">спасибо.</p>
          <p className="my">Нет, спасибо.</p>
          <p className="not-my">Нет, спасибо.</p>
          <p className="my">спасибо.</p>
          <p className="not-my">спасибо.</p>
          <p className="my">Нет, спасибо.</p>
          <p className="not-my">Нет, спасибо.</p> */}
          {currentMesages.map((message) => {
            return (
              <p key={message.id} className={`${message.senderUserId !== userID ? 'not-' : ''}my`}>
                {message.messageBody}
              </p>
            );
          })}

        </MessagesField>
        <MessageInputWrap>
          <input
            onChange={(e) => setInputMessage(e.target.value)}
            value={inputMessage}
            type="text"
            placeholder={vocabularyObject["chat.send.message"][defaultLang]}
          />
          <button type="submit">
            <img src={iconSend} alt="iconSend" />
          </button>
        </MessageInputWrap>
      </ChatContainer>
    </div>
  );
}

export default ChatInner;

const ChatContainer = styled.form`
  position: relative;
  max-width: 900px;
  margin: 0 auto;
  padding-top: 5vh;
  padding-bottom: 64px;
  padding-right: 9px;
  padding-left: 9px;
`;

const StyledLinkBack = styled(Link)`
  position: fixed;
  top: 80px;
  left: 8vw;
`;

const MessagesField = styled.div`
  display: flex;
  flex-direction: column;
  p {
    font-size: 15px;
    line-height: 20px;
    text-align: center;
    color: #000000;
    width: auto;
    display: inline-flex;
    margin-bottom: 2vh;
    background: ${theme.primaryBg};
    border-radius: 20px;
    padding: 10px 12px;
  }
  .my {
    align-self: flex-start;
  }
  .not-my {
    align-self: flex-end;
    background-color: ${theme.userMsgReplyBgColor};
  }
`;
const MessageInputWrap = styled.div`
  position: fixed;
  bottom: 9px;
  left: 50%;
  transform: translateX(-50%);
  width: calc(100vw - 18px);
  max-width: 900px;
  height: 54px;
  button {
    position: absolute;
    right: 10px;
    top: 7px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 45px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  input {
    width: 100%;
    height: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    font-size: 10px;
    line-height: 13px;
    color: #c8c8c8;
    font-family: 'Roboto', sans-serif;
    padding: 0 63px 0 9px;
    font-size: 18px;
    line-height: 21px;
    font-weight: 300;
  }
`;
