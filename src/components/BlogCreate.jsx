import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';

import iconCamera from '../assets/images/blogs/person.svg';
import iconAdd from '../assets/images/clan-pay/AddClan.svg';


import { theme } from '../assets/styles/variables';

function ClanInfo() {
  const defaultLang = useSelector(selectLangLocale);

  return (
    <Container>
      <StyledLinkBack to="/blogs">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span>Add Blogs </span>
      </StyledLinkBack>
      <WrapperBgTurquoise>
        <div className="wrapper-outer">
          <input type="file" className="field-share" id="share-input" />
          <label for="share-input" className="share-label">
            <img src={iconCamera} alt="" className="camera" />
            <img src={iconAdd} alt="imgInput" />
          </label>
          <input type="text" className="input-name" placeholder={vocabularyObject["blogs.create.name"][defaultLang]}/>
        </div>

        <WrapperBgPink>
          <ClanInner>
            <textarea className="textarea" placeholder={vocabularyObject["profile.modal.description"][defaultLang]}></textarea>

            <div className="connect-wrap">
              <button className="connect"><FormattedMessage id="blogs.publish"/></button>
            </div>
          </ClanInner>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ClanInfo;

const ClanInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .connect-wrap {
    display: flex;
    margin: 0 auto;
  }
  .par-participants {
    margin-bottom: 22px;
    font-size: 18px;
    line-height: 24px;
    color: #3f3e3e;
  }
  .share {
    margin-top: 30px;
  }
  .participants,
  .rating,
  .connect {
    background: #b0b1a1;
    border-radius: 20px;
    padding: 10px 15px;
    text-decoration: none;
    font-size: 15px;
    line-height: 20px;
    text-align: center;
    margin-bottom: 2vh;
    min-width: 196px;
    color: #ffffff;
  }
  .rating {
    min-width: 154px;
  }
  .connect {
    margin-top: 0;
    min-width: 131px;
    margin-bottom: 0;
    font-weight: 700;
    &--transparent {
      background: transparent;
    }
  }
  textarea {
    dispaly: flex;
    align-items: center;
    justify-content: center;
    resize: none;
    width: 80%;
    height: 116px;
    overflow: overlay;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 33px;
    text-align: left;
    font-size: 14px;
    padding: 13px 20px;
    height: 150px;
    color: ${theme.textFieldColor};
    margin: 0 auto 2vh;
  }
`;

const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 72px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  @media (max-width: 375px) {
    padding: 24px 9px 18px;
  }
  .field-share {
    opacity: 0;
    visibility: hidden;
    position: absolute;
  }
  .share-label {
    display: flex;
    margin-bottom: 37px;
  }
  .camera {
    margin-right: 5px;
  }
  .btn-fake {
    background: transparent;
    border: none;
  }
  .wrapper-outer {
    width: 100%;
    display: flex;
    .input-name {
      width: 100%;
      font-size: 15px;
      line-height: 20px;
      height: 37px;
      color: #c8c8c8;
      background: #ffffff;
      border: 1px solid #a4a4a4;
      border-radius: 20px;
      margin-top: 17px;
      margin-left: 6px;
      margin-right: 6px;
      padding: 0 15px;
      text-align: center;
    }
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 85%;
  max-height: 77%;
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  @media (max-width: 375px) {
    padding: 19px 12px;
  }
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
`;

const Container = styled.form`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
