//libs
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
// functions, methods
// import { vocabularyObject } from '../locales/vocabularyObject';
import { langs } from '../utils/languages';
import { setLang } from '../features/langs/langsSlice';
import { selectLangLocale } from '../features/langs/langsSlice';
// images
import imgLogo from '../assets/images/Лого.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import Button from './atoms/Button';
import Select from './atoms/Select';
import FormattedMessage from './atoms/FormattedMessage';
// styles
import { theme } from '../assets/styles/variables';
import { StartContainer } from './styled';

function Start() {
  const dispatch = useDispatch();
  const defaultLang = useSelector(selectLangLocale);
  const dot = (img = langs[defaultLang].flag) => ({
    alignItems: 'center',
    display: 'flex',

    ':before': {
      background: `url(${img})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'contain',
      borderRadius: 20,
      content: '" "',
      display: 'block',
      height: 37,
      width: 50,
    },
  });

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 10,
    svg: {
      display: 'none',
    },
  });
  const colourStyles = {
    menu: (styles) => ({
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: '#ececec',
      borderRadius: '12px',
      border: 'none',
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '5px 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      ...dot(),
      textAlign: 'center',
      width: '150px',
      justifyContent: 'space-between',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;
      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        ':active': {
          ...styles[':active'],
          backgroundImage: isDisabled ? null : isSelected ? `url(${img})` : null,
        },
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        display: 'flex',
        color: theme.languagesColor,
        ':before': {
          background: `url(${img})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'contain',
          borderRadius: 20,
          content: '" "',
          display: 'block',
          marginRight: 35,
          height: 37,
          width: 50,
        },
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      ...dot(data.flagImg),
      width: '150px',
      justifyContent: 'space-between',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };
  return (
    <StartContainer bgColor={theme.primaryBg}>
      {/* <Greeting><FormattedMessage id={'start.first.par'} /></Greeting> */}
      <StyledImg src={imgLogo} alt="" />
      <Title>Democrat chess</Title>
      <Tip><FormattedMessage id={'start.lang.choice'} /></Tip>
      <div>
        <Select
          onChange={(selectedOption) => dispatch(setLang(selectedOption.value))}
          defaultValue={defaultLang}
          value={langs[defaultLang].value}
          placeholder={langs[defaultLang].value}
          options={langs}
          styles={colourStyles}
        />
      </div>
      <Wrapper>
        <Button
          margin="2vh 0 0 0"
          defType="primary"
          text={<FormattedMessage id={'start.btn.continue'} />}
          to="/entrance"
          isLink
        />
      </Wrapper>
    </StartContainer>
  );
}

export default Start;

const Greeting = styled.p`
  font-size: 30px;
  line-height: 40px;
  color: #000;
  margin-bottom: 5px;
`;
const StyledImg = styled.img`
  position: relative;
  width: 27%;
  left: 10px;
  max-width: 120px;
`;
const Title = styled.h1`
  font-size: 40px;
  line-height: 53px;
  margin-top: 0;
  margin-bottom: 2vh;
  color: #000000;
`;
const Tip = styled.p`
  color: ${theme.greyTextColor};
  margin-bottom: 2vh;
`;
const Wrapper = styled.div`
  margin-top: auto;
`;
