import React, { useState } from 'react';

import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import iconCard from '../assets/images/blogs/puzzles-main.svg';
import iconTick from '../assets/images/blogs/puzzle-rev.svg';

function PuzzlesCreate() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const [isUserInfo, setUserInfo] = useState(false);

  return (
    <Container>
      <StyledLinkBack to="#">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span><FormattedMessage id="share.puzzles.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <img src={iconCard} alt="" className="main-img" />
        <WrapperBgPink>
          <Link to="/puzzles-create" className="links active">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            Создать свой шахматный пазл
          </Link>
          <Link to="/puzzles-available" className="links active">
            <div className="tick">
              <img src={iconTick} alt="" />
            </div>
            Решить пазл из списка готовых
          </Link>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}
export default PuzzlesCreate;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  text-align: center;
  .main-img {
    margin: 0 auto 20px;
    display: inline-block;
  }
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 161px);
  margin: 0 auto;
  padding: 10vh 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .links {
    text-decoration: none;
    color: #fff;
    cursor: pointer;
    width: 100%;
    font-size: 10px;
    margin: 0 auto 43px;
    line-height: 13px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    padding: 0;
    max-width: 299px;
    height: 50px;
    position: relative;
    margin-bottom: 43px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    transition: all 0.3s;
    font-size: 12px;
    line-height: 16px;
    .tick {
      opacity: 0;
      position: absolute;
      left: 9px;
      top: 9px;
      border-radius: 50%;
      position: absolute;
      background: #fff;
      width: 33px;
      height: 33px;
      color: #656565;
      img {
        width: 18px;
        height: 18px;
        transform: translate(-1px, 8px);
      }
    }
    &.active {
      .tick {
        opacity: 1;
      }
    }
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
