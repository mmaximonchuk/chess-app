import React from 'react';
import { Link } from 'react-router-dom';
// import { useSelector } from 'react-redux';
import styled from 'styled-components';

// import { vocabularyObject } from '../locales/vocabularyObject';
// import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';
import Stepper from './molecules/Stepper';
import HeaderControls from './organism/HeaderControls';

import imgArrowBack from '../assets/images/svg/balck-arrow-back.svg';
import imgInput from '../assets/images/game/input.svg';

function ShareGame() {
  // const defaultLang = useSelector(selectLangLocale);
  return (
    <Container>
      {/* <HeaderControls /> */}

      <StyledLinkBack to="/game">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span><FormattedMessage id="share.game.title"/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <p className="first-step">1. <FormattedMessage id="share.game.1"/></p>
        {/* <SearchContainer>
          <input
            type="text"
            placeholder={vocabularyObject['messages.input.username'][defaultLang]}
          />
          <button>
            <img src={iconSearch} alt="" />
          </button>
        </SearchContainer> */}
        <WrapperBgPink>
          <label>
            <ShareBtn>Share game in real time</ShareBtn>
            <input placeholder="Введите Адрес" type="text" className="input-share" />
          </label>
          <label>
            <ShareBtn>Share your game history</ShareBtn>
            <input placeholder="Введите Адрес" type="text" className="input-share" />
          </label>
          <input type="file" className="field-share" id="share-input" />
          <label for="share-input">
            <ShareBtn>Share the screenshot</ShareBtn>
            <img src={imgInput} alt="imgInput" />
          </label>

          <Stepper currentStep={1} />
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ShareGame;

const ShareBtn = styled.button`
  font-size: 25px;
  line-height: 33px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #b0b1a1;
  border-radius: 20px;
  color: #ffffff;
  padding: 5px 15px;
  border: none;
  margin-bottom: 8px;
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  .first-step {
    color: #000;
    margin: 10px 0 28px;
    text-align: center;
    font-size: 20px;
    line-height: 27px;
  }
  .input-share {
    margin: 0 auto;
    width: 235px;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
    margin-bottom: 33px;
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 63px);
  margin: 0 auto;
  padding: 20px 8px 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  label {
    text-align: center;
  }
  .field-share {
    opacity: 0;
    visibility: hidden;
    position: absolute;
  }
  .btn-fake {
    background: transparent;
    border: none;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  img {
    position: absolute;
    left: 15%;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

// const SearchContainer = styled.div`
//   width: 70%;
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   margin: 0 auto 14px;
//   input {
//     width: 100%;
//     background: #ffffff;
//     border: 1px solid #cfcfcf;
//     border-radius: 20px;
//     text-align: center;
//     font-size: 10px;
//     line-height: 13px;
//     height: 24px;
//     color: #c8c8c8;
//   }
//   button {
//     margin-left: 5px;
//     background: #b0b1a1;
//     border-radius: 20px;
//     border: none;
//     width: 32px;
//     height: 24px;
//     display: flex;
//     align-items: center;
//     justify-content: center;
//   }
// `;
