import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { clanVisible } from '../utils/clanVisible';
import { vocabularyObject } from '../locales/vocabularyObject';
import { selectLangLocale } from '../features/langs/langsSlice';
import FormattedMessage from './atoms/FormattedMessage';

import iconUserSmall from '../assets/images/user-profile/small-user-icon.svg';
import Modal from './molecules/ModalContainer';
import Select from './atoms/Select';
import { gameRanksRequest, selectGameRanks } from '../features/game/gameSlice';

import iconShare from '../assets/images/clan-pay/share-us-tab.svg';
import imgArrowBack from '../assets/images/svg/arrow-back-grey.svg';
import iconArrow from '../assets/images/svg/arrow-dropdown-down.svg';
import iconCamera from '../assets/images/clan-pay/camera.svg';
import iconAdd from '../assets/images/clan-pay/AddClan.svg';
import iconSearch from '../assets/images/svg/search.svg';
import imgInput from '../assets/images/game/input.svg';
import iCloseBtn from '../assets/images/game/close-btn.svg';
import { theme } from '../assets/styles/variables';

function ClanInfo() {
  const defaultLang = useSelector(selectLangLocale);
  const getRanks = useSelector(selectGameRanks);

  const dispatch = useDispatch();
  const [clanInfo, setClanInfo] = useState(null);
  const [userData, setUserData] = useState({});
  const [isModalOpen, setModalOpen] = useState(false);

  const arrowDown = () => ({
    background: `url(${iconArrow})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    content: '" "',
    display: 'block',
    height: 'auto',
    width: 17,
    marginRight: 5,
    svg: {
      display: 'none',
    },
  });

  const searchStyles = {
    menu: (styles) => ({
      ...styles,
      position: 'absolute',
      top: '0',
      backgroundColor: '#fff',
      width: '200px',
      left: '50%',
      transform: 'translate(-50%, -100%)',
    }),
    control: (styles) => ({
      ...styles,
      backgroundColor: 'white',
      borderRadius: '12px',
      minHeight: 26,
      margin: '0 auto 2vh',
      border: 'none',
      maxWidth: '300px',
      width: '100%',
      cursor: 'pointer',
      backgroundColor: theme.secondaryBg,
    }),
    input: (styles) => ({
      ...styles,
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 15px',
    }),
    placeholder: (styles) => ({
      ...styles,
      textAlign: 'center',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      fontSize: 14,
      justifyContent: 'space-between',
      color: '#000',
    }),
    option: (styles, { data, isDisabled, isSelected }) => {
      const img = data.flagImg;

      return {
        ...styles,
        cursor: isDisabled ? 'not-allowed' : 'default',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        color: '#4B4B4B',
        padding: '5px 0',
        fontSize: 10,
      };
    },
    singleValue: (styles, { data }) => ({
      ...styles,
      width: '173px',
      fontSize: 14,
      justifyContent: 'space-between',
      width: '200px',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%, -50%)',
      textAlign: 'center',
    }),
    indicatorSeparator: (styles) => ({ display: 'none' }),
    indicatorsContainer: (styles, state) => ({
      ...arrowDown(),
    }),
    valueContainer: (styles) => ({ padding: '5px 10px', width: 'calc(100% - 37px)' }),
  };

  return (
    <Container>
      <StyledLinkBack to="/clans">
        <img src={imgArrowBack} alt="imgArrowBack" />
        <span>
          <FormattedMessage id="clans.title" />
        </span>
      </StyledLinkBack>
      {isModalOpen && (
        <Modal closeModal={setModalOpen}>
          <ModalInnerSendMessage>
            <div className="wrapper">
              <img
                onClick={() => setModalOpen((prev) => !prev)}
                className="c-btns"
                src={iCloseBtn}
                alt=""
              />
              <SearchContainer>
                <input
                  type="text"
                  placeholder={vocabularyObject['messages.input.username'][defaultLang]}
                />
                <button>
                  <img src={iconSearch} alt="" />
                </button>
              </SearchContainer>
              <OfferListModal>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
                <div className="item">
                  <SearchedUser />
                </div>
              </OfferListModal>
            </div>
          </ModalInnerSendMessage>
        </Modal>
      )}
      <WrapperBgTurquoise>
        <div className="wrapper-outer">
          <input type="file" className="field-share" id="share-input" />
          <label for="share-input" className="share-label">
            <img src={iconCamera} alt="" className="camera" />
            <img src={iconAdd} alt="imgInput" />
          </label>
          <input type="text" className="input-name" placeholder={vocabularyObject["clans.clan.name"][defaultLang]} />
        </div>

        <WrapperBgPink>
          <ClanInner>
            <Link to="rating-tab" className="participants">
              <FormattedMessage id="clans.members" />: 14
            </Link>
            <Link to="rating" className="rating">
              <FormattedMessage id="clans.reting" />
            </Link>
            <textarea className="textarea" placeholder="Информация о клане"></textarea>
            <Select
              $minWidth="300px"
              defaultValue={clanVisible['All']}
              placeholder={<FormattedMessage id="clans.open" />}
              options={clanVisible}
              styles={searchStyles}
            />
            <NotificationButton onClick={() => setModalOpen(!isModalOpen)}>
              <span>
                <FormattedMessage id="clans.choose.members" />
              </span>
              <img src={iconArrow} alt="" />
            </NotificationButton>
            <p className="par-participants">
              <FormattedMessage id="clans.clan.members" />
            </p>
            <ParticipantsList>
              <SearchedUser shortenDescription />
              <SearchedUser shortenDescription />
              <SearchedUser shortenDescription />
              <SearchedUser shortenDescription />
              <SearchedUser shortenDescription />
              <SearchedUser shortenDescription />
            </ParticipantsList>
            <div className="connect-wrap">
              <button className="connect">
                <FormattedMessage id="clans.edit" />
              </button>
              <button className="connect--transparent">
                <FormattedMessage id="clans.remove" />
              </button>
            </div>

            <Link to="/share-us-tab" className="share">
              <img src={iconShare} alt="" />
            </Link>
          </ClanInner>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default ClanInfo;

const ParticipantsList = styled.div`
  border: 1px solid #a4a4a4;
  border-radius: 20px;
  max-width: 340px;
  width: 100%;
  height: 195px;
  overflow: overlay;
  padding: 26px 34px;
  margin-bottom: 31px;
`;

const SearchContainer = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto 13px;
  input {
    width: 100%;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 10px;
    line-height: 13px;
    height: 24px;
    color: #c8c8c8;
  }
  button {
    margin-left: 5px;
    background: #b0b1a1;
    border-radius: 20px;
    border: none;
    width: 32px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const OfferListModal = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  height: 137px;
  background: #ffffff;
  border: 1px solid #a4a4a4;
  border-radius: 20px;
  padding: 20px 22px;
  overflow: overlay;
  @media (max-width: 514px) {
    padding: 10px 10px;
  }
  .item {
    display: flex;
    width: 50%;
    .chat-item {
      width: 100%;
      margin-bottom: 11px;
      .chat-body {
        height: 22px;
      }
      .ava {
        width: 23px;
        height: 23px;
        display: flex;
        align-items: center;
        justify-content: center;
        img {
          width: 70%;
          height: 70%;
        }
      }
      .name {
        padding: 5px;
        background: #e3e3e3;
        font-size: 10px;
        color: #8f8f8f;
        line-height: 13px;
        text-align: left;
      }
    }
  }
`;

const ModalInner = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 320px;
  width: 60%;
  min-height: 170px;
  padding: 13px 11px 2px;
  background: #e5e5e5;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 12;
  transform: translateY(-50%) translateX(-50%);
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  span {
    font-size: 20px;
    line-height: 27px;
    color: #000000;
  }
`;

const ModalInnerSendMessage = styled(ModalInner)`
  background: #fff;
  max-width: 576px;
  .wrapper {
    .c-btns {
      position: absolute;
      top: 0px;
      right: 0px;
      cursor: pointer;
    }
    position: relative;
    width: 100%;
    .send-btn {
        margin-top: 15px;
        margin-left: auto;
        margin-right: 15px;
      padding: 0;
      background: ${theme.secondaryBg};
      border-radius: 20px;
      border: none;
      font-size: 8px;
      line-height: 11px;
      color: #fff;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 82px;
      height: 24px;
      img {
          margin-left: 5px;
          width: 16px;
          height: 14px;
      }
    }
    .emoji {
      position: absolute;
      bottom: 14px;
      left: 12px;
    }
    .toggle-emoji-dropdown {
      border: none;
      padding: 0;
      background: transparent;
    }
    .dropdown {
      position: absolute;
      border: 1px solid ${theme.inputBorderColor};
      border-radius: 20px;
      display: none;
      width: 138px;
      height: 120px;
      flex-wrap: wrap;
      padding: 16px;
      background: #fff;
      justify-content: space-between;
      align-items: center;space-between;
      &.visible {
        display: flex;
      }
    }
    .smile {
      width: 18px;
      height: 18px;
      border-radius: 50%;
      background: #c4c4c4;
    }
  }
`;

const NotificationButton = styled.div`
  font-size: 10px;
  background-color: #b0b1a1;
  width: 300px;
  padding: 0 20px 0 5px;
  height: 41px;
  border: none;
  border-radius: 12px;
  position: relative;
  font-size: 14px;
  margin-bottom: 2vh;
  display: flex;
  align-items: center;
  cursor: pointer;
  img {
    position: absolute;
    right: 5px;
  }
  span {
    margin: 0 auto;
  }
`;

const ClanInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .connect-wrap {
    max-width: 300px;
    width: 100%;
    display: flex;
    justify-content: space-between;
  }
  .par-participants {
    margin-bottom: 22px;
    font-size: 18px;
    line-height: 24px;
    color: #3f3e3e;
  }
  .share {
    margin-top: 30px;
  }
  .participants,
  .rating,
  .connect {
    background: #b0b1a1;
    border-radius: 20px;
    padding: 10px 15px;
    text-decoration: none;
    font-size: 15px;
    line-height: 20px;
    text-align: center;
    margin-bottom: 2vh;
    min-width: 196px;
    color: #ffffff;
  }
  .rating {
    min-width: 154px;
  }
  .connect {
    margin-top: 0;
    min-width: 131px;
    margin-bottom: 0;
    font-weight: 700;
    &--transparent {
      background: transparent;
    }
  }
  textarea {
    dispaly: flex;
    align-items: center;
    justify-content: center;
    resize: none;
    width: 80%;
    height: 116px;
    overflow: overlay;
    background: #ffffff;
    border: 1px solid #cfcfcf;
    border-radius: 20px;
    text-align: center;
    font-size: 14px;
    color: ${theme.textFieldColor};
    padding: 5px;
    margin: 0 auto 2vh;
  }
`;

const Loader = styled.div`
  .lds-ring {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, 50%);
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #000;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
  @media (max-width: 375px) {
    padding: 24px 9px 18px;
  }
  .field-share {
    opacity: 0;
    visibility: hidden;
    position: absolute;
  }
  .share-label {
    display: flex;
    margin-bottom: 37px;
  }
  .camera {
    margin-right: 5px;
  }
  .btn-fake {
    background: transparent;
    border: none;
  }
  .wrapper-outer {
    width: 100%;
    display: flex;
    .input-name {
      width: 100%;
      font-size: 15px;
      line-height: 20px;
      height: 37px;
      color: #c8c8c8;
      background: #ffffff;
      border: 1px solid #a4a4a4;
      border-radius: 20px;
      margin-top: 17px;
      margin-left: 6px;
      margin-right: 6px;
      padding: 0 15px;
      text-align: center;
    }
  }
`;
const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100% - 107px);
  margin: 0 auto;
  padding: 19px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  @media (max-width: 375px) {
    padding: 19px 12px;
  }
  .chat-body {
    min-height: 41px;
    align-items: center;
    display: flex;
  }
  .chat-item {
    margin-bottom: 7px;
  }
  .name {
    line-height: 1.6;
    padding-right: 38px;
  }
`;

const Container = styled.form`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;

function SearchedUser({ shortenDescription = false, to, showRating = false, callback, data = {} }) {
  return (
    <ChatItem className="chat-item">
      <Avatar className="ava">
        <img src={iconUserSmall} alt="" />
      </Avatar>
      <Body className="chat-body">
        <p className={`name ${!shortenDescription ? '' : 'mini'}`}>{`${data?.firstName || 'Имя'} ${
          data?.secondName || 'Фамилия'
        }`}</p>
        {!shortenDescription ? (
          <button className="add-btn">
            <img src={iconAdd} alt="" />
          </button>
        ) : (
          ''
        )}
      </Body>
      {!shortenDescription ? (
        ''
      ) : (
        <button className="remove-btn">
          <img src={iconAdd} alt="" />
        </button>
      )}
    </ChatItem>
  );
}

const ChatItem = styled.div`
  display: flex;
  align-items: center;
  text-decoration: none;
  width: 90%;
  margin: 0 auto 27px;
  .share-btn,
  .info-btn {
    cursor: pointer;
    padding: 0;
    border: none;
    background: transparent;
    position: absolute;
    right: 50px;
    top: 5px;
    width: 33px;
    height: 33px;
    img {
      width: 100%;
    }
  }
  .add-btn {
    position: absolute;
    top: 3px;
    right: 11px;
    width: 17px;
    background: transparent;
    img {
      width: 100%;
    }
  }
  .remove-btn {
    width: 34px;
    background: transparent;
    transform: rotate(45deg);
    img {
      width: 100%;
    }
  }
`;
const Avatar = styled.div`
  position: relative;
  background: #d2d2d2;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 41px;
  height: 41px;
  margin-right: 16px;
`;

const Body = styled.div`
  flex: 1;
  position: relative;
  .name {
    margin-left: -9px;
    padding: 8px 9px;
    background: #ffffff;
    border-radius: 20px;
    width: 100%;
    height: 100%;
    color: ${({ shortenDescription }) => (!shortenDescription ? '#505050' : '#a9a9a9')};
    font-size: 15px;
    &.mini {
      font-size: 10px;
      line-height: 13px;
    }
  }
`;
