import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import FormattedMessage from './atoms/FormattedMessage';
import Loader from './atoms/Loader';
import { getNewsRequest, selectNews } from '../features/news/newsSlice';

import iconCard from '../assets/images/blogs/share.svg';

function News() {
  // const defaultLang = useSelector(selectLangLocale);
  const dispatch = useDispatch();
  const news = useSelector(selectNews);

  useEffect(() => {
    localStorage.clear();
    let token = localStorage.getItem('token');

    if (!token) {
      token = 'tmp+0fc04049-2c8c-4d9e-a5dd-2e24410930d7';

      localStorage.setItem('token', token);
    }
    dispatch(getNewsRequest({ Token: token }));
  }, []);

  return (
    <Container>
      <StyledLinkBack to="/shop">
        {/* <img src={imgArrowBack} alt="imgArrowBack" /> */}
        <span><FormattedMessage id='news.title'/></span>
      </StyledLinkBack>

      <WrapperBgTurquoise>
        <WrapperBgPink>
          <NewsWrapper>
            {news ? (
              news.map((item) => {
                return (
                  <Link key={item.newsID} to={`/news-open/${item.newsID}`} className="news-item">
                    <p className="title">{item.subject}</p>
                    <div className="bottom">
                      <span>
                        <FormattedMessage id="news.description" />
                      </span>
                      <Link to="/share-us-tab">
                        <img src={iconCard} alt="" />
                      </Link>
                    </div>
                  </Link>
                );
              })
            ) : (
              <Loader />
            )}
          </NewsWrapper>
        </WrapperBgPink>
      </WrapperBgTurquoise>
    </Container>
  );
}

export default News;

const NewsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  text-decoration: none;

  .news-item {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-height: 193px;
    flex: 1 1 calc(50% - 23px);
    align-items: center;
    background: #ffffff;
    border: 1px solid #bdbdbd;
    border-radius: 20px;
    max-width: calc(50% - 23px);
    margin-bottom: 20px;
    text-decoration: none;
    &:nth-child(even) {
      margin-left: 46px;
    }
    .title {
      margin-top: 10px;
      font-size: 10px;
      line-height: 13px;
      color: #222222;
      font-weight: bold;
    }
    .bottom {
      display: flex;
      align-items: center;
      margin-bottom: 5px;
      color: #979797;
      span {
        line-height: 20px;
        font-size: 15px;
        margin-right: 8px;
      }
    }
  }
  @media (max-width: 390px) {
    .news-item {
      flex: 1 1 100%;
      max-width: 100%;
      &:nth-child(even) {
        margin-left: 0;
      }
    }
  }
`;

const WrapperBgTurquoise = styled.div`
  max-width: 700px;
  margin: 0 auto;
  height: calc(var(--app-height) - 106px);
  position: relative;
  background-color: #d1e6e7;
  border-radius: 50px 50px 0px 0px;
  padding: 24px 16px 18px;
  overflow: hidden;
`;

const WrapperBgPink = styled.div`
  overflow: overlay;
  position: relative;
  max-width: 100%;
  height: 100%;
  max-height: calc(100%);
  margin: 0 auto;
  padding: 70px 22px;

  background-color: #d1e6e7;
  background: #f7f4ea;
  border-radius: 50px 50px 0px 0px;
  .loader {
    margin: 0 auto;
  }
`;

const Container = styled.div`
  overflow: hidden;
`;

const StyledLinkBack = styled(Link)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 66px;
  text-decoration: none;
  max-width: 414px;
  margin: 54px auto 19px;
  & > img {
    position: absolute;
    left: 15%;
  }
  .add-clan {
    position: absolute;
    right: 15%;
    top: 0;
  }
  span {
    color: #444444;
    font-size: 25px;
    line-height: 33px;
  }
`;
