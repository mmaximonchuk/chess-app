import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../features/user/userSlice';
import langsReducer from '../features/langs/langsSlice';
import gameReducer from '../features/game/gameSlice';
import messageReducer from '../features/message/messageSlice';
import tournamentReducer from '../features/tournament/tournamentSlice';
import clansReducer from '../features/clans/clansSlice';
import productsReducer from '../features/products/productsSlice';
import puzzleReducer from '../features/puzzles/puzzleSlice';
import newsReducer from '../features/news/newsSlice';

export const store = configureStore({
  reducer: {
    user: userReducer,
    game: gameReducer,
    langs: langsReducer,
    message: messageReducer,
    tournament: tournamentReducer,
    clans: clansReducer,
    products: productsReducer,
    puzzles: puzzleReducer,
    news: newsReducer,
  },
});