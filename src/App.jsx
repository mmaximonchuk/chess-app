import React, { useEffect } from 'react';
// import { useSelector } from 'react-redux';
import { Switch, Route, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import iPlay from './assets/images/game/play.svg';
import iUser from './assets/images/game/user.svg';
import iRating from './assets/images/game/rating.svg';
import iMessages from './assets/images/game/messages.svg';
import iPlayAgain from './assets/images/game/play-again.svg';
import iShare from './assets/images/game/share.svg';
import iCart from './assets/images/game/cart.svg';
import iPlayers from './assets/images/game/users.svg';
import iTrophie from './assets/images/game/trophies.svg';
import iPuzzle from './assets/images/game/puzzle.svg';
import iWriteMsg from './assets/images/game/write-message.svg';
import iNews from './assets/images/game/news.svg';
import iTopRating from './assets/images/game/top-rating.svg';
import iSettings from './assets/images/game/settings.svg';
import iSupport from './assets/images/game/support.svg';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
// import { selectLangLocale } from './features/langs/langsSlice';

import Start from '../src/components/Start';
import Entrance from '../src/components/Entrance';
import SignUp from '../src/components/SignUp';
import PasswordForgot from '../src/components/PasswordForgot';
import EmailPasswordForgot from '../src/components/EmailPasswordForgot';
import EmailChange from '../src/components/ChangeEmail';
import PasswordChange from '../src/components/ChangePassword';
import Profile from '../src/components/Profile';
import Messages from '../src/components/Messages';
import MessagesPool from '../src/components/MessagesPool';
import ChatInner from '../src/components/ChatInner';
import AllGames from '../src/components/AllGames';
import FindPartner from '../src/components/FindPartner';
import CreateGame from '../src/components/CreateGame';
import Game from '../src/components/Game';
import ShareGame from '../src/components/ShareGame';
import ShareGame2 from '../src/components/ShareGame2';
import ShareGame3 from '../src/components/ShareGame3';
import RatingTab from '../src/components/RatingTab';
import ShareUsTab from '../src/components/ShareUsTab';
import GameHistory from '../src/components/GameHistory';
import Shop from '../src/components/Shop';
import Clans from '../src/components/Clans';
import Payment from '../src/components/Payment';
import CreditCard from '../src/components/CreditCard';
import Paypal from '../src/components/PayPal';
import ClanInfo from '../src/components/ClanInfo';
import ClanCreate from '../src/components/ClanCreate';
import Blogs from '../src/components/Blogs';
import BlogCreate from '../src/components/BlogCreate';
import Tournament from '../src/components/Tournament';
import TournamentCreate from '../src/components/TournamentCreate';
import Puzzles from '../src/components/Puzzles';
import PuzzlesAvailable from '../src/components/PuzzlesAvailable';
import PuzzlesCreate from '../src/components/PuzzlesCreate';
import News from '../src/components/News';
import NewsOpen from '../src/components/NewsOpen';
import Settings from '../src/components/Settings';
import League from '../src/components/League';
import Support from '../src/components/Support';
// import Layout from '../src/components/Layout';
// import {theme} from './assets/styles/variables';

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: `transparent`,
    padding: '0',
  },
}));

export default function Layout() {
  const classes = useStyles();
  const history = useHistory();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    const newRoute = event.target.closest('.MuiTab-root').getAttribute('aria-label');
    setValue(newValue);
    history.push(`/${newRoute}`);
  };

  return (
    <div className="App">
      <Switch>
        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              <Tab aria-label="all-games" icon={<img src={iPlay} alt="" />} {...a11yProps(0)} />
              <Tab aria-label="entrance" icon={<img src={iUser} alt="" />} {...a11yProps(1)} />
              <Tab aria-label="rating-tab" icon={<img src={iRating} alt="" />} {...a11yProps(2)} />
              <Tab aria-label="messages" icon={<img src={iMessages} alt="" />} {...a11yProps(3)} />
              <Tab
                aria-label="game-history"
                icon={<img src={iPlayAgain} alt="" />}
                {...a11yProps(4)}
              />
              <Tab aria-label="share-us-tab" icon={<img src={iShare} alt="" />} {...a11yProps(5)} />
              <Tab aria-label="shop" icon={<img src={iCart} alt="" />} {...a11yProps(6)} />
              <Tab aria-label="clans" icon={<img src={iPlayers} alt="" />} {...a11yProps(7)} />
              <Tab aria-label="tournament" icon={<img src={iTrophie} alt="" />} {...a11yProps(8)} />
              <Tab aria-label="puzzles" icon={<img src={iPuzzle} alt="" />} {...a11yProps(9)} />
              <Tab aria-label="blogs" icon={<img src={iWriteMsg} alt="" />} {...a11yProps(10)} />
              <Tab aria-label="news" icon={<img src={iNews} alt="" />} {...a11yProps(11)} />
              <Tab
                aria-label="league-overview"
                icon={<img src={iTopRating} alt="" />}
                {...a11yProps(12)}
              />
              <Tab aria-label="settings" icon={<img src={iSettings} alt="" />} {...a11yProps(13)} />
              <Tab aria-label="support" icon={<img src={iSupport} alt="" />} {...a11yProps(14)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={5}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={6}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={7}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={8}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={9}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={10}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={11}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={12}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={13}>
            <InnerTabs />
          </TabPanel>
          <TabPanel value={value} index={14}>
            <InnerTabs />
          </TabPanel>
          {/* <TabPanel value={value} index={12}>
            <InnerTabs/>
          </TabPanel>
          <TabPanel value={value} index={13}>
            <InnerTabs/>
          </TabPanel>
          <TabPanel value={value} index={14}>
            <InnerTabs/>
          </TabPanel> */}
        </div>
      </Switch>
    </div>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function InnerTabs() {
  return (
    <>
      <Route path="/league-overview">
        <League />
      </Route>
      <Route path="/settings">
        <Settings />
      </Route>
      <Route path="/support">
        <Support />
      </Route>
      <Route path="/news-open/:newsId?">
        <NewsOpen />
      </Route>
      <Route path="/news">
        <News />
      </Route>
      <Route path="/puzzles">
        <Puzzles />
      </Route>
      <Route path="/puzzles-available">
        <PuzzlesAvailable />
      </Route>
      <Route path="/puzzles-create">
        <PuzzlesCreate />
      </Route>
      <Route path="/tournament-create">
        <TournamentCreate />
      </Route>
      <Route path="/tournament">
        <Tournament />
      </Route>
      <Route path="/blog-create">
        <BlogCreate />
      </Route>
      <Route path="/blogs">
        <Blogs />+
      </Route>
      <Route path="/clan-create">
        <ClanCreate />
      </Route>
      <Route path="/clan-info/:clanId?">
        <ClanInfo />
      </Route>
      <Route path="/paypal">
        <Paypal />
      </Route>
      <Route path="/credit-card">
        <CreditCard />
      </Route>
      <Route path="/payment">
        <Payment />
      </Route>
      <Route path="/clans">
        <Clans />
      </Route>
      <Route path="/shop">
        <Shop />
      </Route>
      <Route path="/game-history">
        <GameHistory />
      </Route>
      <Route path="/share-us-tab">
        <ShareUsTab />
      </Route>
      <Route path="/rating-tab">
        <RatingTab />
      </Route>
      <Route path="/share-3-step">
        <ShareGame3 />
      </Route>
      <Route path="/share-2-step">
        <ShareGame2 />
      </Route>
      <Route path="/share-1-step">
        <ShareGame />
      </Route>
      <Route path="/game">
        <Game />
      </Route>
      <Route path="/create-game">
        <CreateGame />
      </Route>
      <Route path="/find-partner">
        <FindPartner />
      </Route>
      <Route path="/all-games">
        <AllGames />
      </Route>
      <Route path="/chat/:userId?">
        <ChatInner />
      </Route>
      <Route path="/messages-pool">
        <MessagesPool />
      </Route>
      <Route path="/messages">
        <Messages />
      </Route>
      <Route path="/profile">
        <Profile />
      </Route>
      <Route path="/email-change">
        <EmailChange />
      </Route>
      <Route path="/password-change">
        <PasswordChange />
      </Route>
      <Route path="/email-password-forgot">
        <EmailPasswordForgot />
      </Route>
      <Route path="/password-forgot">
        <PasswordForgot />
      </Route>
      <Route path="/sign-up">
        <SignUp />
      </Route>
      <Route path="/entrance">
        <Entrance />
      </Route>
      <Route path="/" exact>
        <Start />
      </Route>
    </>
  );
}
