import hvsh from '../assets/images/game-type/human-human.svg';
import hvsht from '../assets/images/game-type/human-human-t.svg';
import hvsai from '../assets/images/game-type/human-ii.svg';
import hvsait from '../assets/images/game-type/human-ii-t.svg';
import gvsg from '../assets/images/game-type/group-group.svg';
import gvsai from '../assets/images/game-type/group-ii.svg';
import gvsgt from '../assets/images/game-type/group-group-t.svg';
import gvsait from '../assets/images/game-type/group-ii-t.svg';

export const gameType = {
    humanVSHuman: {
        value: 'Человек с Человеком',
        flag: hvsh,
      },
      humanVSHumanTime: { value: 'человек с Человеком на время', flag: hvsht },
      humanVSai: { value: 'Человек с ИИ', flag: hvsai },
      humanVSaiTime: { value: 'Человек с ИИ на время', flag: hvsait },
      groupVSGroup: { value: 'Группа против Группы', flag: gvsg },
      groupVSai: { value: 'Группа против ИИ', flag: gvsai },
      groupVSGroupTime: { value: 'Группа против Группы на время', flag: gvsgt },
      groupVSaiTime: { value: 'группа против ИИ на время', flag: gvsait },
};
  
  