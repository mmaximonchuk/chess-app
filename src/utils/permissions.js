
export const permissions = {
  All: 'всем',
  FriendsAndMyClansMemeber: 'только друзьям и членам клана',
  FriendsOnly: 'только друзьям',
  ClansOnly: 'только членам клана',
  Nobody: 'никому',
};

