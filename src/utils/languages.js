import imgEN from '../assets/images/svg/gb.svg';
import imgFR from '../assets/images/svg/bl.svg';
import imgSP from '../assets/images/svg/flag-spanish-flag-spanish-spain-flag-of-spain.svg';
import imgGR from '../assets/images/svg/germany-flag-png-239051.svg';
import imgPR from '../assets/images/svg/flag-portuga.svg';
import imgCN from '../assets/images/svg/china.svg';
import imgHN from '../assets/images/svg/hindi.svg';
import imgJP from '../assets/images/svg/japan-flag-png-xl1.svg';
import imgAR from '../assets/images/svg/Arabic-Language-Flag1.svg';
import imgRUS from '../assets/images/svg/Flag_of_Russia1.svg';
import imgHBR from '../assets/images/svg/Flag_of_Israel.svg';

export const langs = {
  EN: {
    value: 'English',
    flag: imgEN,
  },
  FR: { value: 'French', flag: imgFR },
  SP: { value: 'Spanish', flag: imgSP },
  GR: { value: 'German', flag: imgGR },
  PR: { value: 'Portuges', flag: imgPR },
  CN: { value: 'Chinese', flag: imgCN },
  HN: { value: 'Hindi', flag: imgHN },
  JP: { value: 'Japanese', flag: imgJP },
  AR: { value: 'Arabic', flag: imgAR },
  RUS: { value: 'Russian', flag: imgRUS },
  HBR: { value: 'Hebrew', flag: imgHBR },
};

