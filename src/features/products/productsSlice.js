
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getProductsRequest = createAsyncThunk('game/getProductsRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Payment/GetProducts', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});



const initialState = {
  products: null,
  status: null,
};

const productsSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },
  },
  extraReducers: {
      [getProductsRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getProductsRequest.fulfilled]: (state, action) => {
        state.products = action.payload;
        state.status = 'success';
      },
      [getProductsRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

export const selectProducts = (state) => state.products.products;

export default productsSlice.reducer;
