import {createSlice} from '@reduxjs/toolkit';
// import { langs } from '../../utils/languages';

const initialState = {
    lang: 'RUS',
}


const langsSlice = createSlice({
    name: 'langs',
    initialState,
    reducers: {
        setLang: (state, action) => {
            state.lang = action.payload;
        },
    }
})

export const { setLang } = langsSlice.actions;
export const selectLangLocale = state => state.langs.lang;


export default langsSlice.reducer;