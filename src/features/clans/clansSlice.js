import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getClansRequest = createAsyncThunk('game/getClansRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Clan/GetClans', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});



const initialState = {
  clans: null,
  status: null,
};

const clansSlice = createSlice({
  name: 'clans',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },
  },
  extraReducers: {
      [getClansRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getClansRequest.fulfilled]: (state, action) => {
        state.clans = action.payload;
        state.status = 'success';
      },
      [getClansRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

export const selectClans = (state) => state.clans.clans;

export default clansSlice.reducer;
