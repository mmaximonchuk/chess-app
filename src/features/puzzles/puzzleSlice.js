
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getPuzzlesRequest = createAsyncThunk('game/getPuzzlesRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Game/GetPuzzles', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});



const initialState = {
  puzzles: null,
  status: null,
};

const puzzleSlice = createSlice({
  name: 'puzzle',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },
  },
  extraReducers: {
      [getPuzzlesRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getPuzzlesRequest.fulfilled]: (state, action) => {
        state.puzzles = action.payload;
        state.status = 'success';
      },
      [getPuzzlesRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

export const selectPuzzles = (state) => state.puzzles.puzzles;

export default puzzleSlice.reducer;
