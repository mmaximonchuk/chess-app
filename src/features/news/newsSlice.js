
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getNewsRequest = createAsyncThunk('game/getNewsRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Content/GetNews', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});


const initialState = {
  news: null,
  status: null,
};

const newsSlice = createSlice({
  name: 'new',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },
  },
  extraReducers: {
      [getNewsRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getNewsRequest.fulfilled]: (state, action) => {
        state.news = action.payload;
        state.status = 'success';
      },
      [getNewsRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

export const selectNews = (state) => state.news.news;

export default newsSlice.reducer;
