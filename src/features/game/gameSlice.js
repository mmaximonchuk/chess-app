import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const gameSetupRequest = createAsyncThunk('game/gameSetup', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Game/GetSetup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});
export const gameHistoriesRequest = createAsyncThunk('game/gameHistoriesRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Game/GetGameHistories', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});
export const gameRanksRequest = createAsyncThunk('game/gameRanksRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Game/GetRanks', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});
export const getUsersRequest = createAsyncThunk('game/getUsersRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Validator/GetUsers', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});



const initialState = {
  gameSetup: null,
  gameHistories: null,
  gameRanks: null,
  users: null,
  status: null,
};

const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },

  },
  extraReducers: {
      [gameSetupRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [gameSetupRequest.fulfilled]: (state, action) => {
        state.gameSetup = action.payload;
        state.status = 'success';
      },
      [gameSetupRequest.rejected]: (state) => {
        state.status = 'failed';
      },
      [gameHistoriesRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [gameHistoriesRequest.fulfilled]: (state, action) => {
        state.gameHistories = action.payload;
        state.status = 'success';
      },
      [gameHistoriesRequest.rejected]: (state) => {
        state.status = 'failed';
      },
      [gameRanksRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [gameRanksRequest.fulfilled]: (state, action) => {
        state.gameRanks = action.payload;
        state.status = 'success';
      },
      [gameRanksRequest.rejected]: (state) => {
        state.status = 'failed';
      },
      [getUsersRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getUsersRequest.fulfilled]: (state, action) => {
        state.users = action.payload;
        state.status = 'success';
      },
      [getUsersRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

// export const { gameMainCategoriesRequest } = gameSlice.actions;
export const selectGameSetup = (state) => state.game.gameSetup;
export const selectGameHistories = (state) => state.game.gameHistories;
export const selectGameRanks = (state) => state.game.gameRanks;
export const selectUsers = (state) => state.game.users;

export default gameSlice.reducer;
