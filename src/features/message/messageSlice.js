import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getUnreadMessageRequest = createAsyncThunk('user/getUnreadMessageRequest',
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Message/GetUnreadChatMessages', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});

export const getUserChatRequest = createAsyncThunk('user/getUserChatRequest',
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Message/GetChatMessages', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});

const initialState = {
  messagesData: null,
  messages: [
    {
      id: 1,
      messageBody: 'privet',
    },
    {
      id: 2,
      messageBody: 'privet2',
    },
    {
      id: 3,
      messageBody: 'otviet',
    },
  ],
  status: null,
  userId: null,
};

const messageSlice = createSlice({
  name: 'message',
  initialState,
  reducers: {
    addMessage: (state, action) => {
      let messageObj = {
        id: new Date().getMilliseconds(),
        messageBody: action.payload.text,
        senderUserId: action.payload.userId,
      }
      state.messages = [...state.messages, messageObj];
    },
  },
  extraReducers: {
    [getUnreadMessageRequest.pending]: (state) => {
      state.status = 'loading';
    },
    [getUnreadMessageRequest.fulfilled]: (state, action) => {
      state.messagesData = action.payload;
      state.status = 'success';
    },
    [getUnreadMessageRequest.rejected]: (state) => {
      state.status = 'failed';
    },
    [getUserChatRequest.pending]: (state) => {
      state.status = 'loading';
    },
    [getUserChatRequest.fulfilled]: (state, action) => {
      state.messages = action.payload;
      state.status = 'success';
    },
    [getUserChatRequest.rejected]: (state) => {
      state.status = 'failed';
    },
  }
});

export const { addMessage } = messageSlice.actions;
export const selectMessages = state => state.message.messages;
export const selectNewMessages = state => state.message.messagesData;

export default messageSlice.reducer;
