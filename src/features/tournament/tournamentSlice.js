import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const getTournamentResultsRequest = createAsyncThunk('game/getTournamentResultsRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Tournament/GetTournamentResult', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});



const initialState = {
  tournamentResults: null,
  status: null,
};

const tournamentSlice = createSlice({
  name: 'tournament',
  initialState,
  reducers: {
    // setUserSignUp: (state, action) => {
    //   state.userData = action.payload;
    // },
  },
  extraReducers: {
      [getTournamentResultsRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [getTournamentResultsRequest.fulfilled]: (state, action) => {
        state.tournamentResults = action.payload;
        state.status = 'success';
      },
      [getTournamentResultsRequest.rejected]: (state) => {
        state.status = 'failed';
      },
  },
});

export const selectTournamentResults = (state) => state.tournament.tournamentResults;

export default tournamentSlice.reducer;
