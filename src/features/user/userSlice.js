import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const signUpRequest = createAsyncThunk('user/signUpRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Validator/Signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});

export const signInRequest = createAsyncThunk('user/signInRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Validator/ValidateUser', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});
export const recoverPasswordRequest = createAsyncThunk('user/recoverPasswordRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Validator/RecoverPassword', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});

export const updateUserSettingsRequest = createAsyncThunk('user/updateUserSettingsRequest', 
    async (formData) => {
    const result = await fetch('https://cad83b692024.ngrok.io/ChessApp/api/Validator/UpdateUserInfo', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
    console.log(result.clone().json());
    return result.clone().json();
});

const initialState = {
  userData: { 
    userID: '',
  },
  token: '',
  signUpstatus: null,
  status: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserSignUp: (state, action) => {
      state.userData = action.payload;
    },

  },
  extraReducers: {
      [signUpRequest.pending]: (state) => {
        state.signUpstatus = 'loading';
      },
      [signUpRequest.fulfilled]: (state, action) => {
        state.signUpstatus = 'success';
        // state.token = action.payload.token;
      },
      [signUpRequest.rejected]: (state) => {
        state.signUpstatus = 'failed';
      },
      [signInRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [signInRequest.fulfilled]: (state, action) => {
        state.userData = action.payload;
        state.status = 'success';
        state.token = action.payload.token;
      },
      [signInRequest.rejected]: (state) => {
        state.status = 'failed';
      },
      [recoverPasswordRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [recoverPasswordRequest.fulfilled]: (state) => {
        state.status = 'success';
      },
      [recoverPasswordRequest.rejected]: (state) => {
        state.status = 'failed';
      },
      [updateUserSettingsRequest.pending]: (state) => {
        state.status = 'loading';
      },
      [updateUserSettingsRequest.fulfilled]: (state) => {
        state.status = 'success';
      },
      [updateUserSettingsRequest.rejected]: (state) => {
        state.status = 'failed';
      }
  },
});

export const { setUserSignUp } = userSlice.actions;
export const selectUserData = state => state.user.userData;
export const selectSignUpStatus = state => state.user.signUpstatus;
// export const selectUserPhoto = state => state.user.photo;

export default userSlice.reducer;
